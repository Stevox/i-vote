import { xFetch } from "flitlib";

export const ACTIONS = {
    FETCH_USER: 'users',
    FETCH_ELECTIONS: 'elections',
    FETCH_ELECTION: 'voters/election',
}

export const fetchData = (action) => {
    return async dispatch => {
        if (!action) {
            const promises = [];
            for (let val of Object.values(ACTIONS)) {
                promises.push(dispatch(fetchData(val)))
            }
            // Reflected coz we the requests aren't dependent on each other
                // Don't care if some fail coz different user authorizations
            return await Promise.all(promises.map(p => p.then(
                (value) => ({ value, status: "fulfilled" }),
                err => ({ err, status: "rejected" }))
            ));
        }

        dispatch({
            type: `pendingAction/start`,
            payload: action
        });

        try {
            const { data } = await xFetch(`/${action}`);
            dispatch({
                type: `user/fetchedData`,
                payload: { [action]: data }
            });
            return data;
        } finally {
            dispatch({
                type: `pendingAction/end`,
                payload: action
            });
        }

    }
}

export const login = (auth) => {
    return async dispatch => {
        if (auth) {
            await xFetch('/login', {
                method: 'POST',
                body: auth
            });
        }

        await fetchData();
    }
}

export const logout = () => {
    return async dispatch => {
        dispatch({
            type: 'user/loggedOut'
        });
        await xFetch('/login', {
            method: 'DELETE'
        });
    }
}





/**
 * @typedef Notification
 * @type {{
    *  message: string,
    *  variant: "default"|"info"|"success"|"error"|"warning",
    *  persist: boolean,
    *  autoHideDuration: number,
    *  action: Node|null,
    *  onClose: Function
    * }}
    */
/**
 * @param {Notification} payload - notification details
 */
export const addNotification = (payload) => {
    return dispatch => {
        const key = Math.random().toString(36).slice(2);
        const addTime = new Date();

        dispatch({
            type: 'notification/added',
            payload: { ...payload, key, addTime }
        });

        return key;
    }
}

/**
 * @param {Notification} payload - notification details
 * @param {string} payload.key - notification identifier
 */
export const editNotification = (payload) => {
    return dispatch => {
        const newKey = Math.random().toString(36).slice(2);
        const addTime = new Date();
        dispatch({
            type: 'notification/edited',
            payload: { ...payload, newKey, addTime }
        });

        return newKey;
    }
}

/**
 * @param {string} key - notification identifier
 */
export const dismissNotification = (key) => {
    return {
        type: 'notification/dismissed',
        payload: key
    }
}