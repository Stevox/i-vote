import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Typography, Button } from '@material-ui/core';
import { useRouter, xFetch } from 'flitlib';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import ResultDialog from './ResultDialog';
import { addNotification } from 'Redux/actions';

function ResultsPage(props) {
    const [resultsDialogOpen, setResultsDialogOpen] = useState(false);
    const user = useSelector(state => state.user);
    const election = useSelector(state => state.election);

    const dispatch = useDispatch();
    const fetchResults = async () => {
        try {
            await xFetch('/voters/election/results', {
                body: {
                    electionId: election.electionId,
                }
            });
        } catch (err) {
            dispatch(addNotification({
                message: err.toString(),
                autoHideDuration: 8000,
            }));
        }
    }

    return (
        <Wrapper>
            <div className='info votes' >
                {user?.hasVoted &&
                    <Typography variant='h3' color='primary' >
                        {'You have voted.'}
                    </Typography>
                }
                <Typography variant='h4' >
                    {`Election ended ${moment(election?.endDate).fromNow()}`}
                </Typography>
                <Typography variant='h4' >
                    {`Votes cast : ${election?.votesNum || 0}`}
                </Typography>
            </div>
            <div className='info action' >
                <Button
                    color='primary' size='large' variant='contained'
                    onClick={e => setResultsDialogOpen(true)}
                >
                    View Results
                </Button>
                <Button
                    color='secondary' size='large'
                    variant='outlined'
                    onClick={fetchResults}
                >
                    Download Full Results
                </Button>
            </div>

            <ResultDialog 
                open={resultsDialogOpen} onClose={_=> setResultsDialogOpen(false)}
            />
        </Wrapper>
    )
}


const Wrapper = styled.div`
    >.info {
        &.votes {
            margin-bottom: 3rem;
        }
        &.action>* {
            align-self: stretch;
            padding: 1rem;
        }
    }
`;

ResultsPage.propTypes = {

}

export default ResultsPage;

