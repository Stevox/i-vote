import { Step, StepContent, StepLabel, Stepper, Typography, useMediaQuery } from '@material-ui/core';
import { ResponsiveBar } from '@nivo/bar';
import GenericDialog from 'Components/GenericDialog';
import PropTypes from 'prop-types';
import React, { useMemo, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { counter } from 'flitlib';
import ResultsBarGraph from 'Components/ResultsBarGraph';

function ResultDialog(props) {
    const [stepIndex, setStepIndex] = useState(0);
    const election = useSelector(state => state.election);
    const user = useSelector(state => state.user);
    const isMinWidthMd = useMediaQuery(theme => theme.breakpoints.up('sm'));
    
    useEffect(() => {
        setStepIndex(0);
    }, [props.open]);

    const polls = useMemo(() => {
        const len = election?.regions?.length;
        if (!len) return null;
        const acc = [];
        let regionAreaId = user.regionAreaId;
        for (let i = len - 1; i > -1; i--) {
            // Starts from the lowest region to get RegionArea where voter is registered
            const region = election.regions[i];
            const regionArea = region.regionAreas?.find(ra =>
                ra.regionAreaId == regionAreaId
            );
            if (!regionArea) continue;
            regionAreaId = regionArea.parentRegionAreaId;
            const regionPolls = region.polls.map(p => {
                const candidates = regionArea.candidates?.reduce((cAcc, c) => {
                    if (c.pollId != p.pollId) return cAcc;
                    if (!cAcc) {
                        cAcc = [c];
                    } else {
                        cAcc.push(c);
                    }
                    return cAcc;
                }, null);
                p.candidates = candidates;
                p.regionName = region.name;
                return p;
            });

            acc.push(...regionPolls);
        }
        return acc;
    }, [election, user]);

    const handleStep = (toPrev) => {
        if (toPrev) {
            if (stepIndex === 0) return props.onClose?.();
            return setStepIndex(idx => --idx);
        }
        if (stepIndex === polls.length) {
            return props.onClose?.();
        }
        return setStepIndex(idx => ++idx);
    }

    const getGraphSpacing = (num) => {
        return counter((isMinWidthMd ? Math.min(8, window.innerWidth / 150) : 6) - num, i => ({
            name: String().padEnd(i, ' '),
            votes: 0, id: i
        }))
    }

    return (
        <Dialog
            open={props.open} close={props.onClose} fullWidth
            title='Your Vote' disableEscapeKeyDown
            saveButtonLabel={stepIndex !== polls.length ? 'Next' : 'Exit'}
            onSave={_ => handleStep()} onClose={_ => handleStep(true)}
            cancelButtonLabel={stepIndex > 0 ? 'Previous' : 'Exit'}
            breakpoint='md'
        >
            <Stepper activeStep={stepIndex} orientation='vertical'>
                {polls.map(p => (
                    <Step key={p.pollId}>
                        <StepLabel >{p.position} ({p.regionName})</StepLabel>
                        <StepContent>
                            {p.candidates ?
                                <ResultsBarGraph candidates={p.candidates} />:
                                <Typography>
                                    There were no candidates in this poll
                                </Typography>
                            }
                        </StepContent>
                    </Step>
                ))}
                <Step>
                    <StepLabel unselectable='on' >Full Results</StepLabel>
                    <StepContent>
                        <div id="bar-container">
                            <ResponsiveBar
                                data={[
                                    ...polls.map(p => {
                                        let c;
                                        (p.candidates || []).forEach(cdt => {
                                            if (cdt.votes > (c?.votes || 0)) {
                                                c = cdt;
                                            }
                                        });
                                        if (!c) {
                                            return { name: `null_${p.pollId}`, votes: 0 }
                                        }
                                        return {
                                            name: c.name.split(' ').join('\n'), votes: c.votes,
                                            id: c.candidateId
                                        }
                                    }),
                                    ...getGraphSpacing(polls?.length),
                                ]}
                                layout={isMinWidthMd ? 'vertical' : 'horizontal'}
                                keys={['votes']}
                                indexBy='name'
                                colors={{ scheme: "nivo" }}
                                colorBy='index'
                                margin={{
                                    left: 60, bottom: 50, right: 20,
                                }}
                                labelSkipWidth={12}
                                labelSkipHeight={12}
                            />
                        </div>
                    </StepContent>
                </Step>
            </Stepper>
        </Dialog>
    )
}


/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialog-paper{
        #bar-container {
            height: calc(100vh - 400px);
            min-height: 300px;
            svg text {
                white-space: pre;
            }
        }

        &:not(.MuiDialog-paperFullScreen) {
            height: 90%;
        }
        &.MuiDialog-paperFullScreen {
            .MuiDialogContent-root {
                padding: 0;
            }
            .MuiStepper-root{
                padding: 16;
                .MuiStepContent-root{
                    padding-left: 0;
                }
            }
        }
    }
`;


ResultDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default ResultDialog;