import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Typography, Button } from '@material-ui/core';
import { useRouter } from 'flitlib';
import { useSelector } from 'react-redux';
import VoteDialog from './VoteDialog';
import moment from 'moment';

function VotingPage(props) {
    const [voteDialogOpen, setVoteDialogOpen] = useState(false);
    const election = useSelector(state => state.election);
    const user = useSelector(state => state.user);
    const { changePage } = useRouter();

    const getRemainingTime = end => {
        if (!end) return 'Invalid Date';
        let rTime = new Date(end) - new Date();
        const oneMin = 1000 * 60;
        const oneHr = oneMin * 60;
        const oneDay = oneHr * 24;
        const days = Math.floor(rTime / oneDay);
        rTime %= oneDay;
        const hours = Math.floor(rTime / oneHr);
        rTime %= oneHr;
        const min = Math.floor(rTime / oneMin);

        return `Ends in \t ${days}d: ${hours}h: ${min}min`;
    }

    const [remTime, setRemTime] = useState(getRemainingTime(election?.endDate));
    useEffect(() => {
        if (!election) return;
        setRemTime(getRemainingTime(election?.endDate));
        const timer = window.setInterval(() => {
            setRemTime(getRemainingTime(election?.endDate));
        }, 60000)
        return () => {
            window.clearInterval(timer);
        }
    }, [election]);


    return (
        <Wrapper>
            {user.hasVoted &&
                <div className='info votes' >
                    <Typography variant='h3' color='primary' >
                        {'You have voted.'}
                    </Typography>
                    <Typography variant='h4' >
                        {`Votes cast : ${election?.votesNum || 0}`}
                    </Typography>
                </div>
            }
            {new Date(election?.startDate) > new Date() ?
                <div className='info' >
                    <Typography variant='h4' >
                        Starts {moment(election.startDate).fromNow()}
                    </Typography>
                </div> :
                <>
                    <div className='info' >
                        <Typography variant='h4' >
                            {remTime}
                        </Typography>
                    </div>
                    {!user?.hasVoted &&
                        <div className='info' >
                            <Button
                                id='start-button' color='primary' size='large' variant='contained'
                                // onClick={e => changePage(`./poll`, { matchParent: true })}
                                onClick={e => setVoteDialogOpen(true)}
                            >
                                Begin
                        </Button>
                        </div>
                    }
                </>

            }

            <VoteDialog
                open={voteDialogOpen} onClose={_ => setVoteDialogOpen(false)}
            />
        </Wrapper>
    )
}



const Wrapper = styled.div`
    >.info {
        &.votes {
            margin-bottom: 3rem;
        }
        >#start-button {
            align-self: stretch;
            padding: 1rem;
        }
    }
`;

VotingPage.propTypes = {

}

export default VotingPage

