import React, { useState, useMemo, } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GenericDialog from 'Components/GenericDialog';
import { useSelector, useDispatch } from 'react-redux';
import { Stepper, Step, StepLabel, StepContent, Typography, Checkbox, ListItem, ListItemText, IconButton } from '@material-ui/core';
import CustomAvatar from 'Components/CustomAvatar';
import EditIcon from '@material-ui/icons/Edit';
import { xFetch } from 'flitlib';
import { addNotification, editNotification, logout, fetchData, ACTIONS } from 'Redux/actions';

function VoteDialog(props) {
    const [stepIndex, setStepIndex] = useState(0);
    const [pollVotes, setPollVotes] = useState({});
    const election = useSelector(state => state.election);
    const user = useSelector(state => state.user);

    const polls = useMemo(() => {
        const len = election?.regions?.length;
        if (!len) return null;
        const acc = [];
        let regionAreaId = user.regionAreaId;
        for (let i = len - 1; i > -1; i--) {
            // Starts from the lowest region to get RegionArea where voter is registered
            const region = election.regions[i];
            const regionArea = region.regionAreas?.find(ra =>
                ra.regionAreaId == regionAreaId
            );
            if (!regionArea) continue;
            regionAreaId = regionArea.parentRegionAreaId;
            const regionPolls = region.polls.map(p => {
                const candidates = regionArea.candidates?.reduce((cAcc, c) => {
                    if (c.pollId != p.pollId) return cAcc;
                    if (!cAcc) {
                        cAcc = [c];
                    } else {
                        cAcc.push(c);
                    }
                    return cAcc;
                }, null);
                p.candidates = candidates;
                p.regionName = regionArea.name;
                return p;
            });

            acc.push(...regionPolls);
        }
        return acc;
    }, [election, user]);



    const dispatch = useDispatch();
    const saveVote = async e => {
        e && e.preventDefault();
        const key = dispatch(addNotification({
            message: 'Saving vote',
            action: null,
            persist: true,
        }));
        const finalPollVotes = Object.values(pollVotes).map(({ pollId, candidateId }) =>
            ({ pollId, candidateId })
        );
        // TODO will be sent to all peer servers
        try {
            await xFetch('/voters/election/vote', {
                method: 'POST',
                body: {
                    electionId: election.electionId,
                    pollVotes: finalPollVotes,
                    voterHash: user.voterHash,
                    castDate: new Date()
                }
            });
            const peerVotes = (election.peerServerUris || []).map(peer => {
                return xFetch(`${peer}/voters/election/vote`, {
                    method: 'POST',
                    body: {
                        electionId: election.electionId,
                        pollVotes: finalPollVotes,
                        voterHash: user.voterHash,
                        castDate: new Date()
                    }
                });
            })
            await Promise.all(peerVotes.map(p => p.then(
                (value) => ({ value, status: "fulfilled" }),
                err => ({ err, status: "rejected" }))
            ));

            dispatch(editNotification({
                key,
                message: 'Vote successfully saved. Thank you for voting.',
            }));
            dispatch(fetchData());
            return props.onClose?.();
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));
        }
    }


    const handleStep = (toPrev) => {
        if (toPrev) {
            if (stepIndex === 0) return props.onClose?.();
            return setStepIndex(idx => --idx);
        }
        if (stepIndex === polls.length) {
            return saveVote();
        }
        const poll = polls[stepIndex];
        if (!pollVotes[poll.pollId] && poll.candidates?.length && !election.allowNullVotes) {
            alert('Please make a vote to proceed');
            return;
        }
        return setStepIndex(idx => ++idx);
    }

    return (
        <Dialog
            open={props.open} fullWidth title='Your Vote' disableEscapeKeyDown
            saveButtonLabel={stepIndex !== polls.length ? 'Next' : 'Submit Vote'}
            onSave={_ => handleStep()} onClose={_ => handleStep(true)}
            cancelButtonLabel={stepIndex > 0 ? 'Previous' : 'Cancel'}
            breakpoint='sm'
        >
            {/* TODO Make step label selectable */}
            {/* TODO Make responsive */}
            <Stepper activeStep={stepIndex} orientation='vertical'>
                {polls.map(p => (
                    <Step key={p.pollId}>
                        <StepLabel >{p.position} ({p.regionName})</StepLabel>
                        <StepContent>
                            {p.candidates?.map(c =>
                                <ListItem
                                    className="candidate-container" button
                                    key={c.candidateId}
                                    onClick={e => {
                                        setPollVotes(pv => ({ ...pv, [p.pollId]: c }));
                                    }}
                                    selected={pollVotes[p.pollId]?.candidateId === c.candidateId}
                                >
                                    <CustomAvatar
                                        src={c.profilePicUrl} name={c.name}
                                    />
                                    <div id="details">
                                        <Typography>
                                            {c.name}
                                        </Typography>
                                        {c.deputyName &&
                                            <Typography variant='caption' >
                                                Deputy: {c.deputyName}
                                            </Typography>
                                        }
                                        {c.party &&
                                            <Typography variant='caption' >
                                                Party: {c.party}
                                            </Typography>
                                        }
                                    </div>
                                    <Checkbox
                                        color='primary'
                                        checked={pollVotes[p.pollId]?.candidateId === c.candidateId}
                                    />
                                </ListItem>
                            ) ||
                                <Typography>
                                    No Candidates set for this poll
                                </Typography>
                            }
                        </StepContent>
                    </Step>
                ))}
                <Step>
                    <StepLabel unselectable='on' >Vote Confirmation</StepLabel>
                    <StepContent>
                        {polls.map((p, i) => {
                            const c = pollVotes[p.pollId] || {};
                            return (
                                <>
                                    <Typography variant='body2' key={p.pollId + '1'} >
                                        {p.position} ({p.regionName})
                                    </Typography>
                                    <ListItem
                                        className="candidate-container" key={p.pollId + '2'}
                                        onClick={e => {
                                            setPollVotes(pv => ({ ...pv, [p.pollId]: c }));
                                        }}
                                    >
                                        <CustomAvatar
                                            src={c.profilePicUrl} name={c.name}
                                        />
                                        {c.name ?
                                            <ListItemText id="details" >
                                                <Typography>
                                                    {c.name}
                                                </Typography>
                                                {c.deputyName &&
                                                    <Typography variant='caption' >
                                                        Deputy: {c.deputyName}
                                                    </Typography>
                                                }
                                                {c.party &&
                                                    <Typography variant='caption' >
                                                        Party: {c.party}
                                                    </Typography>
                                                }
                                            </ListItemText> :
                                            <Typography id="details" color='textSecondary' >
                                                None Selected
                                            </Typography>
                                        }
                                        <IconButton onClick={e => setStepIndex(i)} >
                                            <EditIcon />
                                        </IconButton>
                                    </ListItem>
                                </>
                            );
                        })}
                    </StepContent>
                </Step>
            </Stepper>
        </Dialog>
    )
}


/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialog-paper{
        .candidate-container {
            display: flex;
            align-items: center;
            width: 100%;
            #details {
                flex: 1;
                margin-left: 2rem;
                > * {
                    display: block;
                }
            }
        }

        &:not(.MuiDialog-paperFullScreen) {
            height: 90%;
        }
        &.MuiDialog-paperFullScreen {
            .MuiDialogContent-root {
                padding: 0;
            }
            .MuiStepper-root{
                padding: 16;
                .MuiStepContent-root{
                    padding-left: 0;
                }
            }
            .candidate-container #details {
                margin-left: 1rem;
            }
        }
    }
`;


VoteDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default VoteDialog;