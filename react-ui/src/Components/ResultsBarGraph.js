import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ResponsiveBar } from '@nivo/bar';
import { useMediaQuery } from '@material-ui/core';
import { counter } from 'flitlib';

function ResultsBarGraph(props) {
    const { candidates } = props;
    const isMinWidthMd = useMediaQuery(theme => theme.breakpoints.up('sm'));

    const getGraphSpacing = (num) => {
        return counter((isMinWidthMd ? Math.min(8, window.innerWidth / 150) : 6) - num, i => ({
            name: String().padEnd(i, ' '),
            votes: 0, id: i
        }))
    }

    return (
        <Wrapper>
            <ResponsiveBar
                data={[
                    ...candidates?.map(c => ({
                        name: c.name.split(' ').join('\n'), votes: c.votes,
                        id: c.candidateId
                    })),
                    ...getGraphSpacing(candidates?.length),
                ]}
                layout={isMinWidthMd ? 'vertical' : 'horizontal'}
                keys={['votes']}
                indexBy='name'
                colors={{ scheme: "nivo" }}
                colorBy='index'
                margin={{
                    left: 60, bottom: 50, right: 20, top: 50
                }}
                labelSkipWidth={12}
                labelSkipHeight={12}
                isInteractive={isMinWidthMd}
            />
        </Wrapper>
    )
}

const Wrapper = styled.div`
    height: calc(100vh - 400px);
    min-height: 300px;
    svg text {
        white-space: pre;
    }
`;

ResultsBarGraph.propTypes = {
    candidates: PropTypes.arrayOf(PropTypes.object,),
}

export default ResultsBarGraph

