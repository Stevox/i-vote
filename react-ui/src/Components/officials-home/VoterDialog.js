import VoterDialogContent from 'Components/data-dialog-content/VoterDialogContent';
import GenericDialog from 'Components/GenericDialog';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';


function VotersDialog(props) {
    const { election, } = props;
    
    if (!election) return null;
    return (
        <Dialog
            title='Voters'
            hideSaveButton cancelButtonLabel='Back'
            open={props.open} onClose={props.onClose} fullWidth breakpoint='sm'
        >
            <VoterDialogContent 
                election={election}
            />
        </Dialog>
    )
}



/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
`;





VotersDialog.propTypes = {
    election: PropTypes.object,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default VotersDialog;