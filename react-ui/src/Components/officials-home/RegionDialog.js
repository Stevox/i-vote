import GenericDialog from 'Components/GenericDialog';
import PropTypes from 'prop-types';
import React, { useState, useMemo, useEffect } from 'react';
import styled from 'styled-components';
import { Tabs, Tab, AppBar } from '@material-ui/core';
import RegionAreasDialogContent from 'Components/data-dialog-content/RegionAreasDialogContent';
import PollsDialogContent from 'Components/data-dialog-content/PollsDialogContent';
import CandidatesDialogContent from 'Components/data-dialog-content/CandidatesDialogContent';


function RegionDialog(props) {
    const { election, region } = props;
    const [selectedIndex, setSelectedIndex] = useState(0);

    useEffect(() => {
        setSelectedIndex(0);
    }, [props.open]);

    const parentRegion = useMemo(() => {
        return election?.regions?.find(r =>
            r.regionId == region?.parentRegionId
        )
    }, [election, region]);

    
    const getTabContent = idx => {
        switch(idx){
            case 0: return (
                <RegionAreasDialogContent 
                    region={region} parentRegion={parentRegion}
                />
            );
            case 1: return (
                <PollsDialogContent
                    election={election} region={region}
                />
            );
            case 2: return (
                <CandidatesDialogContent
                    election={election} region={region}
                />
            );
            default: return null;
        }
    }
    
    if (!region) return null;
    return (
        <Dialog
            title={`Region: ${region.name}`}
            hideSaveButton cancelButtonLabel='Back'
            open={props.open} onClose={props.onClose} fullWidth breakpoint='sm'
            showCloseButton
        >
            <AppBar position='static' >
                <Tabs value={selectedIndex} onChange={(_, idx) => setSelectedIndex(idx) } >
                    <Tab label='Region Areas' />
                    <Tab label='Polls' />
                    <Tab label='Candidates' />
                </Tabs>
            </AppBar>
            <div id="tab-panel">
                {getTabContent(selectedIndex)}
            </div>
        </Dialog>
    )
}



/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialog-paper:not(.MuiDialog-paperFullScreen) {
        height: 90%;
    }
    #tab-panel {
        margin-top: 1rem;
        overflow-y: auto;
        height: calc(100% - ${(({theme}) => theme.mixins.toolbar.minHeight)}px);
    }
`;





RegionDialog.propTypes = {
    election: PropTypes.object,
    region: PropTypes.object,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default RegionDialog;