import React, { useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GenericDialog from 'Components/GenericDialog';
import ResultsBarGraph from 'Components/ResultsBarGraph';
import { Typography } from '@material-ui/core';

function RegionPollResultDialog(props) {
    const { poll, region } = props;
    const [selectedRegionArea, setSelectedRegionArea] = useState(region?.regionAreas?.[0]);

    const candidates = useMemo(() => {
        return (selectedRegionArea?.candidates || []).filter(c => c.pollId == poll?.pollId)
    }, [selectedRegionArea, poll]);

    return (
        <Wrapper
            title={`${poll?.position} Poll Results`} showCloseButton open={props.open}
            onClose={props.onClose} cancelButtonLabel='Back' hideSaveButton fullWidth
            breakpoint='sm'
        >
            {region?.regionAreas?.length > 1 ?
                <div className='sel-field' >
                    <Typography>Region Area</Typography>
                    <select
                        name="region-area" id="region-area-sel" value={selectedRegionArea?.regionAreaId}
                        onChange={e => {/* Handled by the options */ }}
                    >
                        {(region?.regionAreas || []).map(ra =>
                            <option
                                value={ra.regionAreaId} key={ra.regionAreaId}
                                onClick={e => setSelectedRegionArea(ra)}
                            >
                                {ra.name}
                            </option>
                        )}
                    </select>
                </div> :
                <Typography>{region?.regionAreas?.[0].name}</Typography>
            }
            <ResultsBarGraph candidates={candidates} />
        </Wrapper>
    )
}

/** @type {GenericDialog} */
const Wrapper = styled(GenericDialog)`
    .sel-field {
        display: grid;
        grid-template-columns: 1fr 2fr;
    }
`;

RegionPollResultDialog.propTypes = {
    region: PropTypes.object,
    poll: PropTypes.object,
}

export default RegionPollResultDialog;

