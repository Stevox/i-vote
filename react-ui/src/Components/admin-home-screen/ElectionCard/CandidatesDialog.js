import { IconButton, Menu, MenuItem, TextField, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CandidatesDialogContent from 'Components/data-dialog-content/CandidatesDialogContent';
import GenericDialog from 'Components/GenericDialog';
import ProfilePicture from 'Components/ProfilePicture';
import { useValidator, xFetch } from 'flitlib';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addNotification, editNotification, fetchData, ACTIONS } from 'Redux/actions';
import styled from 'styled-components';

function CandidatesDialog(props) {
    const { election } = props;
    const [selectedCandidate, setSelectedCandidate] = useState(null);
    const [selectedRegionArea, setSelectedRegionArea] = useState(null);
    const [anchorEl, setAnchorEl] = useState(null);
    const [newDialogOpen, setNewDialogOpen] = useState(false);


    useEffect(() => {
        if (!election || !selectedRegionArea) return;
        for (let r of election.regions) {
            for (let ra of r.regionAreas) {
                if (ra.regionAreaId == selectedRegionArea.regionAreaId) {
                    setSelectedRegionArea(ra);
                    return;
                }
            }
        }
    }, [election]);

    const getTemplate = async e => {
        await xFetch('/elections/candidates/template');
    }

    const getRecords = async e => {
        await xFetch('/elections/candidates', {
            body: {
                electionId: election.electionId,
            }
        });
    }

    const dispatch = useDispatch();
    const deleteCandidate = async ({ candidateId }) => {
        if (!window.confirm('Are you sure you want to delete this candidate?')) return;

        const key = dispatch(addNotification({
            message: 'Deleting Candidate',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections/candidates', {
                method: 'DELETE',
                body: {
                    candidateId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Candidate deleted',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }

    const saveFile = async (e) => {
        e.preventDefault();
        const file = e.target.files[0];
        if (!file) return;
        if (!window.confirm('This will replace the current list of candidates. Proceed?')) return;

        const key = dispatch(addNotification({
            message: 'Uploading file',
            persist: true,
            action: null,
        }));
        const form = new FormData();
        form.set('file', file);
        form.set('electionId', election.electionId);
        try {
            await xFetch('/elections/candidates', {
                method: 'POST',
                body: form,
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'File uploaded',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }


    if (!election?.regions?.some(r => !!r.polls?.length)) {
        if (props.open) {
            props.onClose();
            alert('Set up at least one poll');
        }
        return null
    };
    return (
        <Dialog
            title={
                <>
                    <Typography variant='h4' >
                        Candidates
                    </Typography>
                    <IconButton onClick={e => setAnchorEl(e.target)} >
                        <MoreVertIcon />
                    </IconButton>
                </>
            } titleProps={{ disableTypography: true }}
            hideSaveButton cancelButtonLabel='Back'
            open={props.open} onClose={props.onClose} fullWidth
            breakpoint='sm'
        >
            <CandidatesDialogContent
                election={election} canEdit
                onEditCandidate={(c, { regionArea }) => {
                    setSelectedCandidate(c);
                    setSelectedRegionArea(regionArea);
                    setNewDialogOpen(true);
                }}
                onDeleteCandidate={deleteCandidate}
            />

            <Menu
                open={!!anchorEl} anchorEl={anchorEl}
                onClose={() => setAnchorEl(null)}
            >
                <MenuItem onClick={getTemplate} >
                    Download Upload Template
                </MenuItem>
                <MenuItem onClick={getRecords} >
                    Download Records
                </MenuItem>
                <label htmlFor="file-input">
                    <MenuItem>
                        Upload File
                    </MenuItem>
                </label>
                <MenuItem onClick={e => {
                    setSelectedCandidate(null);
                    setNewDialogOpen(true);
                }} >
                    Add New
                </MenuItem>
            </Menu>

            <input type="file" id='file-input' hidden onChange={saveFile} />

            <NewCandidateDialog
                open={newDialogOpen} onClose={_ => setNewDialogOpen(false)}
                candidate={selectedCandidate} election={election}
                regionArea={selectedRegionArea}
            />
        </Dialog>
    )
}


/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialogTitle-root {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
`;



function NewCandidateDialog(props) {
    const { candidate, election, regionArea } = props;
    const [selectedRegion, setSelectedRegion] = useState(null);
    const { values, setValue, reset } = useValidator();

    useEffect(() => {
        if (!props.open) return;
        if (props.regionArea) {
            setSelectedRegion(election?.regions?.find(r => r.regionId == props.regionArea.regionId));
        } else {
            setSelectedRegion(null);
        }
        reset(candidate);
    }, [props.open, candidate]);


    const dispatch = useDispatch();
    const saveCandidate = async e => {
        e.preventDefault();
        const key = dispatch(addNotification({
            message: 'Saving candidate',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections/candidates', {
                method: values.candidateId ? 'PUT' : 'POST',
                body: {
                    ...values,
                    electionId: election.electionId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Candidate saved',
            }));
            props.onClose();
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }

    return (
        <NewDialog
            wrapForm title='New Candidate' maxWidth='sm' onClose={props.onClose}
            onSave={saveCandidate} open={props.open} fullWidth
        >
            <ProfilePicture
                cropSize={180}
                onImageLoaded={({ data }) => {
                    // TODO handle save
                }}
                frameFormat='circle'
            />
            <TextField
                label={`Name`} required name='name'
                value={values.name} onChange={setValue} autoFocus
            />
            <TextField
                label={`Party`} name='party'
                value={values.party} onChange={setValue}
            />
            <TextField
                label={`Deputy Name`} name='deputyName'
                value={values.deputyName} onChange={setValue}
            />
            <div className='sel-field' >
                <label htmlFor="region-sel">
                    Region*
                </label>
                <select
                    name="regionId" id="region-sel" required
                    value={values.regionId}
                    onChange={e => {
                        const region = election.regions?.find(r =>
                            r.regionId == e.target.value
                        );
                        setSelectedRegion(region);
                        setValue(e);
                    }}
                >
                    <option disabled selected value='' > -- select an option -- </option>
                    {election.regions?.map(r =>
                        <option
                            key={r.regionId} value={r.regionId}
                        >
                            {r.name}
                        </option>
                    )}
                </select>
            </div>
            {selectedRegion &&
                <div className='sel-field' >
                    <label htmlFor="position-sel">
                        Position*
                    </label>
                    <select
                        name="pollId" id="position-sel" required
                        value={values.pollId} onChange={setValue}
                    >
                        <option disabled selected value='' > -- select an option -- </option>
                        {selectedRegion.polls?.map((p) =>
                            <option key={p.pollId} value={p.pollId} >
                                {p.position}
                            </option>
                        )}
                    </select>
                </div>
            }
            {selectedRegion &&
                <div className='sel-field' >
                    <label htmlFor="ra-sel">
                        {selectedRegion.name}*
                    </label>
                    <select
                        name="regionAreaId" id="ra-sel" required
                        value={values.regionAreaId} onChange={setValue}
                    >
                        <option disabled selected value='' > -- select an option -- </option>
                        {selectedRegion.regionAreas?.map(ra =>
                            <option key={ra.regionAreaId} value={ra.regionAreaId} >
                                {ra.name} {ra.code ? `(${ra.code})` : ''}
                            </option>
                        )}
                    </select>
                </div>
            }
        </NewDialog>
    );
}


/**@type {GenericDialog} */
const NewDialog = styled(GenericDialog)`
    .MuiDialogContent-root form {
        display: grid;
        row-gap: 1rem;
        padding-bottom: 3rem;
        .sel-field {
            display: grid;
            grid-template-columns: 1fr 2fr;
        }

    }
`;



CandidatesDialog.propTypes = {
    election: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default CandidatesDialog;