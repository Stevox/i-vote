import { Collapse, Divider, IconButton, Menu, MenuItem, Paper, Typography, useMediaQuery, Card, CardHeader, CardContent, Icon } from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MenuIcon from '@material-ui/icons/Menu';
import { xFetch } from 'flitlib';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useState, useMemo, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { addNotification, editNotification, fetchData, ACTIONS } from 'Redux/actions';
import styled from 'styled-components';
import CandidatesDialog from './CandidatesDialog';
import RegionAreasDialog from './RegionAreasDialog';
import PollsDialog from './PollsDialog';
import VotersDialog from './VotersDialog';
import LiveIcon from '@material-ui/icons/PlayCircleFilled';


function ElectionCard(props) {
    const { election } = props;
    const [anchorEl, setAnchorEl] = useState(null);
    const [expanded, setExpanded] = useState(true);
    const [regionAreasDialogOpen, setRegionAreasDialogOpen] = useState(false);
    const [pollDialogOpen, setPollDialogOpen] = useState(false);
    const [candidatesDialogOpen, setCandidatesDialogOpen] = useState(false);
    const [votersDialogOpen, setVotersDialogOpen] = useState(false);
    const isMinWidthMd = useMediaQuery(theme => theme.breakpoints.up('sm'));


    useEffect(() => {
        if(!election) return;
        if(new Date(election.endDate) < new Date()) setExpanded(false);
    }, [election]);
    
    const dispatch = useDispatch();
    const deleteElection = async e => {
        e.preventDefault();
        if (!window.confirm("Are you sure you want to delete this election?")) return;
        const key = dispatch(addNotification({
            message: 'Deleting election',
            persist: true,
            action: null,
        }));
        const { electionId } = election;
        try {
            await xFetch('/elections', {
                method: 'DELETE',
                body: {
                    electionId
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Election deleted',
            }))
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
            }))
        }
    }

    const goLive = async e => {
        e.preventDefault();
        if (!window.confirm("Once the election goes live, no more edits can be made. Proceed?")) return;
        const key = dispatch(addNotification({
            message: 'Going live',
            persist: true,
            action: null,
        }));
        const { electionId } = election;
        try {
            await xFetch('/elections/go-live', {
                method: 'POST',
                body: {
                    electionId
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Election is now live',
            }))
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
            }))
        }
    }

    const cancelElection = async e => {
        e.preventDefault();
        if (!window.confirm("Are you sure? Once the election is cancelled, "+
            "all votes made will be invalidated and voters will be notified "+
            "of the election's cancellation")
        ) return;

        const key = dispatch(addNotification({
            message: 'Cancelling election',
            persist: true,
            action: null,
        }));
        const { electionId } = election;
        try {
            await xFetch('/elections', {
                method: 'DELETE',
                body: {
                    electionId
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Election cancelled',
            }))
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
            }))
        }
    }

    const candidatesNumArr = useMemo(() => election.regions?.map(r => r.regionAreas?.reduce((acc, ra) =>
        acc + (ra.candidates?.length || 0), 0
    )) || [], [election]);


    if (!election) return null;
    const {
        title, description, electionCode, voters,
        startDate, endDate, regions, isLive,
    } = election;
    return (
        <Wrapper>
            <div id="header">
                <Typography variant='h4' >
                    {isLive &&
                        <Icon color='green' >
                            <LiveIcon />
                        </Icon>
                    }
                    {title}
                </Typography>
                <Typography variant='body1' >
                    <b>Election Code:</b> {electionCode}
                </Typography>
                {expanded &&
                    <Divider />
                }
            </div>
            <div id="top-buttons">
                <IconButton onClick={() => setExpanded(exp => !exp)} >
                    {expanded ?
                        <ExpandLessIcon /> :
                        <ExpandMoreIcon />
                    }
                </IconButton>
                <IconButton onClick={e => setAnchorEl(e.target)} >
                    <MenuIcon />
                </IconButton>
            </div>
            <Collapse in={!expanded} unmountOnExit >
                {new Date() > new Date(startDate) ?
                    <Typography>{`Voting ended ${moment(endDate).fromNow()}`}</Typography> :
                    <Typography>{`Voting is set to start ${moment(startDate).fromNow()}`}</Typography>
                }
            </Collapse>

            <Collapse in={expanded} unmountOnExit >
                <Typography variant='body1' >{description}</Typography>
                <div id='registration' >
                    <Typography className='title' variant='h5' >Registered Voters:</Typography>
                    <Typography variant='body1' >{voters?.length || 0}</Typography>
                </div>
                <div id='period'>
                    <Typography className='title' variant='h5' >Voting Period:</Typography>
                    <div>
                        <div className='boxed-div' >{!isMinWidthMd && 'Start: '} {moment(startDate).format('Do MMMM YYYY (hh:mm a)')}</div>
                        {isMinWidthMd &&
                            <div className='bolded' >--</div>
                        }
                        <div className='boxed-div' >{!isMinWidthMd && 'End: '} {moment(endDate).format('Do MMMM YYYY (hh:mm a)')}</div>
                    </div>
                </div>
                <div id="regions">
                    <Typography className='title' variant='h5' >Regions:</Typography>
                    {isMinWidthMd ?
                        <table>
                            <thead>
                                <tr>
                                    <th>Region Name</th>
                                    <th>Region Areas</th>
                                    <th>Polls</th>
                                    <th>Candidates</th>
                                </tr>
                            </thead>
                            <tbody>
                                {regions?.map((r, i) =>
                                    <tr key={r.regionId} >
                                        <td>{r.name}</td>
                                        <td>{r.regionAreas?.length || 0}</td>
                                        <td>{r.polls?.length || 0}</td>
                                        <td>{candidatesNumArr[i] || 0}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table> :
                        <div id="cards">
                            {regions?.map((r, i) =>
                                <Card variant='elevation' key={r.regionId} elevation={2} >
                                    <CardHeader
                                        title={r.name}
                                        titleTypographyProps={{
                                            variant: 'h6',
                                        }}
                                    >
                                    </CardHeader>
                                    <CardContent>
                                        <Typography><b>Region Areas:</b> {r.regionAreas?.length || 0}</Typography>
                                        <Typography><b>Polls:</b> {r.polls?.length || 0}</Typography>
                                        <Typography><b>Candidates:</b> {candidatesNumArr[i] || 0}</Typography>
                                    </CardContent>
                                </Card>
                            )}
                        </div>
                    }
                </div>
            </Collapse>

            <Menu
                open={!!anchorEl} anchorEl={anchorEl}
                onClose={() => setAnchorEl(null)}
            >
                {!isLive ?
                    [
                        <MenuItem
                            key='Edit Election'
                            onClick={e => {
                                setAnchorEl(null);
                                props.onEdit(e);
                            }}
                        >
                            Edit Election
                        </MenuItem>,
                        <MenuItem
                            key='Set Region Areas'
                            onClick={e => {
                                setAnchorEl(null);
                                setRegionAreasDialogOpen(true);
                            }}
                        >
                            Set Region Areas
                        </MenuItem>,
                        <MenuItem
                            key='Set Polls'
                            onClick={e => {
                                setAnchorEl(null);
                                setPollDialogOpen(true);
                            }}
                        >
                            Set Polls
                        </MenuItem>,
                        <MenuItem
                            key='Set Candidates'
                            onClick={e => {
                                setAnchorEl(null);
                                setCandidatesDialogOpen(true);
                            }}
                        >
                            Set Candidates
                        </MenuItem>,
                        <MenuItem
                            key='Set Voters'
                            onClick={e => {
                                setAnchorEl(null);
                                setVotersDialogOpen(true);
                            }}
                        >
                            Set Voters
                        </MenuItem>,
                        <MenuItem
                            key='Delete Election'
                            onClick={e => {
                                setAnchorEl(null);
                                deleteElection(e);
                            }}
                        >
                            Delete Election
                        </MenuItem>,
                        <MenuItem
                            key='Go Live'
                            onClick={e => {
                                setAnchorEl(null);
                                goLive(e);
                            }}
                        >
                            Go Live
                        </MenuItem>,
                    ] :
                    new Date(election.endDate) > new Date() ?
                        <MenuItem
                            onClick={e => {
                                setAnchorEl(null);
                                cancelElection(e);
                            }}
                        >
                            Cancel Election
                        </MenuItem>:
                        <MenuItem
                            onClick={e => {
                                setAnchorEl(null);
                                alert('Not implemented');
                            }}
                        >
                            View Results
                        </MenuItem>
                }
            </Menu>

            <RegionAreasDialog
                open={regionAreasDialogOpen} onClose={_ => setRegionAreasDialogOpen(false)}
                election={election}
            />

            <PollsDialog
                open={pollDialogOpen} onClose={_ => setPollDialogOpen(false)}
                election={election}
            />

            <CandidatesDialog
                open={candidatesDialogOpen} onClose={_ => setCandidatesDialogOpen(false)}
                election={election}
            />

            <VotersDialog
                open={votersDialogOpen} onClose={_ => setVotersDialogOpen(false)}
                election={election}
            />

        </Wrapper>
    )
}



const Wrapper = styled(Paper)`
    width: 90%;
    display: flex;
    flex-direction: column;
    position: relative;
    margin-bottom: 30px;
    padding: 1rem;
    .title {
        font-weight: 450;
    }
    .MuiDivider-root {
        margin: 0 -1rem;
    }
    #header {
        >:first-child {
            font-weight: bold;
            .MuiIcon-root {
                color: ${({theme}) => theme.palette.success.main };
                margin-right: 1rem;
            }
        }
        >:nth-child(2) {
            font-style: italic;
        }
        margin-bottom: 1rem;
    }
    #top-buttons {
        position: absolute;
        top: 0;
        right: 0;
        display: flex;
    }
    #registration {
        margin-top: 1rem;
        display: flex;
        align-items: center;
        >:not(:first-child) {
            margin-left: 1rem;
        }
        /* grid-template-columns: 1fr 1fr; */
    }
    #period {
        margin-top: 1rem;
        display: grid;
        >*:nth-child(2) {
            padding-left: 1rem;
            width: max-content;
            display: grid;
            justify-items: center;
            grid-template-columns: 1fr 40px 1fr;
            align-items: center;
            @media (max-width: ${({ theme }) => theme.breakpoints.values.sm}px){
                padding-left: .4rem;
                grid-template-columns: 1fr;
            }
        }
    }
    #regions {
        margin-top: 1rem;
        >#cards {
            display: grid;
            grid-template-columns: repeat(auto-fit, 180px);
            gap: 1rem;
            justify-content: center;
            .MuiCardContent-root {
                padding-top: 0;
            }
        }
        >table {
            margin-top: .5rem;
            border-collapse: collapse;
            margin-left: 1rem;
            width: calc(100% - 2rem);
            max-width: 640px;
            td, th {
                border: 1px solid;
                padding: 1rem;
                text-align: center;
            }
        }
    }
`;

ElectionCard.propTypes = {
    election: PropTypes.object.isRequired,
    onEdit: PropTypes.func.isRequired,
}

export default ElectionCard

