import React, { useMemo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GenericDialog from 'Components/GenericDialog';
import { TextField, Menu, MenuItem, IconButton, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useDispatch } from 'react-redux';
import { useValidator, xFetch } from 'flitlib';
import { editNotification, addNotification, fetchData, ACTIONS } from 'Redux/actions';
import VoterDialogContent from 'Components/data-dialog-content/VoterDialogContent';


function VotersDialog(props) {
    const { election, } = props;
    const [selectedVoter, setSelectedVoter] = useState(null);
    const [anchorEl, setAnchorEl] = useState(null);
    const [newDialogOpen, setNewDialogOpen] = useState(false);

    const getTemplate = async e => {
        await xFetch('/elections/voters/template', {
            body: {
                electionId: election.electionId,
            }
        });
    }

    const getRecords = async e => {
        await xFetch('/elections/voters', {
            body: {
                electionId: election.electionId,
            }
        });
    }

    const dispatch = useDispatch();
    const deleteVoter = async ({ voterId }) => {
        if (!window.confirm('Are you sure you want to delete this voter?')) return;

        const key = dispatch(addNotification({
            message: 'Deleting Voter',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections/voters', {
                method: 'DELETE',
                body: {
                    voterId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Voter deleted',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }

    const saveFile = async (e) => {
        e.preventDefault();
        const file = e.target.files[0];
        if (!file) return;
        if (!window.confirm('This will replace the current list of voters. Proceed?')) return;

        const key = dispatch(addNotification({
            message: 'Uploading file',
            persist: true,
            action: null,
        }));
        const form = new FormData();
        form.set('file', file);
        form.set('electionId', election.electionId);
        try {
            await xFetch('/elections/voters', {
                method: 'POST',
                body: form,
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'File uploaded',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }


    if (!election) return null;
    if (!election?.regions?.some(r => !!r.regionAreas?.length)) {
        if (props.open) {
            props.onClose();
            alert('Set up at least one region area');
        }
        return null
    };
    return (
        <Dialog
            title={
                <>
                    <Typography variant='h4' >
                        Voters
                    </Typography>
                    <IconButton onClick={e => setAnchorEl(e.target)} >
                        <MoreVertIcon />
                    </IconButton>
                </>
            } titleProps={{ disableTypography: true }}
            hideSaveButton cancelButtonLabel='Back'
            open={props.open} onClose={props.onClose} fullWidth breakpoint='sm'
        >
            <VoterDialogContent 
                election={election} canEdit
                onEditVoter={v => {
                    setSelectedVoter(v);
                    setNewDialogOpen(true);
                }}
                onDeleteVoter={v => {
                    deleteVoter(v);
                }}
            />

            <Menu
                open={!!anchorEl} anchorEl={anchorEl}
                onClose={() => setAnchorEl(null)}
            >
                <MenuItem onClick={getTemplate} >
                    Download Upload Template
                </MenuItem>
                <MenuItem onClick={getRecords} >
                    Download Records
                </MenuItem>
                <label htmlFor="file-input">
                    <MenuItem>
                        Upload File
                    </MenuItem>
                </label>
                <MenuItem onClick={e => {
                    setSelectedVoter(null);
                    setNewDialogOpen(true);
                }} >
                    Add New
                </MenuItem>
            </Menu>

            <input type="file" id='file-input' hidden onChange={saveFile} />

            <NewVoterDialog
                open={newDialogOpen} onClose={_ => setNewDialogOpen(false)}
                voter={selectedVoter} election={election}
            />
        </Dialog>
    )
}



/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialogTitle-root {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
`;







function NewVoterDialog(props) {
    const { voter, election, } = props;
    const { values, setValue, reset } = useValidator();

    useEffect(() => {
        if (!props.open) return;
        reset(voter);
    }, [props.open, voter]);

    const dispatch = useDispatch();
    const saveVoter = async e => {
        e.preventDefault();
        const key = dispatch(addNotification({
            message: 'Saving voter',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections/voters', {
                method: values.voterId ? 'PUT' : 'POST',
                body: {
                    ...values,
                    electionId: election.electionId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Voter saved',
            }));
            props.onClose();
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }

    const region = useMemo(() => {
        return election?.regions.find(r => r.regionId == election.lowestRegionId)
    }, [election]);

    return (
        <NewDialog
            wrapForm title='New Voter' maxWidth='sm' onClose={props.onClose}
            onSave={saveVoter} open={props.open} fullWidth
        >
            <TextField
                label={`Full Name`} required name='fullName'
                value={values.fullName} onChange={setValue} autoFocus
            />
            <TextField
                label={`ID Number`} required name='idFieldValue'
                value={values.idFieldValue} onChange={setValue}
            />
            <TextField
                label={`Email`} required name='email' type='email'
                value={values.email} onChange={setValue}
            />
            <TextField
                label={`Phone`} required name='phone'
                value={values.phone} onChange={setValue}
            />
            <div className='sel-field' >
                <label htmlFor="ra-sel">
                    {region?.name}*
                </label>
                <select
                    name="regionAreaId" id="ra-sel" required
                    value={values.regionAreaId} onChange={setValue}
                >
                    <option disabled selected value='' > -- select an option -- </option>
                    {election?.regions?.[election.regions.length - 1]?.regionAreas?.map(ra =>
                        <option key={ra.regionAreaId} value={ra.regionAreaId} >
                            {ra.name}
                        </option>
                    )}
                </select>
            </div>
        </NewDialog>
    );
}


/**@type {GenericDialog} */
const NewDialog = styled(GenericDialog)`
    .MuiDialogContent-root form {
        display: grid;
        row-gap: 1rem;
        .sel-field {
            display: grid;
            grid-template-columns: 1fr 2fr;
        }
    }
`;





VotersDialog.propTypes = {
    election: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default VotersDialog;