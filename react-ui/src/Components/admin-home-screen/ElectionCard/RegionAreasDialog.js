import { Divider, IconButton, Menu, MenuItem, TextField, Typography } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import RegionAreasDialogContent from 'Components/data-dialog-content/RegionAreasDialogContent';
import GenericDialog from 'Components/GenericDialog';
import { useValidator, xFetch } from 'flitlib';
import PropTypes from 'prop-types';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addNotification, editNotification, fetchData, ACTIONS } from 'Redux/actions';
import styled from 'styled-components';

function RegionAreasDialog(props) {
    const { election } = props;
    const [anchorEl, setAnchorEl] = useState(null);
    const [selectedRegion, setSelectedRegion] = useState(election?.regions?.[0]);
    const [newDialogOpen, setNewDialogOpen] = useState(false);
    const [selectedRegionArea, setSelectedRegionArea] = useState(null);

    useEffect(() => {
        if (!election || !selectedRegion) return;
        const newRegion = election.regions
            .find(r => selectedRegion.regionId == r.regionId);
        setSelectedRegion(newRegion);
    }, [election]);
    
    const getTemplate = async e => {
        setAnchorEl(null);
        await xFetch('/elections/region-areas/template');
    }

    const getRecords = async e => {
        setAnchorEl(null);
        await xFetch('/elections/region-areas',  {
            body: {
                electionId: election.electionId,
            }
        });
    }

    const dispatch = useDispatch();
    const deleteRegionArea = async ({ regionAreaId }) => {
        if (!window.confirm('Are you sure you want to delete this region area?')) return;

        const key = dispatch(addNotification({
            message: 'Deleting Region Area',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections/region-areas', {
                method: 'DELETE',
                body: {
                    regionAreaId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Region Area deleted',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }


    const saveFile = async (e) => {
        e.preventDefault();
        setAnchorEl(null);
        const file = e.target.files[0];
        if (!file) return;
        if (!window.confirm('This will replace the current list of region areas. Proceed?')) return;

        const key = dispatch(addNotification({
            message: 'Uploading file',
            persist: true,
            action: null,
        }));
        const form = new FormData();
        form.set('file', file);
        form.set('electionId', election.electionId);
        try {
            await xFetch('/elections/region-areas', {
                method: 'POST',
                body: form,
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'File uploaded',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }

    const parentRegion = useMemo(() => {
        return election?.regions?.find(r =>
            r.regionId == selectedRegion?.parentRegionId
        )
    }, [election, selectedRegion]);


    if (!selectedRegion) {
        if(props.open){
            props.onClose();
            alert('Election must have at least one region');
        }
        return null;
    }
    const { regions } = election;
    return (
        <Dialog
            title={
                <>
                    <Typography variant='h4' >
                        Region Areas
                    </Typography>
                    <IconButton onClick={e => setAnchorEl(e.target)} >
                        <MoreVertIcon />
                    </IconButton>
                </>
            } titleProps={{ disableTypography: true }}
            hideSaveButton cancelButtonLabel='Back' 
            open={props.open} onClose={props.onClose} fullWidth
            breakpoint='sm'
        >
            <div className='sel-field' >
                <Typography>Region</Typography>
                <select
                    name="region" id="region-sel" value={selectedRegion.regionId}
                    onChange={e => {}}
                >
                    {regions?.map(r =>
                        <option 
                            value={r.regionId} key={r.name} 
                            onClick={e => setSelectedRegion(r)}
                        >
                            {r.name}
                        </option>
                    )}
                </select>
            </div>
            <Divider />
            <RegionAreasDialogContent 
                region={selectedRegion} parentRegion={parentRegion} canEdit
                onEditRegionArea={ra => {
                    setSelectedRegionArea(ra);
                    setNewDialogOpen(true);
                }}
                onDeleteRegionArea={deleteRegionArea}
            />

            <Menu
                open={!!anchorEl} anchorEl={anchorEl}
                onClose={() => setAnchorEl(null)}
            >
                <MenuItem onClick={getTemplate} >
                    Download Upload Template
                </MenuItem>
                <MenuItem onClick={getRecords} >
                    Download Records
                </MenuItem>
                <label htmlFor="file-input">
                    <MenuItem>
                        Upload File
                    </MenuItem>
                </label>
                <MenuItem onClick={e => {
                    setAnchorEl(null)
                    setSelectedRegionArea(null);
                    setNewDialogOpen(true);
                }} >
                    Add New
                </MenuItem>
            </Menu>

            <input type="file" id='file-input' hidden onChange={saveFile} />

            <NewRegionAreaDialog
                open={newDialogOpen} onClose={_ => setNewDialogOpen(false)}
                regionArea={selectedRegionArea} region={selectedRegion}
                parentRegion={parentRegion}
            />
        </Dialog>
    );
}

/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialogTitle-root {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .MuiDialogContent-root {
        display: flex;
        flex-direction: column;
        >*:not(:first-child){
            margin-top: .8rem;
        }
        .sel-field {
            display: grid;
            grid-template-columns: 1fr 2fr;
            align-content: center;
        }
    }
`;


function NewRegionAreaDialog(props) {
    const { regionArea, region, parentRegion } = props;
    const { values, setValue, reset } = useValidator(regionArea);

    useEffect(() => {
        if(!props.open) return;
        reset(regionArea);
    }, [props.open, regionArea]);
    
    const dispatch = useDispatch();
    const saveRegionArea = async e => {
        e.preventDefault();
        const key = dispatch(addNotification({
            message: 'Saving Region Area',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections/region-areas', {
                method: values.regionAreaId ? 'PUT' : 'POST',
                body: {
                    ...values,
                    regionId: region.regionId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Region Area saved',
            }));
            props.onClose();
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }


    return (
        <NewDialog
            wrapForm 
            title={`${values.regionAreaId ? 'Edit':'New'} Region Area`} maxWidth='sm' onClose={props.onClose}
            onSave={saveRegionArea} open={props.open} fullWidth
        >
            <TextField
                label={`${region.name} Name`} required name='name'
                value={values.name} onChange={setValue} autoFocus
            />
            <TextField
                label={`${region.name} Code`} name='code'
                value={values.code} onChange={setValue}
            />
            {region.parentRegionId &&
                <div className='sel-field' >
                    <label htmlFor="p-area-code">
                        {parentRegion?.name}*
                    </label>
                    <select
                        name="parentRegionAreaId" id="p-area-code" required
                        value={values.parentRegionAreaId || ''} onChange={setValue}
                    >
                        <option disabled value='' > -- select an option -- </option>
                        {parentRegion?.regionAreas?.map(ra =>
                            <option key={ra.regionAreaId} value={ra.regionAreaId} >
                                {ra.name} {ra.code ? `(${ra.code})`:''}
                            </option>
                        )}
                    </select>
                </div>
            }
        </NewDialog>
    );
}


/**@type {GenericDialog} */
const NewDialog = styled(GenericDialog)`
    .MuiDialogContent-root form {
        display: grid;
        row-gap: 1rem;
        .sel-field {
            display: grid;
            grid-template-columns: 1fr 2fr;
            align-items: center;
        }
    }
`;



RegionAreasDialog.propTypes = {
    election: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default RegionAreasDialog;