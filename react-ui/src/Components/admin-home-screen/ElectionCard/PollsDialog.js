import { Button, TextField, Typography } from '@material-ui/core';
import PollsDialogContent from 'Components/data-dialog-content/PollsDialogContent';
import GenericDialog from 'Components/GenericDialog';
import { useValidator, xFetch } from 'flitlib';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addNotification, editNotification, fetchData, ACTIONS } from 'Redux/actions';
import styled from 'styled-components';

function PollsDialog(props) {
    const { election } = props;
    const [selectedPoll, setSelectedPoll] = useState(null);
    const [newDialogOpen, setNewDialogOpen] = useState(false);

    const dispatch = useDispatch();
    const deletePoll = async ({ pollId }) => {
        if (!window.confirm('Are you sure you want to delete this poll?')) return;

        const key = dispatch(addNotification({
            message: 'Deleting Poll',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections/polls', {
                method: 'DELETE',
                body: {
                    pollId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Poll deleted',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }




    if (!election) return null;
    return (
        <Dialog
            open={props.open} onClose={props.onClose} 
            hideSaveButton cancelButtonLabel='Back'
            title={
                <>
                    <Typography variant='h4' >
                        Polls
                    </Typography>
                    <Button
                        variant='outlined' color='primary' onClick={e => {
                            setSelectedPoll(null);
                            setNewDialogOpen(true);
                        }}
                    >
                        Add New
                    </Button>
                </>
            } titleProps={{ disableTypography: true }} breakpoint='sm' fullWidth
        >
            <PollsDialogContent 
                election={election} canEdit
                onEditPoll={p => {
                    setSelectedPoll(p);
                    setNewDialogOpen(true);
                }}
                onDeletePoll={deletePoll}
            />

            <NewPollDialog
                open={newDialogOpen} onClose={_ => setNewDialogOpen(false)}
                election={election} poll={selectedPoll}
            />
        </Dialog>
    )
}


/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialogTitle-root {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
`;



function NewPollDialog(props) {
    const { poll, election } = props;
    const { values, setValue, reset } = useValidator(poll);

    const dispatch = useDispatch();
    const savePoll = async e => {
        e.preventDefault();
        const key = dispatch(addNotification({
            message: 'Saving Poll',
            action: null,
            persist: true,
        }));
        try {
            await xFetch('/elections/polls', {
                method: values.pollId ? 'PUT' : 'POST',
                body: {
                    ...values,
                    electionId: election.electionId,
                }
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Poll saved',
            }));
            props.onClose();
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));

        }
    }

    useEffect(() => {
        if (!props.open) return;
        reset(poll);
    }, [props.open, poll]);



    return (
        <NewDialog
            wrapForm
            title={`${values.pollId ? 'Edit' : 'New'} Poll`} maxWidth='sm'
            onClose={props.onClose}
            onSave={savePoll} open={props.open} fullWidth
        >
            <TextField
                label='Position' value={values.position} name='position'
                onChange={setValue} autoFocus required
            />
            <div className='sel-field' >
                <label htmlFor="region-sel">
                    Region*
                </label>
                <select
                    name="regionId" id="region-sel" value={values.regionId}
                    onChange={setValue} required
                >
                    <option disabled selected value='' > -- select an option -- </option>
                    {election?.regions?.map(r =>
                        <option value={r.regionId} key={r.regionId} >{r.name}</option>
                    )}
                </select>
            </div>
        </NewDialog>
    );
}


/**@type {GenericDialog} */
const NewDialog = styled(GenericDialog)`
    .MuiDialogContent-root form {
        display: grid;
        row-gap: 1rem;
        .sel-field {
            display: grid;
            grid-template-columns: 1fr 2fr;
            align-items: center;
        }
    }
`;








PollsDialog.propTypes = {
    election: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default PollsDialog;