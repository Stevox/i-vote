import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GenericDialog from 'Components/GenericDialog';
import { useValidator, dataURItoFile, xFetch } from 'flitlib';
import ProfilePicture from 'Components/ProfilePicture';
import { TextField, FormControlLabel, Checkbox, Typography } from '@material-ui/core';
import { DatePicker } from '@material-ui/pickers';
import RegionInput from './RegionInput';
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { addNotification, editNotification, fetchData, ACTIONS } from 'Redux/actions';
import { appendFormData } from 'Utils';

function ElectionDialog(props) {
    const { election } = props;
    const picRef = useRef(null);
    const { values, setValue, reset } = useValidator(election);

    useEffect(() => {
        if (!props.open) return;
        if (props.open) {
            const el = election;
            // if(election){
            //     el.regionNames = election.regions?.map(r => r.name);
            // }
            reset(election);
        };
    }, [props.open, election]);

    const setImage = ({ data }) => {
        // /**@type {ProfilePicture} */
        const pEl = picRef.current;
        const dataURI = pEl.getImageAsDataUrl();
        const filename = `election-logo.${/^\w+:.+?\/(.+?);/.exec(dataURI)?.[1]}`;
        const logoFile = dataURItoFile(dataURI, filename);
        setValue({ logoFile });
    }

    const dispatch = useDispatch();
    const saveElection = async e => {
        e.preventDefault();
        if (!values.regions?.length) {
            return alert('You must set at least one region');
        }
        const form = new FormData();
        for (let [key, val] of Object.entries(values)) {
            if(/^(?:Object|Array)/.test(val.constructor.name)){
                key = `${key}{}`;
                val = JSON.stringify(val);
            }
            form.set(key, val);
        }
        const key = dispatch(addNotification({
            message: 'Saving Election',
            persist: true,
            action: null,
        }));
        try {
            await xFetch('/elections', {
                method: values.electionId ? 'PUT': 'POST',
                body: form
            });
            await dispatch(fetchData(ACTIONS.FETCH_ELECTIONS));
            dispatch(editNotification({
                key,
                message: 'Election saved',
            }));            return props?.onClose();
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));
        }
    }

    return (
        <Dialog
            wrapForm fullWidth open={props.open} breakpoint='sm'
            title={`${values.electionId ? 'Edit' : 'New'} Election`}
            onSave={saveElection}
            onClose={props.onClose}
        >
            <ProfilePicture
                cropSize={200}
                ref={picRef}
                src={values.logoFileUrl}
                onImageLoaded={setImage}
                frameFormat='circle'
            />
            <TextField
                label='Title' name='title' value={values.title}
                onChange={setValue} autoFocus required fullWidth
            />
            <TextField
                label='Description' name='description' value={values.description}
                onChange={setValue} multiline fullWidth
            />
            <TextField
                label="Administrating Official's Email" name='officialEmail'
                value={values.officialEmail} type='email' required
                onChange={setValue} fullWidth
            />
            <TextField
                label="Number of Peer Servers" name='peerServersNum'
                value={values.peerServersNum} type='number' defaultValue={0}
                onChange={setValue} fullWidth
                inputProps={{
                    min: 0, max: 100,
                }} 
            />
            <div id="date">
                <DatePicker
                    label='Voting Start Date' format='ddd Do MMMM YYYY - hh:mm a'
                    minDate={values.electionId ? undefined: moment().add(2, 'weeks')} 
                    value={values.startDate || null}
                    onChange={date => {
                        const startDate = moment(date).hour(0).minutes(1);
                        setValue({ startDate }, true)
                    }} required
                    fullWidth
                />
                <DatePicker
                    label='Voting End Date' format='ddd Do MMMM YYYY - hh:mm a'
                    minDate={values.startDate || new Date()} value={values.endDate || null}
                    onChange={date => {
                        const endDate = moment(date).hour(23).minutes(59);
                        setValue({ endDate }, true)
                    }} required
                    fullWidth
                    minDateMessage='Date should not be before Voting start date'
                />
            </div>
            <FormControlLabel
                control={
                    <Checkbox
                        checked={!!values.allowNullVotes}
                        onChange={e => setValue({ allowNullVotes: e.target.checked }, true)}
                        name='allowNullVotes'
                    />
                }
                label='Allow Null Votes'
            />

            <div id='regions-container'>
                <Typography variant='h4' >Regions</Typography>
                <div id="regions">
                    {(values.regions || []).map((r, i) =>
                        <RegionInput
                            regionName={r.name} depth={i} key={r.name}
                            onSave={name => {
                                const rs = values.regions;
                                rs[i] = {...r, name};
                                setValue({ regions: rs }, true);
                            }}
                            onDelete={_ => {
                                if(r.regionId && !window.confirm(
                                    "Deleting a region will also delete "+
                                    "all region areas, polls and candidates "+
                                    "attached to it. Proceed?")) return;
                                const rs = values.regions;
                                rs.splice(i, 1);
                                setValue({ regions: rs }, true);
                            }}
                        />
                    )}
                    <RegionInput
                        autoFocus={Boolean(!values.electionId && values.regions)}
                        onSave={name => {
                            let rs = values.regions;
                            const r = (values.regions || []).find(pr => pr.name === name) || {};
                            const region = {...r, name};
                            if (Array.isArray(rs)) {
                                rs.push(region);
                            } else rs = [region];
                            setValue({ regions: rs }, true);
                        }}
                    />
                </div>
            </div>
        </Dialog>
    )
}


/**@type {GenericDialog} */
const Dialog = styled(GenericDialog)`
    .MuiDialogContent-root form {
        display: grid;
        row-gap: 1rem;
        #date {
            /* width: 100%; */
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(360px, 1fr));
            column-gap: 1rem;
        }
        #regions-container {
            margin-top: 1rem;
            margin-bottom: 2rem;
            #regions {
                margin-top: 1rem;
            }
        }
    }
`;

ElectionDialog.propTypes = {
    election: PropTypes.object,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
}

export default ElectionDialog;