import { IconButton, TextField, Typography } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import styled from 'styled-components';

function RegionInput(props) {
    const [editing, setEditing] = useState(!props.regionName);
    const [val, setVal] = useState(props.regionName || '');

    const saveRegion = e => {
        e.preventDefault();
        e.stopPropagation();
        if(!val) return;
        props.onSave(val);
        setVal('');
    }
    
    return (
        <Wrapper>
            {editing ? 
                <div id='edit-container' >
                    <TextField 
                        label='Region Name' name='regionName' 
                        autoFocus={props.autoFocus}
                        value={val} onChange={e => setVal(e.target.value)}
                        onBlur={e => {
                            const value = e.target.value.trim()
                                .replace(/^(\w)/, (_, m1) => m1.toUpperCase());
                            setVal(value);
                        }}
                        onKeyDown={e => {
                            if(val && e.key === 'Enter'){
                                saveRegion(e);
                            }
                        }}
                    />
                    <div id="action">
                        <IconButton onClick={saveRegion} >
                            <SaveIcon />
                        </IconButton>
                        {props.regionName &&
                            <IconButton type='button' onClick={e => setEditing(false)} >
                                <CancelIcon />
                            </IconButton>
                        }
                    </div>
                </div>:
                <div>
                    <Typography>
                        {'+' + Array(((props.depth||0) * 5) + 3).fill('-').join('') + '> '} 
                        {props.regionName}
                    </Typography>
                    <div id="action">
                        <IconButton onClick={e => setEditing(true)} >
                            <EditIcon />
                        </IconButton>
                        <IconButton onClick={props.onDelete} >
                            <DeleteIcon />
                        </IconButton>
                    </div>
                </div>
            }
        </Wrapper>
    )
}

const Wrapper = styled.div`
    >*:first-child{
        width: 100%;
        padding-left: 3%;
        display: grid;
        grid-template-columns: 3fr 1fr;
        justify-content: space-between;
        #action {
            display: flex;
            justify-content: space-around;
        }
    }
    >#edit-container {
        margin-bottom: 1rem;
    }
`;

RegionInput.propTypes = {
    regionName: PropTypes.string,
    depth: PropTypes.number,
    onSave: PropTypes.func.isRequired,
    onDelete: PropTypes.func,
}

export default RegionInput;