import { IconButton, TextField } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import DataTable from 'Components/DataTable';
import PropTypes from 'prop-types';
import React, { useMemo, useState } from 'react';
import styled from 'styled-components';


function RegionAreasDialogContent(props) {
    const { region, parentRegion, canEdit } = props;
    const [filterText, setFilterText] = useState('');

    const headers = useMemo(() => {
        const cols = ['Region Area Name', 'Region Area Code', 'Parent Region'];
        if (canEdit) {
            cols.push('');
        }
        return cols;
    }, [canEdit]);

    const regionAreasRows = useMemo(() => region?.regionAreas?.reduce((acc, ra) => {
        const regex = new RegExp(`\\b${filterText}`, 'i');
        if (filterText && !(regex.test(ra.name) || regex.test(ra.code))) return acc;
        const parentRA = (parentRegion?.regionAreas || []).find(pra =>
            pra.regionAreaId == ra.parentRegionAreaId
        );

        const row = [
            ra.name, ra.code, parentRA?.name,
        ];
        if (canEdit) {
            row.push(
                <div className='action' >
                    <IconButton onClick={e => {
                        return props.onEditRegionArea?.(ra);
                    }} >
                        <EditIcon />
                    </IconButton>
                    <IconButton onClick={e => {
                        return props.onDeleteRegionArea?.(ra)
                    }} >
                        <DeleteIcon />
                    </IconButton>
                </div>
            );
        }

        return [...acc, row];
    }, []), [region, filterText, parentRegion]);

    return (
        <Wrapper>
            {(regionAreasRows || []).length > 8 &&
                <TextField
                    placeholder='Region Area...' value={filterText}
                    onChange={e => setFilterText(e.target.value)}
                />
            }

            <DataTable
                headers={headers}
                rows={regionAreasRows}
            />
        </Wrapper>
    )
}

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    >*:not(:first-child){
        margin-top: .8rem;
    }
    .uiDataTable {
        flex: 1;
        overflow: visible;
        @media (max-width: ${({ theme }) => theme.breakpoints.values.sm}px){
            overflow-y: auto;
        }
        td .action {
            display: flex;
            align-items: center;
        }
    }
`;


RegionAreasDialogContent.propTypes = {
    region: PropTypes.object,
    parentRegion: PropTypes.object,
    canEdit: PropTypes.bool,
    onEditRegionArea: PropTypes.func,
    onDeleteRegionArea: PropTypes.func,
}

export default RegionAreasDialogContent;