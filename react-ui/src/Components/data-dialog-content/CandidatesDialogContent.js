import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { IconButton, TextField, Divider, Typography } from '@material-ui/core';
import DataTable from 'Components/DataTable';
import CustomAvatar from 'Components/CustomAvatar';


function CandidatesDialogContent(props) {
    const { election, region, canEdit } = props;
    const [selectedPoll, setSelectedPoll] = useState((region || election?.regions?.find(r => !!r.polls?.length))?.polls[0]);
    const [filterText, setFilterText] = useState('');

    const pollPositions = useMemo(() => {
        if(region){
            return getPositions(region) || [];
        }
        
        return election?.regions?.reduce((acc, r) => {
            const rPolls = getPositions(r);
            if (!rPolls) return acc;
            return [...acc, ...rPolls];
        }, []);

        function getPositions(r){
            const rPolls = r?.polls?.map(p => {
                return (
                    <option
                        value={p.pollId} key={p.pollId}
                        onClick={e => setSelectedPoll(p)}
                    >
                        {p.position}
                    </option>
                );
            });
            if (!rPolls) return null;
            return [ ...rPolls];
        }
    }, [election, region]);



    const headers = useMemo(() => {
        const cols = ['Candidate', 'Position', 'Region Area']
        if(new Date(election.endDate) < new Date()){
            cols.push('Votes');
        }
        if (canEdit) {
            cols.push('');
        }
        return cols;
    }, [election, canEdit]);

    const candidatesRows = useMemo(() => election?.regions?.reduce((acc, r) => {
        if (r.regionId != selectedPoll?.regionId) return acc;
        const cArr = [...acc];

        (r.regionAreas || []).forEach(ra => {
            (ra.candidates || []).forEach(c => {
                const regex = new RegExp(`\\b${filterText}`, 'i');
                if (filterText && !(regex.test(c.name) || regex.test(ra.name))) return;
                if (c.pollId != selectedPoll.pollId) return;

                const row = [
                    <div className='name' >
                        <CustomAvatar src={c.imageUrl} />
                        <Typography variant='body2' >{c.name}</Typography>
                    </div>,
                    selectedPoll.position,
                    ra.name,
                ];
                if(new Date(election.endDate) < new Date()){
                    row.push(c.votes);
                }
                if (canEdit) {
                    row.push(
                        <div className='action'>
                            <IconButton onClick={e => props.onEditCandidate?.(c, { regionArea: ra })} >
                                <EditIcon />
                            </IconButton>
                            <IconButton onClick={e => props.onDeleteCandidate?.(c)} >
                                <DeleteIcon />
                            </IconButton>
                        </div>
                    );
                }

                cArr.push(row);
            });
        });

        return cArr;
    }, []), [selectedPoll, filterText, election]);

    return (
        <Wrapper>
            {pollPositions?.length > 1 &&
                <div className='sel-field' >
                    <Typography>Position</Typography>
                    <select
                        name="position" id="position-sel" value={selectedPoll.pollId}
                        onChange={e => {/* Handled by the options */ }}
                    >
                        {pollPositions}
                    </select>
                </div>
            }
            <Divider />
            {candidatesRows.length > 8 &&
                <TextField
                    placeholder='Candidate Name or Region Area' value={filterText}
                    onChange={e => setFilterText(e.target.value)}
                />
            }
            <DataTable
                headers={headers}
                rows={candidatesRows}
            />
        </Wrapper>
    )
}

const Wrapper = styled.div`
    display: grid;
    row-gap: .8rem;
    align-content: flex-start;
    .uiDataTable {
        .name {
            display: flex;
            align-items: center;
            >:first-child {
                margin-right: 1rem;
            }
        }
        .action {
            display: flex;
            align-items: center;
            justify-content: flex-end;
        }
    }
    .sel-field {
        display: grid;
        grid-template-columns: 1fr 2fr;
    }
`;


CandidatesDialogContent.propTypes = {
    election: PropTypes.object,
    region: PropTypes.object,
    canEdit: PropTypes.bool,
    onEditCandidate: PropTypes.func,
    onDeleteCandidate: PropTypes.func,
}

export default CandidatesDialogContent;