import React, { useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { TextField, IconButton } from '@material-ui/core';
import DataTable from 'Components/DataTable';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import styled from 'styled-components';

function VoterDialogContent(props) {
    const { election, canEdit } = props;
    const [filterText, setFilterText] = useState('');
    
    const headers = useMemo(() => {
        const cols = [
            'ID Number', 'Name',
            `${election?.regions?.[election.regions.length - 1].name}`, 'Email'
        ];
        if(canEdit){
            cols.push('');
        }
        return cols;
     }, [election, canEdit]);

    const voterRows = useMemo(() => election?.voters?.reduce((acc, v) => {
        const regex = new RegExp(`\\b${filterText}`, 'i');
        if (filterText && !(regex.test(v.fullName) || regex.test(v.idFieldValue))) return acc;

        const row = [v.idFieldValue, v.fullName, v.regionAreaName, v.email];
        if (canEdit) {
            row.push(
                <div className='action' key={v.idFieldValue} >
                    <IconButton onClick={e => {
                        return props.onEditVoter?.(v);
                    }} >
                        <EditIcon />
                    </IconButton>
                    <IconButton onClick={e => {
                        e.preventDefault();
                        return props.onDeleteVoter?.(v);
                    }} >
                        <DeleteIcon />
                    </IconButton>
                </div>
            );
        }

        return [
            ...acc, row,
        ]
    }, []), [election, filterText, canEdit]);
    
    return (
        <Wrapper>
            <TextField
                placeholder='Voter Name...' value={filterText}
                onChange={e => setFilterText(e.target.value)}
            />
            <DataTable
                headers={headers}
                rows={voterRows}
            />
        </Wrapper>
    )
}

const Wrapper = styled.div`
    display: grid;
    row-gap: .8rem;
    align-content: flex-start;
    .uiDataTable tr .action {
        display: flex;
        align-items: center;
        justify-content: flex-end;
    }
    .sel-field {
        display: grid;
        grid-template-columns: 1fr 2fr;
    }
`;

VoterDialogContent.propTypes = {
    election: PropTypes.object,
    canEdit: PropTypes.bool,
    onEditVoter: PropTypes.func,
    onDeleteVoter: PropTypes.func,
}

export default VoterDialogContent

