import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import EditIcon from '@material-ui/icons/Edit';
import ResultsIcon from '@material-ui/icons/Assessment';
import DeleteIcon from '@material-ui/icons/Delete';
import { IconButton } from '@material-ui/core';
import DataTable from 'Components/DataTable';
import RegionPollResultDialog from 'Components/officials-home/RegionPollResultDialog';


function PollsDialogContent(props) {
    const { election, region, canEdit } = props;
    const [resultsDialogOpen, setResultsDialogOpen] = useState(false);
    const [selectedPoll, setSelectedPoll] = useState();
    const [selectedRegion, setSelectedRegion] = useState(region);

    const headers = useMemo(() => {
        const cols = ['Position', 'Region'];
        if (new Date(election.endDate) < new Date()) {
            cols.push('Results');
        }
        if (canEdit) {
            cols.push('');
        }
        return cols;
    }, [election, canEdit]);

    const PollRows = useMemo(() => {
        if (region) {
            return getRows(region);
        }

        return election?.regions?.reduce((acc, r) => {
            const rPolls = getRows(r);
            return [...acc, ...(rPolls || [])];
        }, []);

        function getRows(r) {
            return r?.polls?.map(p => {
                const row = [p.position, r.name];
                if (new Date(election.endDate) < new Date()) {
                    row.push(
                        <IconButton onClick={e => {
                            setResultsDialogOpen(true);
                            setSelectedPoll(p);
                            setSelectedRegion(r);
                        }} >
                            <ResultsIcon />
                        </IconButton>
                    );
                }
                if (canEdit) {
                    row.push(
                        <div className='action' >
                            <IconButton onClick={e => {
                                return props.onEditPoll?.(p);
                            }} >
                                <EditIcon />
                            </IconButton>
                            <IconButton onClick={e => {
                                return props.onDeletePoll?.(p);
                            }} >
                                <DeleteIcon />
                            </IconButton>
                        </div>
                    );
                }
                return row;
            });
        }
    }, [election, region]);



    return (
        <Wrapper>
            <DataTable
                headers={headers}
                rows={PollRows}
            />
            <RegionPollResultDialog
                open={resultsDialogOpen} region={selectedRegion} poll={selectedPoll}
                onClose={() => setResultsDialogOpen(false)}
            />
        </Wrapper>
    )
}

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    >*{ margin-top: .8rem;}
    td .action {
        display: flex;
        align-items: center;
    }
`;


PollsDialogContent.propTypes = {
    election: PropTypes.object,
    region: PropTypes.object,
    canEdit: PropTypes.bool,
    onEditPoll: PropTypes.func,
    onDeletePoll: PropTypes.func,
}

export default PollsDialogContent;