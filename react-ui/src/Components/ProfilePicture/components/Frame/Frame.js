import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";


class Frame extends Component {
    render() {
        const framesMap = {
            circle: "frame--circle",
            square: "frame--square",
            "rounded-square": "frame--rounded-square"
        }
        const style = {
            width: `${this.props.size}px`,
            height: `${this.props.size}px`
        };
        return (
            <Wrapper
                className={`frame ${framesMap[this.props.format]}`}
                style={style} src={this.props.src}
                ref={this.props.frameRef}
            >
                {this.props.children}
            </Wrapper>
        );
    }
}


const Wrapper = styled.div`
    overflow: hidden;
    position: relative;
    border: 1px solid #95bbdf;
    margin: 0 auto;
    background: url(${({src}) => src}) 100%/100% no-repeat;

    &.frame--circle {
        border-radius: 50%;
    }

    &.frame--square {
    }

    &.frame--rounded-square {
        border-radius: 4px;
    }

    img {
        display: none;
        user-select: none;
        position: relative;
    }
`;



Frame.propTypes = {
    src: PropTypes.string,
    size: PropTypes.number.isRequired,
    format: PropTypes.oneOf(["circle", "square", "rounded-square"])
};

Frame.defaultProps = {
    size: 160,
    format: "circle"
};

export default Frame;
