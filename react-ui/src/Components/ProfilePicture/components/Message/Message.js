import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class Message extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Wrapper >
                {this.props.onClick ? (
                    <button type='button' className={'__button'}  onClick={this.props.onClick}>
                        <div className={"__content"} >{this.props.children}</div>
                    </button>
                ) : (
                        <div className={"__content"} >{this.props.children}</div>
                    )}
            </Wrapper>
        );
    }
}

const Wrapper = styled.div`
    position: absolute;
    left: 0px;
    right: 0px;
    top: 0;
    bottom: 0;
    color: color(primary);
    text-align: center;
    z-index: 3;
    user-select: none;
    background: #FFFB;

    .__content {
        position: absolute;
        left: 5px;
        right: 5px;
        top: 50%;
        transform: translateY(-50%);
    }

    .__text {
        padding: 0;
        font-size: 0.9em;
    }

    .__button {
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: transparent;
        border: 0;
        cursor: pointer;
        font-family: inherit;
        font-size: inherit;
    }
`;

Message.propTypes = {
    onClick: PropTypes.func
};

export default Message;
