import React, { Component } from "react";
import PropTypes from "prop-types";
import Icon from "../Icon/Icon";
import styled from "styled-components";

const MIN = "MIN";
const MAX = "MAX";

class ZoomScale extends Component {
    constructor(props) {
        super(props);

        this.state = {
            limitReached: MIN
        };
    }

    onChange(event) {
        const value = Number(event.target.value).toFixed(2);
        const minValue = Number(this.props.min).toFixed(2);
        const maxValue = Number(this.props.max).toFixed(2);
        let limitReached = false;

        if (value === minValue) limitReached = MIN;
        if (value === maxValue) limitReached = MAX;

        this.setState({ limitReached });

        this.props.onChange.call(this, event);
    }

    render() {
        const zoomMinCssClasses = ["__min", "__icon"];
        const zoomMaxCssClasses = ["__max", "__icon"];

        if (this.state.limitReached === MIN)
            zoomMinCssClasses.push("__icon--disabled");

        if (this.state.limitReached === MAX)
            zoomMaxCssClasses.push("__icon--disabled");

        return (
            <Wrapper >
                <Icon
                    name="picture"
                    size={17}
                    className={zoomMinCssClasses.join(" ")}
                />
                <input
                    type="range"
                    className={"__handler"}
                    max={Number(this.props.max).toFixed(2)}
                    min={Number(this.props.min).toFixed(2)}
                    step={Number(this.props.step).toFixed(2)}
                    value={Number(this.props.value).toFixed(2)}
                    onChange={this.onChange.bind(this)}
                />
                <Icon
                    name="picture"
                    size={20}
                    className={zoomMaxCssClasses.join(" ")}
                />
            </Wrapper>
        );
    }
}



const Wrapper = styled.div`
    position: relative;
    width: 90%;
    margin: 10px auto;
    .__handler {
        -webkit-appearance: none;
        display: block;
        margin: 0 auto;
        width: calc(100% - 40px);
        height: 18px;
        background: transparent;

        &:focus {
            &::-ms-thumb {
                border-color: color(primary);
                box-shadow: 0 0 1px 0px color(primary);
            }
        }

        &::-webkit-slider-thumb {
        -webkit-appearance: none;
        }

        &::-moz-range-thumb {
        border-color: color(primary);
        box-shadow: 0 0 1px 0px color(primary);
        }

        &::-webkit-slider-thumb {
        border-color: color(primary);
        box-shadow: 0 0 1px 0px color(primary);
        -webkit-appearance: none;
        margin-top: -9px;
        box-sizing: border-box;
        cursor: pointer;
        width: 18px;
        height: 18px;
        display: block;
        border-radius: 50%;
        background: #eee;
        border: 1px solid color(primary);

            &:hover {
                border-color: lighten(color(primary), 20%);
            }
        }

        &::-ms-track {
            width: 100%;
            cursor: pointer;
            background: transparent;
            border-color: transparent;
            color: transparent;
        }

        &::-ms-thumb {
            margin-top: 0;
            box-sizing: border-box;
            cursor: pointer;
            width: 18px;
            height: 18px;
            display: block;
            border-radius: 50%;
            background: #eee;
            border: 1px solid color(primary);

            &:hover {
                border-color: darken(color(primary), 20%);
            }
        }

        &::-moz-range-thumb {
            margin-top: 0;
            box-sizing: border-box;
            cursor: pointer;
            width: 18px;
            height: 18px;
            display: block;
            border-radius: 50%;
            background: #eee;
            border: 1px solid color(primary);
            &::-moz-range-thumb:hover {
                border-color: darken(color(primary), 20%);
            }
        }

        &::-webkit-slider-runnable-track {
            width: 100%;
            height: 1px;
            cursor: pointer;
            background: #eee;
            border: 0;
        }

        &::-moz-range-track {
            width: 100%;
            height: 1px;
            cursor: pointer;
            background: #eee;
            border: 0;
        }
        &::-ms-track {
            width: 100%;
            height: 1px;
            cursor: pointer;
            background: #eee;
            border: 0;
        }
    }

    .__min,
    .__max {
        position: absolute;
        transition: color 0.3s ease;
        fill: color(primary);
    }

    .__min {
        left: -12px;
        top: 0px;
    }

    .__max {
        right: -13px;
        top: -1px;
    }

    .__icon {
        &--disabled {
        fill: lighten(color(primary), 50%);
        }
    }
`;

ZoomScale.propTypes = {
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    step: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func
};

ZoomScale.defaultProps = {
    min: 0,
    max: 1,
    step: 0.01,
    value: 0.5,
    onChange: () => { }
};

export default ZoomScale;
