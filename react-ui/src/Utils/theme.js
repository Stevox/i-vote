import { createMuiTheme, lighten } from "@material-ui/core";

const breakpointValues = {
    xs: 480,
    sm: 640,
    md: 960,
    lg: 1280,
    xl: 1920,
};



const theme = createMuiTheme({
    palette: {
        primary: {
            lighter: lighten('#2e7d32', .8),
            light: lighten('#2e7d32', .4),
            main: '#2e7d32',
            dark: '#005005',
            contrastText: '#ffffff',
        },
        secondary: {
            light: '#4e77ca',
            main: '#004c99',
            dark: '#00256a',
            contrastText: '#ffffff',
        },
    },
    breakpoints: {
        values: breakpointValues,
    },
    typography: {
        h1: {
            fontSize: '5rem',
            [`@media (max-width: ${breakpointValues.sm}px)`]: {
                fontSize: '4rem',
            },
            [`@media (max-width: ${breakpointValues.xs}px)`]: {
                fontSize: '3.4rem',
            },
        },
        h2: {
            [`@media (max-width: ${breakpointValues.xs}px)`]: {
                fontSize: '3.1rem',
            },
        },
        h3: {
            [`@media (max-width: ${breakpointValues.sm}px)`]: {
                fontSize: '2.4rem',
            },
        },
        h4: {
            fontSize: '2.125rem',
            [`@media (max-width: ${breakpointValues.sm}px)`]: {
                fontSize: '1.6rem',
            },
        },
        h5: {
            [`@media (max-width: ${breakpointValues.sm}px)`]: {
                fontSize: '1.3rem',
            },
        },
        body1: {
            fontSize: '1.2rem',
        },
    },
    props: {
        MuiTextField: {
            variant: "outlined",
        },
        MuiButton: {
            disableElevation: true,
        },
        MuiTooltip: {
            PopperProps: {
                disablePortal: true,
            },
        },
    }
});


console.log(theme);

export default theme;