import { AppBar, Toolbar, Button, Typography } from '@material-ui/core';
import logoImg from 'Assets/images/logo.png';
import ResultsPage from 'Components/voter-home-screen/ResultsPage';
import VotingPage from 'Components/voter-home-screen/VotingPage';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from 'Redux/actions';
import styled from 'styled-components';
import { useRouter } from 'flitlib';
import { Redirect } from 'react-router-dom';

function VoterHomeScreen() {
    const dispatch = useDispatch();
    const { changePage } = useRouter();

    const user = useSelector(state => state.user);
    const election = useSelector(state => state.election);
    if (!user || !election) {
        return <Redirect to='/voter/login' />
    }

    return (
        <Wrapper className='screen' >
            <AppBar color='transparent' elevation={0} >
                <Toolbar>
                    <div>
                        <img src={logoImg} alt='logo' />
                    </div>
                    <Button
                        size='large' color='inherit' variant='outlined'
                        onClick={async e => {
                            dispatch(logout());
                            changePage('/');
                        }}
                    >
                        Logout
                    </Button>
                </Toolbar>
            </AppBar>
            <div id="content">
                <div className="info">
                    <Typography id='title' variant='h1' >
                        {election?.title}
                    </Typography>
                    {election?.description &&
                        <Typography variant='h5' >
                            {election?.description}
                        </Typography>
                    }
                </div>
                {new Date(election?.endDate) > new Date() ?
                    <VotingPage /> :
                    <ResultsPage />
                }
            </div>
        </Wrapper>
    )
}


const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    >#content {
        max-width: 700px;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        padding: 3vmin;
        .info {
            display: flex;
            flex-direction: column;
            align-items: center;
            text-align: center;
            >* {
                margin-bottom: 10px;
            }
            >#title {
                margin-bottom: 3rem;
                margin-bottom: 3vmax;
            }
        }
    }
`;

export default VoterHomeScreen;
