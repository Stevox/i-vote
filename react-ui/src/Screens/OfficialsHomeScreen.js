import {
    AppBar, Button, Divider, Icon, IconButton, Menu, MenuItem, Paper, TextField, Toolbar, Typography
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MenuIcon from '@material-ui/icons/Menu';
import logoImg from 'Assets/images/logo.png';
import GenericDialog from 'Components/GenericDialog';
import RegionDialog from 'Components/officials-home/RegionDialog';
import VotersDialog from 'Components/officials-home/VoterDialog';
import { useRouter, xFetch } from 'flitlib';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addNotification, editNotification, logout } from 'Redux/actions';
import styled from 'styled-components';

function OfficialsHomeScreen() {
    const [anchorEl, setAnchorEl] = useState(null);
    const [electionAnchorEl, setElectionAnchorEl] = useState(null);
    const [voterDialogOpen, setVoterDialogOpen] = useState(false);
    const [selectedRegion, setSelectedRegion] = useState(null);
    const dispatch = useDispatch();
    const { changePage } = useRouter();
    const user = useSelector(state => state.user);
    // TODO handle many elections
    const elections = useSelector(state => state.elections) || [];
    const [election, setElection] = useState(elections[0]);
    const [emailSubject, setEmailSubject] = useState('');
    const [emailText, setEmailText] = useState('');
    const [emailDialogOpen, setEmailDialogOpen] = useState(false);

    useEffect(() => {
        if (election) return;
        if (elections[0]) {
            setElection(elections[0]);
        }
    }, [election, elections]);

    const fetchResults = async () => {
        try {
            await xFetch('/elections/results', {
                body: {
                    electionId: election.electionId,
                }
            });
        } catch (err) {
            dispatch(addNotification({
                message: err.toString(),
                autoHideDuration: 8000,
            }));
        }
    }

    // if (!election) return null;
    const {
        title, description, electionCode, voters,
        startDate, endDate, regions, isLive,
    } = election || {};


    function getContent() {
        if (!election) return (
            <div id="content" className='empty' >
                <Typography variant='h4' >
                    No elections to show. <br />
                    Please contact the administrator.
                </Typography>
            </div>
        );

        return (
            <>
                <div id="content">
                    <div id="header">
                        <Typography variant='h4' >
                            {title}
                        </Typography>
                        <Typography variant='body1' >
                            <b>Election Code:</b> {electionCode}
                        </Typography>
                        {description &&
                            <Typography variant='h5' >
                                {description}
                            </Typography>
                        }

                        <div id="voters">
                            <Typography variant='h5' >
                                Voters registered: {voters?.length || 0}
                            </Typography>
                            {voters?.length &&
                                <Button
                                    variant='outlined' color='secondary'
                                    onClick={_ => setVoterDialogOpen(true)}
                                >
                                    More info
                                </Button>
                            }
                        </div>
                        <div id="time">
                            {new Date() > new Date(startDate) ?
                                <Typography>{`Voting ended ${moment(endDate).fromNow()}`}</Typography> :
                                <Typography>{`Voting is set to start ${moment(startDate).fromNow()}`}</Typography>
                            }
                        </div>
                    </div>

                    <Paper id='region-paper' >
                        <Typography variant='h5' >Regions</Typography>
                        <div id='regions' >
                            {regions?.map(r =>
                                <Paper key={r.regionId} elevation={5} className='region' >
                                    <Typography variant='h6' >{r.name}</Typography>
                                    <Divider />
                                    <Typography >
                                        Region Areas: {r.regionAreas?.length || 0}
                                    </Typography>
                                    <Typography >
                                        Polls: {r.polls?.length || 0}
                                    </Typography>
                                    <Typography >
                                        Candidates: {r.regionAreas?.reduce((acc, ra) => {
                                        return acc + ra.candidates?.length || 0;
                                    }, 0) || 0}
                                    </Typography>
                                    <Button color='primary' onClick={_ => setSelectedRegion(r)} >
                                        See More
                                    </Button>
                                </Paper>
                            )}
                        </div>
                    </Paper>
                </div>

                <Menu
                    open={electionAnchorEl != null}
                    onClose={() => setElectionAnchorEl(null)}
                    anchorEl={electionAnchorEl}
                >
                    {elections.map(el =>
                        <MenuItem
                            key={el.electionId} onClick={e => setElection(el)}
                            selected={election && election.electionId == el.electionId}
                        >
                            {el.electionCode}
                        </MenuItem>
                    )}
                </Menu>

                <VotersDialog
                    election={election} open={voterDialogOpen}
                    onClose={_ => setVoterDialogOpen(false)}
                />

                <RegionDialog
                    election={election} open={!!selectedRegion}
                    region={selectedRegion}
                    onClose={_ => setSelectedRegion(null)}
                />
            </>
        );
    }

    return (
        <Wrapper className='screen' >
            <AppBar color='transparent' elevation={0} >
                <Toolbar>
                    <div>
                        <img src={logoImg} alt='logo' onClick={e => changePage('/official')} />
                    </div>
                    <div>
                        {elections.length > 1 &&
                            <Button
                                onClick={e => setElectionAnchorEl(e.target)}
                                endIcon={<Icon> <ExpandMoreIcon /> </Icon>}
                            >
                                {electionCode}
                            </Button>
                        }
                        <IconButton onClick={e => setAnchorEl(e.target)} >
                            <MenuIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            <div id="spacer" />

            {getContent()}

            <Menu open={anchorEl != null} onClose={() => setAnchorEl(null)} anchorEl={anchorEl} >
                {new Date((election || {}).endDate) < new Date() &&
                    <MenuItem onClick={e => fetchResults()} >
                        Download Full Results
                    </MenuItem>
                }
                <MenuItem onClick={e => setEmailDialogOpen(true)} >Contact Admin</MenuItem>
                <MenuItem onClick={e => {
                    dispatch(logout());
                    changePage('/')
                }} >Logout</MenuItem>
            </Menu>

            <EmailDialog
                title='Admin Email' saveButtonLabel='Send' open={emailDialogOpen} wrapForm
                onClose={_ => setEmailDialogOpen(false)}
                onSave={async e => {
                    const key = dispatch(addNotification({
                        message: 'Sending email',
                        persist: true,
                        action: null,
                    }));
                    try {
                        await xFetch('/officials/contact-admin', {
                            method: 'POST',
                            body: {
                                subject: emailSubject,
                                message: emailText,
                            }
                        });
                        setEmailSubject('');
                        setEmailText('');
                        dispatch(editNotification({
                            key,
                            message: 'Email sent',
                        }));
                        setEmailDialogOpen(false);
                    } catch (err) {
                        dispatch(editNotification({
                            key,
                            message: err.toString(),
                            autoHideDuration: 7000,
                        }));
                    }
                }}
            >
                <TextField
                    label='Subject' value={emailSubject}
                    onChange={e => setEmailSubject(e.target.value)}
                />
                <TextField
                    multiline rows={5} placeholder='Type your message here...'
                    value={emailText} onChange={e => setEmailText(e.target.value)}
                />
            </EmailDialog>

        </Wrapper>
    )
}


const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    .MuiToolbar-root>div:last-child{
        display: flex;
        align-items: center;
    }
    >#content {
        flex: 1;
        padding: 3vmin;
        &.empty {
            display: flex;
            text-align: center;
            align-items: center;
            justify-content: center;
        }

        >#header {
            text-align: center;
            >#voters {
                display: flex;
                justify-content: center;
                align-items: center;
                margin-top: 2rem;
                >.MuiButton-root {
                    margin-left: 2rem;
                }
            }
        }

        >#region-paper {
            padding: 1rem;
            margin-top: 2.4rem;
            >#regions {
                padding: 1rem;
                display: flex;
                align-items: center;
                justify-content: flex-start;
                overflow-x: auto;
                >.region {
                    display: flex;
                    flex-direction: column;
                    align-items: flex-start;
                    padding: .4rem 1.6rem;
                    margin-right: 2rem;
                    .MuiDivider-root {
                        margin: auto -1.6rem;
                        width: calc(100% + 3.2rem);
                    }
                    .MuiTypography-root {
                        display: flex;
                        align-items: center;
                        white-space: nowrap;
                    }
                    .MuiButton-root {
                        align-self: flex-end;
                        margin-right: -1rem;
                    }
                }
            }
        }
    }
`;

/**@type {GenericDialog} */
const EmailDialog = styled(GenericDialog)`
    .MuiDialogContent-root > form {
        display: grid;
        row-gap: .4rem;
    }
`;

export default OfficialsHomeScreen;
