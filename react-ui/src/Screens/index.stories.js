import React from 'react';
import OfficialsHomeScreen from './OfficialsHomeScreen';
import styled from 'styled-components';
import LoginScreen from './LoginScreen';
import LandingScreen from './LandingScreen';
import VoterLoginScreen from './VoterLoginScreen';
import MobileAuthScreen from './MobileAuthScreen';
import AdminHomeScreen from './AdminHomeScreen';
import VoterHomeScreen from './VoterHomeScreen';

export default {
    title: 'Screens',
    decorators: [
        (storyFn =>
            <Wrapper>
                {storyFn()}
            </Wrapper>
        )
    ]
};

export const Landing = () => (
    <LandingScreen />
);
export const Login = () => (
    <LoginScreen />
);
export const VoterLogin = () => (
    <VoterLoginScreen />
);
export const MobileAuth = () => (
    <MobileAuthScreen />
);
export const AdminHome = () => (
    <OfficialsHomeScreen />
);
export const OfficerHome = () => (
    <AdminHomeScreen />
);
export const VoterHome = () => (
    <VoterHomeScreen />
);




const Wrapper = styled.div`
    background-color: ${({ theme }) => theme.palette.background.default};
    >.screen {
        height: 100vh;
        overflow-y: auto;
    }
    .MuiAppBar-root {
        >.MuiToolbar-root {
            >div {
                display: flex;
                align-items: center;
                >*:not(:last-child) {
                    margin-right: 1rem
                }
            }
            >div:first-child {
                flex-grow: 1;
            }
        }
        img {
            height: 48px;
            margin-right: 16px;
        }
    }
    .MuiTooltip-tooltip {
        font-size: ${({theme}) => theme.typography.body2.fontSize};
    }
    #spacer {
        ${({ theme }) => theme.mixins.toolbar}
    }
`;