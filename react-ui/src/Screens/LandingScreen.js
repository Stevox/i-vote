import { AppBar, Button, IconButton, Toolbar, Typography, useMediaQuery } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import logoImg from 'Assets/images/logo.png';
import voteImg from 'Assets/images/vote-img.jpg';
import { useRouter } from 'flitlib';
import React, { useState } from 'react';
import styled from 'styled-components';

function LandingScreen() {
    const [anchorEl, setAnchorEl] = useState(null);
    const isSmallScreen = useMediaQuery(theme => theme.breakpoints.down('sm'));
    const { changePage } = useRouter();

    return (
        <Wrapper className='screen' >
            <AppBar color='transparent' elevation={0} >
                <Toolbar>
                    <div>
                        <img src={logoImg} alt='logo' onClick={e => changePage('/')} />
                    </div>
                    {isSmallScreen ?
                        <IconButton color='inherit' onClick={e => setAnchorEl(e.target)} >
                            <MenuIcon />
                        </IconButton> :
                        <div id='action' >
                            <Button 
                                size='small' color='inherit' variant='text' 
                                onClick={e => changePage('/about')}
                            >
                                About Us
                            </Button>
                            <Button 
                                size='small' color='inherit' variant='text' 
                                onClick={e => changePage('/contact')}
                            >
                                Contact
                            </Button>
                            <Button
                                size='large' color='inherit' variant='outlined'
                                onClick={e => changePage('/login')}
                            >
                                Login
                            </Button>
                        </div>
                    }
                </Toolbar>
            </AppBar>
            <div id="spacer" />
            <div id="content">
                <div id="quotes">
                    <Typography variant='caption' className='quote' >
                        "Somewhere inside of all of us
                    </Typography>
                    <Typography variant='caption' className='quote' >
                        is the power to change the world."
                    </Typography>
                    <Typography className='author' >
                        Roald Dahl
                    </Typography>
                </div>
                <div id="action">
                    <Button
                        size='large' color='primary' variant='contained'
                        disableElevation
                        onClick={e => changePage('/voter/login')}
                    >
                        Cast Your Vote
                    </Button>
                    <Button
                        size='large' color='secondary' variant='outlined'
                        onClick={e => changePage('/signup')}
                    >
                        Organize A Vote
                    </Button>
                </div>
            </div>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    background-image: linear-gradient(to top,#fff,#fffd 0%,#fff2), url(${voteImg});
    background-repeat: no-repeat;
    background-position: 90% -60px;
    background-size: cover;
    @media screen and (min-width: ${({theme}) => theme.breakpoints.values.sm}px) {
        background-image: linear-gradient(to right, #fffb, #fffa, #fff3, #fff4), url(${voteImg});
        background-size: cover;
        background-position: 70% 0;
    }
    >.MuiAppBar-root {
        #action {
            display: grid;
            gap: 10px;
            grid-template-columns: repeat(3, 1fr);
            .MuiButton-text:hover {
                text-decoration: underline;
            }
        }
    }
    >#content {
        width: 100%;
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: end;
        align-items: start;
        padding: 60px 40px;
        margin-bottom: 5vmin;
        @media (max-width: ${({theme}) => theme.breakpoints.values.sm}px) {
            align-items: center;
            padding: 60px 15px;
        }

        >#quotes {
            font-style: italic;
            font-size: 1rem;
            .quote {
                display: block;
                font-size: 1.5em;
                font-weight: bold;
                &:not(:first-child) {
                    margin-left: 2rem;
                }
            }
            .author {
                font-size: 1.2rem;
                text-align: right;
            }
            @media screen and (max-width: ${({theme}) => theme.breakpoints.values.sm}px) {
                font-size: .8rem;
                .quote:not(:first-child) {
                    margin-left: 0;
                }
                .author {
                    text-align: left;
                }
            }
        }

        >#action {
            display: grid;
            grid-template-rows: 1fr 1fr;
            grid-gap: 10px;
            margin-top: 30px;
        }
    }
`;

export default LandingScreen
