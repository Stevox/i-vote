import { AppBar, Button, CircularProgress, TextField, Toolbar } from '@material-ui/core';
import logoImg from 'Assets/images/logo.png';
import { useRouter, useValidator, xFetch } from 'flitlib';
import React, { useState } from 'react';
import { addNotification, editNotification } from 'Redux/actions';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

function VoterLoginScreen() {
    const { values, setValue } = useValidator();
    const [loading, setLoading] = useState(false);
    const { changePage } = useRouter();
    const dispatch = useDispatch();

    async function handleLogin(e) {        
        e.preventDefault();
        setLoading(true);
        const key = dispatch(addNotification({
            message: 'Logging in',
            action: null,
            persist: true,
        }));
        try {
            await xFetch('/login', {
                method: 'POST',
                body: values,
            });
            dispatch(editNotification({
                key,
                message: 'Login successful',
            }));
            changePage('/auth');
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));
            setLoading(false);
        }
    }


    return (
        <Wrapper className='screen' >
            <AppBar color='transparent' elevation={0} >
                <Toolbar>
                    <img id='logo' src={logoImg} alt='logo' onClick={e => changePage('/')} />
                </Toolbar>
            </AppBar>
            <form id="content" onSubmit={handleLogin} >
                <TextField
                    label='Election Code' name='electionCode'
                    value={values.electionCode} onChange={setValue}
                    required autoFocus
                />
                <TextField
                    label='Voter ID Number' name='idFieldValue'
                    value={values.idFieldValue} onChange={setValue}
                    required
                />
                <Button
                    color='primary' type='submit' variant='contained'
                    disableElevation disabled={loading} size='large'
                    startIcon={loading && <CircularProgress />}
                >
                    Login
                </Button>
            </form>
        </Wrapper>
    )
}


const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    .MuiToolbar-root #logo {
        cursor: pointer;
    }
    
    >#content {
        width: 80%;
        max-width: 700px;
        display: grid;
        gap: 10px;
        .MuiButton-root {
            margin-top: 1rem;
            justify-self: center;
            min-width: 50%;
        }
    }
`;

export default VoterLoginScreen;
