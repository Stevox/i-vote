import { AppBar, Menu, MenuItem, Toolbar, Typography, Button, IconButton, useMediaQuery } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import logoImg from 'Assets/images/logo.png';
import ElectionCard from 'Components/admin-home-screen/ElectionCard';
import { useRouter } from 'flitlib';
import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from 'Redux/actions';
import styled from 'styled-components';
import ElectionDialog from 'Components/admin-home-screen/ElectionDialog';

function AdminHomeScreen() {
    const [anchorEl, setAnchorEl] = useState(null);
    const [electionDialogOpen, setElectionDialogOpen] = useState(false);
    const [selectedElection, setSelectedElection] = useState(null);
    const { changePage } = useRouter();
    const dispatch = useDispatch();
    const user = useSelector(state => state.user);
    const elections = useSelector(state => state.elections);
    const isMinWidthMd = useMediaQuery(theme => theme.breakpoints.up('md'));

    useEffect(() => {
        if (!elections || !selectedElection) return;
        const newElection = elections
            .find(el => selectedElection.electionId == el.electionId);
        setSelectedElection(newElection);
    }, [elections]);

    const appBarProps = useMemo(() => {
        if (elections?.length) {
            return {
                color: '#fff',
                elevation: 4,
            }
        } else {
            return {
                color: 'transparent',
                elevation: 0,
            }
        }
    }, [elections]);

    if (!user) {
        return 'Error: 401';
    }
    return (
        <Wrapper className='screen' >
            <AppBar {...appBarProps} >
                <Toolbar>
                    <div>
                        <img src={logoImg} alt='logo' onClick={e => changePage('/admin')} />
                    </div>
                    <div>
                        <Typography>{`${user.fullName}`}</Typography>
                        {isMinWidthMd && !!elections?.length &&
                            <Button
                                size='small' color='primary' variant='outlined'
                                onClick={e => {
                                    setSelectedElection(null);
                                    setElectionDialogOpen(true);
                                }}
                            >
                                Set Up A Vote
                            </Button>
                        }
                        <IconButton onClick={e => setAnchorEl(e.target)} >
                            <MenuIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            <div id="spacer" />
            <div id="content">
                {elections?.length ?
                    elections.map(el =>
                        <ElectionCard
                            key={el.electionId} election={el}
                            onEdit={_ => {
                                setSelectedElection(el);
                                setElectionDialogOpen(true);
                            }}
                        />
                    ) :
                    <div id='empty-content'>
                        <div id='heading' >
                            <Typography variant='h2' >
                                An Election is always better than a selection
                            </Typography>
                        </div>
                        <div id='action' >
                            <Button
                                size='large' color='primary' variant='contained'
                                onClick={e => {
                                    setSelectedElection(null);
                                    setElectionDialogOpen(true);
                                }}
                            >
                                Set Up A Vote
                            </Button>
                        </div>
                    </div>
                }
            </div>

            <Menu
                open={!!anchorEl} anchorEl={anchorEl}
                onClose={() => setAnchorEl(null)}
            >
                {!isMinWidthMd &&
                    <MenuItem
                        onClick={e => {
                            setSelectedElection(null);
                            setElectionDialogOpen(true);
                        }}
                    >
                        Set Up A Vote
                    </MenuItem>
                }
                <MenuItem onClick={e => {
                    dispatch(logout());
                    changePage('/');
                }} >Logout</MenuItem>
            </Menu>

            <ElectionDialog
                open={electionDialogOpen} onClose={_ => setElectionDialogOpen(false)}
                election={selectedElection}
            />
        </Wrapper>
    )
}


const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    .MuiToolbar-root {
        &>:last-child {
            display: flex;
            align-items: center;
            >:not(:last-child){
                margin-right: 1rem;
            }
        }
    }
    >#content {
        flex: 1;
        display: grid;
        row-gap: 1rem;
        padding: 3vmin;
        justify-items: center;
        align-content: flex-start;
        >#empty-content {
            max-width: 840px;
            text-align: center;
            align-self: center;
            >* {
                margin-bottom: 5rem;
            }
        }
    }
`;

export default AdminHomeScreen;
