import React, { useState } from 'react';
import styled from 'styled-components';
import logoImg from 'Assets/images/logo.png';
import {
    TextField, FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton, AppBar, Toolbar, Button, CircularProgress
} from '@material-ui/core';
import { useValidator, useRouter, xFetch } from 'flitlib';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { useDispatch } from 'react-redux';
import { addNotification, editNotification } from 'Redux/actions';

function SignupScreen() {
    const { values, setValue } = useValidator();
    const { changePage } = useRouter();
    const [loading, setLoading] = useState(false);
    const [showPassword, setShowPassword] = useState(false);

    const dispatch = useDispatch();

    async function handleSignup(e) {
        e.preventDefault();
        setLoading(true);
        const key = dispatch(addNotification({
            message: 'Signing up',
            action: null,
            persist: true,
        }));
        try {
            await xFetch('/sign-up', {
                method: 'POST',
                body: values,
            });
            dispatch(editNotification({
                key,
                message: 'Signup successful',
            }));
            changePage('/login');
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));
            setLoading(false);
        }
    }

    return (
        <Wrapper className='screen' >
            <AppBar color='transparent' elevation={0} >
                <Toolbar>
                    <img id='logo' src={logoImg} alt='logo' onClick={e => changePage('/')} />
                </Toolbar>
            </AppBar>
            <form id="content" onSubmit={handleSignup} >
                <TextField
                    label='Full Name' name='fullName' value={values.fullName}
                    onChange={setValue} autoFocus required fullWidth
                />
                <TextField
                    label='Phone' name='phone' value={values.phone}
                    onChange={setValue} required fullWidth
                />
                <TextField
                    label='Email' name='email' value={values.email}
                    onChange={setValue} required fullWidth
                />
                <FormControl variant='outlined' >
                    <InputLabel htmlFor='pswd-txt' required >Password</InputLabel>
                    <OutlinedInput
                        id='pswd-txt' type={showPassword ? 'text' : 'password'}
                        name='password' value={values.password} onChange={setValue}
                        required fullWidth label='Password' endAdornment={
                            <InputAdornment position='end'>
                                <IconButton onClick={e => setShowPassword(state => !state)} >
                                    {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>

                <Button
                    color='primary' type='submit' variant='contained'
                    disableElevation disabled={loading} size='large'
                    startIcon={loading && <CircularProgress />}
                >
                    Sign Up
                </Button>
                <Button
                    color='secondary' onClick={e => changePage('/login', { replace: true })}
                >
                    Have an account? Login Instead
                </Button>
            </form>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    .MuiToolbar-root #logo {
        cursor: pointer;
    }
    
    >#content {
        width: 80%;
        max-width: 700px;
        display: grid;
        gap: 10px;
        .MuiButton-root {
            margin-top: 1rem;
            justify-self: center;
            min-width: 50%;
        }
    }
`;

export default SignupScreen;
