import React, { useState } from 'react';
import styled from 'styled-components';
import logoImg from 'Assets/images/logo.png';
import mobileImg from 'Assets/images/mobile.png';
import { Typography, TextField, CircularProgress, Button, AppBar, Toolbar, useMediaQuery } from '@material-ui/core';
import { useValidator, useRouter, xFetch } from 'flitlib';
import { useDispatch, useSelector } from 'react-redux';
import { fetchData, addNotification, editNotification } from 'Redux/actions';

function MobileAuthScreen() {
    const { values, setValue } = useValidator();
    const user = useSelector(state => state.user);
    const [loading, setLoading] = useState(false);
    const { changePage } = useRouter();
    const isMinWidthMd = useMediaQuery(theme => theme.breakpoints.up("sm"));

    const dispatch = useDispatch();
    const handleSubmit = async (e) => {
        e.preventDefault();

        const key = dispatch(addNotification({
            message: 'Authenticating',
            action: null,
            persist: true,
        }));

        try {
            await xFetch('/login/verify-code', {
                method: 'POST',
                body: values,
            });
            const [userRes] = await dispatch(fetchData());
            const user = userRes.value;
            const { adminId, officialId, } = user;
            if (adminId) {
                changePage('/admin', { replace: true });
            } else if (officialId) {
                changePage('/official', { replace: true });
            } else {
                changePage('/voter', { replace: true });
            }

            dispatch(editNotification({
                key,
                message: 'Authentication successful',
            }));
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));
            setLoading(false);
        }
    }

    return (
        <Wrapper className='screen' >
            <AppBar color='#fff' elevation={0} >
                <Toolbar>
                    <img id='logo' src={logoImg} alt='logo' onClick={e => changePage('/')} />
                </Toolbar>
            </AppBar>
            {isMinWidthMd &&
                <img id='mobile' src={mobileImg} />
            }
            <form id="content" onSubmit={handleSubmit} >
                <Typography variant='body2' >
                    Enter the Verification code sent to your mobile device
                </Typography>
                <div id="input">
                    <TextField
                        label='Code' name='verificationCode' value={values.verificationCode}
                        onChange={setValue} required autoFocus
                    />
                    {/* TODO Appear after 10s */}
                    <Button
                        color='secondary' variant='text' size='small'
                        type='button' disabled={loading} type='button'
                        onClick={e => {
                            e.preventDefault()
                            xFetch('/login/resend-code');
                        }}
                    >
                        Resend Code
                    </Button>
                </div>
                <Button
                    color='primary' variant='contained' disableElevation
                    type='submit' disabled={loading} size='large'
                    startIcon={loading && <CircularProgress />}
                >
                    Submit
                </Button>
            </form>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    .MuiToolbar-root #logo {
        cursor: pointer;
    }
    >img#mobile {
        box-shadow: ${({ theme }) => theme.shadows[1]};
        border-radius: 27px;
        margin-right: 2rem;
        max-height: 90%;
    }
    >#content {
        width: 80%;
        max-width: 400px;
        display: flex;
        flex-direction: column;
        align-items: start;
        >#input {
            width: 100%;
            display: flex;
            padding: 10px 0;
        }
        >.MuiButton-root {
            align-self: flex-start;
            min-width: 30%;
            margin-top: 1.1rem;
        }
    }
`;

export default MobileAuthScreen;