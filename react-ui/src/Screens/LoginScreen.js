import { AppBar, Button, CircularProgress, TextField, Toolbar } from '@material-ui/core';
import logoImg from 'Assets/images/logo.png';
import { useRouter, useValidator, xFetch } from 'flitlib';
import React, { useState } from 'react';
import { addNotification, editNotification } from 'Redux/actions';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

function LoginScreen() {
    const { values, setValue } = useValidator();
    const [forgot, setForgot] = useState(false);
    const [loading, setLoading] = useState(false);
    const { changePage } = useRouter();
    const dispatch = useDispatch();

    async function handleLogin(e) {
        if (forgot) {
            return handleForgotPassword(e);
        }

        e.preventDefault();
        setLoading(true);
        const key = dispatch(addNotification({
            message: 'Logging in',
            action: null,
            persist: true,
        }));
        try {
            await xFetch('/login', {
                method: 'POST',
                body: values,
            });
            dispatch(editNotification({
                key,
                message: 'Login successful',
            }));
            changePage('/auth', { replace: true });
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));
            setLoading(false);
        }
    }

    const handleForgotPassword = async e => {
        e.preventDefault();
        setLoading(true);
        const key = dispatch(addNotification({
            message: 'Resetting account',
            action: null,
            persist: true,
        }));
        try {
            await xFetch('/login/forgot', {
                method: 'POST',
                body: values,
            });
            dispatch(editNotification({
                key,
                message: 'Account reset details have been sent to your email',
            }));
            changePage('/');
        } catch (err) {
            dispatch(editNotification({
                key,
                message: err.toString(),
                autoHideDuration: 8000,
            }));
            setLoading(false);
        }
    }


    return (
        <Wrapper className='screen' >
            <AppBar color='transparent' elevation={0} >
                <Toolbar>
                    <img id='logo' src={logoImg} alt='logo' onClick={e => changePage('/')} />
                </Toolbar>
            </AppBar>
            <form id="content" onSubmit={handleLogin} >
                <TextField
                    label='Email' type='email' name='email'
                    value={values.email} onChange={setValue}
                    required fullWidth autoFocus
                />
                {!forgot &&
                    <>
                        <TextField
                            label='Password' type='password' name='password'
                            value={values.password} onChange={setValue}
                            required fullWidth
                        />
                        <Button id='forgot' type='button' variant='text' onClick={e => setForgot(true)} >
                            Forgot Password
                        </Button>
                    </>
                }
                <div id="action">
                    <Button
                        color='primary' type='submit' variant='contained'
                        disableElevation disabled={loading}
                        startIcon={loading && <CircularProgress />}
                    >
                        {forgot ? 'Reset Account' : 'Login'}
                    </Button>
                    {forgot ?
                        <Button
                            variant='text' type='button'
                            onClick={() => setForgot(false)}
                        >
                            Cancel
                        </Button> :
                        <Button
                            color='secondary' variant='text' type='button'
                            onClick={() => changePage('/signup')}
                        >
                            Sign Up Instead
                        </Button>
                    }
                </div>
            </form>
        </Wrapper>
    )
}


const Wrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    .MuiToolbar-root #logo {
        cursor: pointer;
    }
    
    >#content {
        width: 80%;
        max-width: 700px;
        display: grid;
        gap: 10px;
        >#forgot {
            justify-self: flex-start;
        }
        >#action {
            margin-top: 20px;
            min-width: 50%;
            display: flex;
            justify-self: center;
            flex-direction: column;
            &>button:not(:last-child){
                margin-bottom: 10px
            }
            @media (min-width: ${({ theme }) => theme.breakpoints.values.sm}px) {
                justify-self: flex-end;
                flex-direction: row-reverse;
                justify-content: flex-start;
                &>*{
                    margin-right: 10px
                }

            }
        }
    }
`;

export default LoginScreen;
