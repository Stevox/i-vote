import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { login, fetchData } from 'Redux/actions';
import styled from 'styled-components';
import LoginScreen from 'Screens/LoginScreen';
import OfficialsHomeScreen from 'Screens/OfficialsHomeScreen';
import AdminHomeScreen from 'Screens/AdminHomeScreen';
import LandingScreen from 'Screens/LandingScreen';
import MobileAuthScreen from 'Screens/MobileAuthScreen';
import VoterHomeScreen from 'Screens/VoterHomeScreen';
import VoterLoginScreen from 'Screens/VoterLoginScreen';
import SignupScreen from 'Screens/SignupScreen';


function App() {
    const dispatch = useDispatch();
    const firstRender = useSelector(state => state.firstRender);
    
    useEffect(() => {
        if(firstRender){
            dispatch(fetchData());
        }
    }, [firstRender]);
    return (
        <Wrapper className="App">
            <Switch>
                <Route path='/login' component={LoginScreen} />
                <Route path='/signup' component={SignupScreen} />
                <Route path='/auth' component={MobileAuthScreen} />
                <Route path='/voter/login' component={VoterLoginScreen} />
                <Route path='/admin' component={AdminRouter} />
                <Route path='/official' component={OfficialRouter} />
                <Route path='/voter' component={VoterRouter} />
                <Route path='/' component={LandingScreen} />
            </Switch>
        </Wrapper>
    );
}


const AdminRouter = props => {
    const firstRender = useSelector(state => state.firstRender);
    const user = useSelector(state => state.user);
    const loading = useSelector(state => state.pending.length > 0);
    
    if (!user) {
        if (firstRender || loading) {
            return 'Loading...';
        } else {
            return (
                <Redirect to='/login' />
            )
        }
    }
    
    return (
        <Switch >
            <Route path={`${props.match.url}/`} component={AdminHomeScreen} />
        </Switch>
    );
}


const OfficialRouter = props => {
    const firstRender = useSelector(state => state.firstRender);
    const user = useSelector(state => state.user);
    const loading = useSelector(state => state.pending.length > 0);
    
    if (!user) {
        if (firstRender || loading) {
            return 'Loading...';
        } else {
            return (
                <Redirect to='/login' />
            )
        }
    }
    
    return (
        <Switch >
            <Route path={`${props.match.url}/`} component={OfficialsHomeScreen} />
        </Switch>
    );
}


const VoterRouter = props => {    
    const firstRender = useSelector(state => state.firstRender);
    const user = useSelector(state => state.user);
    const election = useSelector(state => state.election);
    const loading = useSelector(state => state.pending.length > 0);
    
    if (!user || !election) {
        if (firstRender || loading) {
            return 'Loading...';
        } else {
            return (
                <Redirect to={`${props.match.url}/login`} />
            )
        }
    }
    
    return (
        <Switch >
            <Route path={`${props.match.url}/`} component={VoterHomeScreen} />
        </Switch>
    );
}



const Wrapper = styled.div`
    background-color: ${({ theme }) => theme.palette.background.default};
    >.screen {
        height: 100vh;
        overflow-y: auto;
    }
    .MuiAppBar-root {
        >.MuiToolbar-root>div:first-child {
            flex-grow: 1;
        }
        img {
            height: 48px;
            margin-right: 16px;
        }
    }
    .MuiTooltip-tooltip {
        font-size: ${({theme}) => theme.typography.body2.fontSize};
    }
    #spacer {
        ${({ theme }) => theme.mixins.toolbar}
    }
`;


export default App;
