import MomentUtils from '@date-io/moment';
import { MuiThemeProvider } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import NotificationManager from 'Components/NotificationManager';
import { SnackbarProvider } from 'notistack';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import store from 'Redux/store';
import { ThemeProvider } from 'styled-components';
import theme from 'Utils/theme';
import App from './App';
import './index.css';

const RootApp = () => {
    return (
        <Provider store={store} >
            <MuiThemeProvider theme={theme} >
                <ThemeProvider theme={theme} >
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <BrowserRouter>
                            <SnackbarProvider
                                autoHideDuration={3500}
                                domRoot={document.getElementById('notification-root')}
                            >
                                <App />
                                <NotificationManager />
                            </SnackbarProvider>
                        </BrowserRouter>
                    </MuiPickersUtilsProvider>
                </ThemeProvider>
            </MuiThemeProvider>
        </Provider>
    );
}


export default RootApp;