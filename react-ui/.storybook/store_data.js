export const data = {
    user: {
        userId: '1',
        email: 'benzstevox@mailinator.com',
        firstName: 'Ben',
        lastName: 'Steve',
        otherNames: null,
        dob: null,
        gender: null,
        phone: '+254722980632',
        faceIdUrl: null,
        adminId: '1',
        hasVoted: false,
        regionAreaId: 2,
    },
    elections: [
        {
            electionId: '1',
            electionCode: 'y080k',
            title: 'Class Election',
            description: '',
            officialEmail: 'foo@email.com',
            creationDate: '2020-03-19T09:16:36.249Z',
            startDate: '2020-08-27T21:01:00.000Z',
            endDate: '2020-08-28T20:59:00.000Z',
            idFieldName: 'School ID Number',
            allowNullVotes: false,
            lowestRegionPollFirst: true,
            lowestRegionId: 1,
            isLive: false,
            adminId: '1',
            officials: [
                {
                    electionId: 1,
                    email: 'benzstevox@mailinator.com'
                }
            ],
            // First region is highest (i.e. has no parent)
            regions: [
                {
                    regionId: 1,
                    parentRegionId: null,
                    name: 'Class',
                    electionId: 1,
                    regionAreas: [
                        {
                            regionAreaId: 1,
                            regionId: 1,
                            name: 'Class 1A',
                            parentRegionAreaId: null,
                            candidates: [
                                {
                                    candidateId: 1,
                                    pollId: 1,
                                    name: 'Gonzalis',
                                    deputyName: 'Micheal',
                                    regionAreaId: 1,
                                    profilePicUrl: '',
                                },
                                {
                                    candidateId: 2,
                                    pollId: 1,
                                    name: 'Maria Matthews',
                                    deputyName: 'Janette Monae',
                                    regionAreaId: 1,
                                },
                                {
                                    candidateId: 4,
                                    pollId: 1,
                                    name: 'Alden Pendley ',
                                    deputyName: 'Eneida Jovel ',
                                    regionAreaId: 1,
                                },
                            ]
                        },
                        {
                            regionAreaId: 2,
                            regionId: 1,
                            name: 'Class 1B',
                            parentRegionAreaId: null,
                            candidates: [
                                {
                                    candidateId: 3,
                                    pollId: 1,
                                    name: 'Johnny Jones',
                                    deputyName: ' Kathryne Jarrard ',
                                    regionAreaId: 2,
                                },
                                {
                                    candidateId: 5,
                                    pollId: 1,
                                    name: 'Marni Sheikh',
                                    deputyName: ' Chance Kincaid',
                                    regionAreaId: 2,
                                },
                            ]
                        }
                    ],
                    polls: [
                        {
                            pollId: 1,
                            position: 'Class Representative',
                            regionId: 1,
                        },
                        {
                            pollId: 2,
                            position: 'Academic Representative',
                            regionId: 1,
                        }
                    ],
                }
            ],
            voters: [
                {
                    voterId: 1,
                    electionId: 1,
                    regionAreaId: 1,
                    regionAreaName: 'Class 1A',
                    fullName: 'Scott Wilhoite',
                    email: 'scott@email.com',
                    phone: '+254700123456',
                    idFieldValue: '620026223',
                },
                {
                    voterId: 2,
                    electionId: 1,
                    regionAreaId: 1,
                    regionAreaName: 'Class 1A',
                    fullName: 'Roxanna Sen',
                    email: 'sen@email.com',
                    phone: '+254700123156',
                    idFieldValue: '09821311689',
                },
                {
                    voterId: 3,
                    electionId: 1,
                    regionAreaId: 1,
                    regionAreaName: 'Class 1A',
                    fullName: 'Byron Ploof',
                    email: 'byron@email.com',
                    phone: '+254700123234',
                    idFieldValue: '8124652311',
                },
                {
                    voterId: 4,
                    electionId: 1,
                    regionAreaId: 2,
                    regionAreaName: 'Class 1B',
                    fullName: 'Syble Parkhill',
                    email: 'syble@email.com',
                    phone: '+254700987456',
                    idFieldValue: '92815623411',
                },
                {
                    voterId: 5,
                    electionId: 1,
                    regionAreaId: 2,
                    regionAreaName: 'Class 1B',
                    fullName: 'Edmond Haber',
                    email: 'haber@email.com',
                    phone: '+254700123456',
                    idFieldValue: '2781265689',
                },
                {
                    voterId: 6,
                    electionId: 1,
                    regionAreaId: 2,
                    regionAreaName: 'Class 1B',
                    fullName: 'Cory Ready',
                    email: 'cory@email.com',
                    phone: '+254703209656',
                    idFieldValue: '1234532280',
                },
            ],
        },
        {
            electionId: '2',
            electionCode: 'qfmh3',
            longCode: 'new-vote',
            title: 'New Vote',
            description: 'The new vote of 2020',
            officialEmail: 'foo@email.com',
            creationDate: '2020-05-04T08:01:08.234Z',
            startDate: '2020-07-28T00:01:00.000Z',
            endDate: '2020-07-29T23:59:00.000Z',
            idFieldName: 'ID Number',
            allowNullVotes: true,
            lowestRegionPollFirst: true,
            lowestRegionId: 3,
            isLive: false,
            adminId: '1',
            officials: [
                {
                    electionId: 2,
                    email: 'benzstevox@mailinator.com'
                }
            ],
            regions: [
                {
                    regionId: 2,
                    parentRegionId: null,
                    name: 'School',
                    electionId: 2,
                    regionAreas: [
                        {
                            regionAreaId: 3,
                            regionId: 2,
                            name: 'Engineering',
                            code: 'ELH',
                            parentRegionAreaId: null
                        },
                        {
                            regionAreaId: 4,
                            regionId: 2,
                            name: 'Education',
                            code: 'LLR',
                            parentRegionAreaId: null
                        }
                    ],
                },
                {
                    regionId: 3,
                    parentRegionId: 2,
                    name: 'Department',
                    electionId: 2,
                    regionAreas: [
                        {
                            regionAreaId: 5,
                            regionId: 3,
                            name: 'Computer Information Technology',
                            code: 'CIT',
                            parentRegionAreaId: 3
                        },
                        {
                            regionAreaId: 6,
                            regionId: 3,
                            name: 'Mechanical Engineering',
                            code: 'MENG',
                            parentRegionAreaId: 3
                        }
                    ],
                }
            ],
            votersNum: null
        }
    ],
    election: {
        electionId: '1',
        electionCode: 'y080k',
        title: 'Class Election',
        description: '',
        officialEmail: 'foo@email.com',
        startDate: '2020-08-20T21:01:00.000Z',
        endDate: '2020-08-25T20:59:00.000Z',
        idFieldName: 'School ID Number',
        allowNullVotes: false,
        lowestRegionPollFirst: true,
        lowestRegionId: 1,
        votesNum: 0,
        regions: [
            {
                regionId: 1,
                parentRegionId: null,
                name: 'Class',
                electionId: 1,
                regionAreas: [
                    {
                        regionAreaId: 1,
                        regionId: 1,
                        name: 'Class 1A',
                        parentRegionAreaId: null,
                        candidates: [
                            {
                                candidateId: 1,
                                pollId: 1,
                                name: 'Gonzalis',
                                deputyName: 'Micheal',
                                regionAreaId: 1,
                                votes: 100,
                            },
                            {
                                candidateId: 2,
                                pollId: 1,
                                name: 'Maria Matthews',
                                deputyName: 'Janette Monae',
                                regionAreaId: 1,
                                votes: 78,
                            },
                            {
                                candidateId: 4,
                                pollId: 1,
                                name: 'Alden Pendley ',
                                deputyName: 'Eneida Jovel ',
                                regionAreaId: 1,
                                votes: 45,
                            },
                        ]
                    },
                    {
                        regionAreaId: 2,
                        regionId: 1,
                        name: 'Class 1B',
                        parentRegionAreaId: null,
                        candidates: [
                            {
                                candidateId: 3,
                                pollId: 1,
                                name: 'Johnny Jones',
                                deputyName: ' Kathryne Jarrard ',
                                regionAreaId: 2,
                                votes: 207,
                            },
                            {
                                candidateId: 5,
                                pollId: 1,
                                name: 'Marni Sheikh',
                                deputyName: ' Chance Kincaid',
                                regionAreaId: 2,
                                votes: 23,
                            },
                        ]
                    }
                ],
                polls: [
                    {
                        pollId: 1,
                        position: 'Class Representative',
                        regionId: 1,
                    },
                    {
                        pollId: 2,
                        position: 'Academic Representative',
                        regionId: 1,
                    }
                ],
            }
        ],
    },
}