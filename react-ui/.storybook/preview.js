import React from 'react';
import { addDecorator } from '@storybook/react';
import { Provider } from 'react-redux';
import { data } from './store_data';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import * as reducers from 'Redux/reducers';
import NotificationManager from 'Components/NotificationManager';
import { SnackbarProvider } from 'notistack';
import '../src/index.css';

import StoryRouter from 'storybook-react-router';
import MomentUtils from '@date-io/moment';
import { ThemeProvider } from 'styled-components';
import { MuiThemeProvider } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import theme from 'Utils/theme';
 

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    combineReducers(reducers),
    data,
    composeEnhancers(applyMiddleware(thunk)),
);


addDecorator(StoryRouter())

addDecorator(storyFn =>
    <Provider store={store} >
        <MuiThemeProvider theme={theme} >
            <ThemeProvider theme={theme} >
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <SnackbarProvider
                        autoHideDuration={3500}
                        domRoot={document.getElementById('notification-root')}
                    >
                        {storyFn()}
                        <NotificationManager />
                    </SnackbarProvider>
                </MuiPickersUtilsProvider>
            </ThemeProvider>
        </MuiThemeProvider>
    </Provider>
)