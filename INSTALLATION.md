## Installation of the I-Vote server

### Minimum requirements
1. Core i3 processor or higher
1. 2 GB Memory or higher
1. 1 GB Hard Disk (Dependent on election size)

The following programs need to be installed. Please see the linked webpages for installation instructions specific to your machine:
1. NodeJS [Download](https://nodejs.org/en/download/)
1. PostgreSQL Database [Download](https://www.postgresql.org/download/)
1. Git [Download](https://git-scm.com/downloads)
1. While it would run on any Operating System, Debian is preferred [Download](https://www.debian.org/distrib/)


### Installation Instructions

1. Create the database and user to be used by the server.
1. Clone the repository (link in the [README](./README.md)) and follow any specific instructions detailed in the README.
1. Open your terminal and navigate into the cloned i-vote folder.
1. Run the command ```npm run build``` to build the front-end from the source.
1. Run the command ```npm run init-db``` to initialize the database.
1. Run the command ```npm start``` to start the server.
1. Open your browser and navigate to ```https://localhost:8000```
1. Set up the election as necessary


---------------------------

## Installation of the I-Vote peer server

Minimum requirements are similar to the main server.


### Installation Instructions

1. Create the database and user to be used by the server.
1. Clone the repository (link in the [README](./README.md)) and follow any specific instructions detailed in the README.
1. Open your terminal and navigate into the cloned i-vote-peer folder.
1. Run the command ```npm run init-db``` to initialize the database.
1. Run the command ```npm start``` to start the server.
1. A unique uri will be provisioned to the peer server to add it to the blockchain network.
1. No further action necessary.



---------------------------

## Testing
Run the command ```npm test``` from the project root directory to run the tests.