## I-Vote

The I-Vote Server Source Code. 
[Git repository](https://gitlab.com/Stevox/i-vote)

The peer server source code can be found in its own separate [repository](https://gitlab.com/Stevox/i-vote-peer)

--------------------

For license information, please see [LICENSE](./LICENSE)
For installation instructions, please see [INSTALLATION.md](./INSTALLATION.md)

--------------------

Please see the [.env.sample]() file in the server root for the required environment variables. Create a ".env" file in the same folder following the sample.

---------------------
#### Links:
- https://gitlab.com/Stevox/i-vote
- https://gitlab.com/Stevox/i-vote-peer