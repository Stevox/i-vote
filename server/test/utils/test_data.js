const mimic = require('mimeo-js').mimic;

module.exports.getSampleAdmin = (seed) => {
    return mimic({
        fullName: 'John Doe',
        email: (chance) => chance.email({ domain: 'ivote.com' }),
        phone: (chance) => `+254${chance.natural({ min: 100000000, max: 999999999 })}`,
        password: '123657',
    }, { seed });
}

module.exports.getSampleOfficial = (seed) => {
    return mimic({
        fullName: 'John Doe',
        email: 'officer@email.com',
        phone: (chance) => `0${chance.natural({ min: 100000000, max: 999999999 })}`,
        password: '123657',
    }, { seed });
}

module.exports.getSampleElection = (seed) => {
    const startDate = new Date();
    startDate.setMonth(startDate.getMonth() + 1);
    const endDate = new Date(startDate);
    endDate.setMonth(startDate.getMonth() + 1);

    return mimic({
        title: 'Class Election',
        description: 'Some description here',
        officialEmail: () => mimic('officer@email.com', { seed }),
        startDate,
        endDate,
        idFieldName: 'School ID Number',
        allowNullVotes: false,
        lowestRegionPollFirst: true,
        regions: [{ name: 'Region1' }, { name: 'Region2' }]
    }, { seed });
}

module.exports.getSampleRegionArea = (seed) => {
    return mimic({
        name: 'Region Area',
        code: '122',
    }, { seed });
}

module.exports.getSamplePoll = (seed) => {
    return mimic({
        position: 'Poll',
    }, { seed });
}

module.exports.getSampleCandidate = (seed) => {
    return mimic({
        name: 'Johnny Jones',
        deputyName: 'Kathryne Jarrard',
        party: 'Party',
    }, { seed });
}

module.exports.getSampleVoter = (seed) => {
    return mimic({
        fullName: 'Scott Wilhoite',
        email: 'scott@email.com',
        phone: (chance) => `+254${chance.natural({ min: 100000000, max: 999999999 })}`,
        idFieldValue: '620026223',
    }, { seed });
}

