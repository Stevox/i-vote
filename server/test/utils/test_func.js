const server = require('../../src');

const chai = require('chai');
const chaiHttp = require('chai-http');
const { getSampleOfficial, getSampleElection, getSampleRegionArea, getSampleVoter } = require('./test_data');
const { createNewOfficial, createNewElection, getElections, createNewRegionArea, createNewVoter, getVoters } = require('../../src/model/entities');
chai.use(chaiHttp);



/**
 * @param {{
*      email: string, password: string, 
*      idFieldValue: string, electionCode: string
 * }} user 
 * @returns {Promise<{
 *  agent: ChaiHttp.Agent,
 * }>}
 */
module.exports.login = async (user) => {
    const agent = chai.request.agent(server);
    let { status, text } = await agent
        .post('/login')
        .send(user);
    ({ status, text } = await agent
        .post('/login/verify-code')
        .send({
            verificationCode: process.env.MASTER_PASSWORD
        }));
    if (status >= 400) {
        console.log({status})
        throw new Error(text)
    }
    return ({ agent });
}

module.exports.createTestVoter = async function () {
    const official = getSampleOfficial();
    await createNewOfficial(official);
    let election = await createNewElection({
        ...getSampleElection(), officialEmail: official.email
    });
    const { electionId, electionCode } = election;
    election = { ...election, ...(await getElections({ electionId })) };
    const { regions } = election;
    // region-area
    const ra = getSampleRegionArea();
    ra.regionId = regions[0].regionId;
    const { regionAreaId } = await createNewRegionArea(ra);
    // candidate
    let voter = getSampleVoter();
    voter.regionAreaId = regionAreaId;
    voter.electionId = electionId;
    voter.electionCode = electionCode;
    const { voterId } = await createNewVoter(voter);
    voter = { ...voter, ...(await getVoters({ voterId })) };

    return { voter, election };
}