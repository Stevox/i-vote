process.env.NODE_ENV = 'test';
const runDbDef = require('../database/scripts/run-db-def');

const chai = require('chai');
const chaiHttp = require('chai-http');
const createAdmin = require('../scripts/create-admin');
const { getSampleAdmin } = require('./utils/test_data');

process.env.VERBOSITY = 0;
chai.use(chaiHttp);


before(() => {
    it('should set up the testing environment', async () => {
        await runDbDef();
        const admin = getSampleAdmin('admin'); // DO NOT EDIT THIS ADMIN!
        admin.adminLevel = 1;
        await createAdmin(admin);
    });
});