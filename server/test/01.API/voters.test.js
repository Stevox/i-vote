const server = require('../../src');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { 
    getSampleRegionArea, getSamplePoll, getSampleCandidate 
} = require('../utils/test_data');
const { 
    getElections, createNewRegionArea, createNewPoll, createNewCandidate, 
    getVoters, makeElectionLive 
} = require('../../src/model/entities');
const { login, createTestVoter } = require('../utils/test_func');
chai.use(chaiHttp);
const expect = chai.expect;

module.exports = function () {
    describe('/voters', () => {
        it('POST [/login] - Login as voter', async () => {
            const { voter } = await createTestVoter();
            const agent = chai.request.agent(server);
            const { status } = await agent
                .post('/login')
                .send(voter);
            expect(status).to.be.equal(204);
        });

        describe('/voters', () => {
            it('GET - Get election where voter is registered', async () => {
                const { voter } = await createTestVoter();
                const { agent } = await login(voter);
                const { status, body } = await agent
                    .get('/voters/election');
                expect(status).to.be.equal(200);
                expect(body).to.be.an('object').that.includes.keys('electionId');
            });
            it('POST [/vote] - Cast your vote', async () => {
                const { voter, election } = await createTestVoter();
                const { electionId, regions } = election;
                const { voterId } = voter;
                // poll
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                const { pollId } = await createNewPoll(poll);
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                // candidate
                const candidate = getSampleCandidate();
                candidate.regionAreaId = regionAreaId;
                candidate.pollId = pollId;
                const { candidateId } = await createNewCandidate(candidate);
                // candidate 2
                const candidate2 = getSampleCandidate();
                candidate2.regionAreaId = regionAreaId;
                candidate2.pollId = pollId;
                const { candidateId: candidateId2 } = await createNewCandidate(candidate2);

                await makeElectionLive({ electionId });
                const { agent } = await login(voter);
                const { status } = await agent
                    .post('/voters/election/vote')
                    .send({
                        electionId,
                        voterHash: voter.voterHash,
                        pollVotes: [{
                            pollId, candidateId,
                        }],
                        castDate: new Date()
                    })
                expect(status).to.be.equal(204);
                const { status: status2 } = await agent
                    .post('/voters/election/vote')
                    .send({
                        electionId,
                        voterHash: voter.voterHash,
                        pollVotes: [{
                            pollId, candidateId: candidateId2,
                        }],
                        castDate: new Date()
                    })
                // Should not vote twice
                expect(status2).to.be.equal(422);
                {
                    const voter = await getVoters({ voterId });
                    expect(voter.hasVoted).to.be.equal(true);
                    const election = await getElections({ electionId });
                    expect(election.votesNum).to.be.equal(1);
                    let checked = 0;
                    for (let r of (election.regions || [])) {
                        for (let ra of (r.regionAreas || [])) {
                            for (let candidate of (ra.candidates || [])) {
                                if(candidate.candidateId == candidateId){
                                    expect(candidate.votes).to.be.equal(1);
                                    checked++;
                                } else if(candidate.candidateId == candidateId2){
                                    expect(candidate2.votes).to.be.oneOf([null, undefined]);
                                    checked++;
                                }
                                if(checked === 2) break;
                            }
                            if(checked === 2) break;
                        }
                        if(checked === 2) break;
                    }
                }
            });
        });
    });
}