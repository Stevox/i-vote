const chai = require('chai');
const chaiHttp = require('chai-http');
const { getSampleAdmin, getSampleOfficial } = require('../utils/test_data');
const { createNewAdmin, createNewOfficial } = require('../../src/model/entities');
const server = require('../../src');
const { createTestVoter } = require('../utils/test_func');
chai.use(chaiHttp);
const expect = chai.expect;

module.exports = function () {
    describe('/login', () => {
        it('POST - Should log in an admin', async () => {
            const admin = getSampleAdmin();
            await createNewAdmin (admin);
            const agent = chai.request.agent(server);
            const { status } = await agent
                .post('/login')
                .send(admin);
            expect(status).to.equal(204);
        });
        it('POST - Should log in an official', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            const agent = chai.request.agent(server);
            const { status } = await agent
                .post('/login')
                .send(official);
            expect(status).to.equal(204);
        });
        it('POST - Should log in a voter', async () => {
            const { voter } = await createTestVoter();
            const agent = chai.request.agent(server);
            const { status, body } = await agent
                .post('/login')
                .send(voter);
            expect(status).to.be.equal(204);
        });
    });
}