const chai = require('chai');
const chaiHttp = require('chai-http');
const { getSampleAdmin, getSampleOfficial } = require('../utils/test_data');
const { createNewAdmin, createNewOfficial } = require('../../src/model/entities');
const { login, createTestVoter } = require('../utils/test_func');
chai.use(chaiHttp);
const expect = chai.expect;

module.exports = function () {
    describe('/users', () => {
        it('GET - Should get admin data', async () => {
            const admin = getSampleAdmin();
            await createNewAdmin(admin);
            const { agent } = await login(admin);
            const { status, body } = await agent
                .get('/users');
            expect(status).to.equal(200);
            expect(body).to.be.an('object').that.includes.keys('adminId');
        });
        it('GET - Should get official data', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            const { agent } = await login(official);
            const { status, body } = await agent
                .get('/users');
            expect(status).to.equal(200);
            expect(body).to.be.an('object').that.includes.keys('officialId');
        });
        it('GET - Should get voter data', async () => {
            const { voter } = await createTestVoter();
            const { agent } = await login(voter);
            const { status, body } = await agent
                .get('/users');
            expect(status).to.equal(200);
            expect(body).to.be.an('object').that.includes.keys('voterId', 'voterHash');
        });
    });
}