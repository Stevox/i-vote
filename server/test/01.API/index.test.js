describe('API', () => {    
    require('./admins.test')();
    require('./elections.test')();
    require('./login.test')();
    require('./officials.test')();
    require('./users.test')();
    require('./voters.test')();
});