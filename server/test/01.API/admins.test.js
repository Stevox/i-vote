const chai = require('chai');
const chaiHttp = require('chai-http');
const { getSampleAdmin } = require('../utils/test_data');
const { login } = require('../utils/test_func');
const { createNewAdmin } = require('../../src/model/entities/admins');
chai.use(chaiHttp);
const expect = chai.expect;

module.exports = function () {
    describe('/admins', () => {
        it('POST - Create new admin', async () => {
            const topAdmin = getSampleAdmin('admin');
            const admin1 = getSampleAdmin();
            const admin2 = getSampleAdmin();
            await createNewAdmin(admin1);
            {
                // Admin with level 2 privileges should not add new admins
                const { agent } = await login(admin1);
                const { status } = await agent
                    .post('/admins')
                    .send(admin2);
                expect(status).to.be.equal(401);
            }
            {
                const { agent } = await login(topAdmin);
                const { status } = await agent
                    .post('/admins')
                    .send(admin2);
                expect(status).to.be.equal(200);
            }
        });
        it('PUT - Edit own data', async () => {
            const admin = getSampleAdmin();
            await createNewAdmin(admin);
            const { agent } = await login(admin);
            const { status, body } = await agent
                .put('/admins')
                .send({ fullName: 'foo bar' });
            expect(status).to.be.equal(200);
            expect(body).to.be.an('object').that.includes.keys([
                'adminId', 'fullName',
            ]);
            expect(body.fullName).to.equal('Foo Bar');
        });
        it('DELETE - Delete admin', async () => {
            const topAdmin = getSampleAdmin('admin');
            const admin1 = getSampleAdmin();
            const admin2 = getSampleAdmin();
            await createNewAdmin(admin1);
            const { adminId: adminId2 } = await createNewAdmin(admin2);
            {
                // Admin with level 2 privileges should not delete other admins
                const { agent } = await login(admin1);
                const { status } = await agent
                    .delete('/admins')
                    .send({ adminId: adminId2 });
                expect(status).to.be.equal(401);
            }
            {
                const { agent } = await login(topAdmin);
                const { status } = await agent
                    .delete('/admins')
                    .send({ adminId: adminId2 });
                expect(status).to.be.equal(200);
            }
        });
    });
}