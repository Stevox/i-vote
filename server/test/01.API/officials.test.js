const { getSampleOfficial, getSampleAdmin } = require('../utils/test_data');
const { login } = require('../utils/test_func');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { createNewOfficial } = require('../../src/model/entities');
chai.use(chaiHttp);
const expect = chai.expect;

module.exports = function () {
    describe('/officials', () => {
        it('POST - Create new official', async () => {
            const admin = getSampleAdmin('admin');
            const official = getSampleOfficial();
            const { agent } = await login(admin);
            const { status, body } = await agent
                .post('/officials')
                .send(official);
            expect(status).to.be.equal(200);
            expect(body).to.be.an('object').that.includes.keys('officialId');
        });
        it('PUT - Edit own data', async () => {
            const official = getSampleOfficial();
            const { officialId } = await createNewOfficial(official)
            const { agent } = await login(official);
            const { status, body } = await agent
                .put('/officials')
                .send({ officialId, fullName: 'official' });
            expect(status).to.be.equal(200);
            expect(body.fullName).to.be.equal('Official');
        });
        it('DELETE - Delete official', async () => {
            {
                // Official cannot delete another official
                const official = getSampleOfficial();
                await createNewOfficial(official)
                const official2 = getSampleOfficial();
                const { officialId } = await createNewOfficial(official2)
                const { agent } = await login(official);
                const { status } = await agent
                .delete('/officials')
                .send({ officialId });
                expect(status).to.be.equal(401);
            }
            {
                const official = getSampleOfficial();
                const { officialId } = await createNewOfficial(official)
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status } = await agent
                    .delete('/officials')
                    .send({ officialId });
                expect(status).to.be.equal(200);
            }
        });
    });
}