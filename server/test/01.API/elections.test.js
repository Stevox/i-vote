const fs = require('fs');
const fsPromise = fs.promises;
const path = require('path');
const chai = require('chai');
const chaiHttp = require('chai-http');
const {
    getSampleAdmin, getSampleElection, getSampleOfficial, getSampleRegionArea,
    getSamplePoll, getSampleCandidate, getSampleVoter,
} = require('../utils/test_data');
const { login } = require('../utils/test_func');
const {
    createNewElection, createNewOfficial, getElections, createNewRegionArea,
    createNewPoll,
    createNewCandidate,
    createNewVoter,
} = require('../../src/model/entities');
chai.use(chaiHttp);
const expect = chai.expect;

module.exports = function () {
    describe('/elections', () => {
        it('GET - Get all elections [admin]', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            await createNewElection({
                ...getSampleElection(), officialEmail: official.email
            });
            await createNewElection({
                ...getSampleElection(), officialEmail: official.email
            });
            const admin = getSampleAdmin('admin');
            const { agent } = await login(admin);
            const { status, body } = await agent.get('/elections');
            expect(status).to.be.equal(200);
            expect(body).to.be.an('array').of.length.at.least(2);
        });

        it('GET - Get only registered elections [official]', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            await createNewElection({
                ...getSampleElection(), officialEmail: official.email
            });
            {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
            }
            const { agent } = await login(official);
            const { status, body } = await agent.get('/elections');
            expect(status).to.be.equal(200);
            expect(body).to.be.an('array').of.length(1);
        });

        it('POST - Create new election', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            const admin = getSampleAdmin('admin');
            const { agent } = await login(admin);
            const { status } = await agent
                .post('/elections')
                .send({
                    ...getSampleElection(), officialEmail: official.email
                });
            expect(status).to.be.equal(200);
        });

        it('POST [/go-live] - Make election live', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            const { electionId } = await createNewElection({
                ...getSampleElection(), officialEmail: official.email
            });
            const { regions } = await getElections({ electionId });
            // region-area
            const ra = getSampleRegionArea();
            ra.regionId = regions[0].regionId;
            const { regionAreaId } = await createNewRegionArea(ra);
            // candidate
            const voter = getSampleVoter();
            voter.regionAreaId = regionAreaId;
            voter.electionId = electionId;
            await createNewVoter(voter);

            const admin = getSampleAdmin('admin');
            const { agent } = await login(admin);
            const { status, body } = await agent
                .post('/elections/go-live')
                .send({ electionId });
            expect(status).to.be.equal(200);
            expect(body).to.include.keys('electionId');
        });

        it('PUT - Edit election', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            const { electionId } = await createNewElection({
                ...getSampleElection(), officialEmail: official.email
            });
            const admin = getSampleAdmin('admin');
            const { agent } = await login(admin);
            const { status, body } = await agent
                .put('/elections')
                .send({
                    title: 'Election Title',
                    electionId
                });
            expect(status).to.be.equal(200);
            expect(body.title).to.be.equal('Election Title');
        });

        it('DELETE - Delete election', async () => {
            const official = getSampleOfficial();
            await createNewOfficial(official);
            const { electionId } = await createNewElection({
                ...getSampleElection(), officialEmail: official.email
            });
            const admin = getSampleAdmin('admin');
            const { agent } = await login(admin);
            const { status } = await agent
                .delete('/elections')
                .send({
                    electionId
                });
            expect(status).to.be.equal(200);
        });

        // it('GET [/results] - Get election results file');



        describe('/region-areas', () => {
            it('POST - Create new region area', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/region-areas')
                    .send({
                        electionId, ...ra,
                    });
                expect(status).to.be.equal(200);
                expect(body).to.include.keys('regionAreaId');
            });

            it('GET [/template] - Get region areas template upload file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/region-areas/template');
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="region-areas-template.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
            });

            it('GET - Get existing region area records file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const num = 10, rNum = regions.length;
                for (let i = 0; i < num; i++) {
                    const ra = getSampleRegionArea();
                    ra.regionId = regions[0].regionId;
                    await createNewRegionArea(ra);
                }
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/region-areas')
                    .query({ electionId });
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="region-areas.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
                expect(text.split(/[\n]/).length).to.be.equal(num + 1);
            });

            it('POST - Create new region areas from file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // Create the file
                const lines = ['Region Area Name, Region Area Code, Region Name, Parent Region Area Name'], num = 10;
                for (let i = 0; i < num; i++) {
                    const { name, code } = getSampleRegionArea();
                    const ra = [name, code, regions[0].name];
                    lines.push(ra.join(', '));
                }
                const data = lines.join('\n');
                const filePath = path.resolve(__dirname, '../utils/test-files/ra.csv');
                await fsPromise.writeFile(filePath, data);
                // send file
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/region-areas')
                    .attach('file', fs.readFileSync(filePath), 'ra.csv')
                    .field('electionId', electionId);
                expect(status).to.be.equal(200);
                expect(body.num).to.be.equal(num);
            });

            it('PUT - Edit existing region area', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .put('/elections/region-areas')
                    .send({
                        regionAreaId, name: 'RegionArea',
                    });
                expect(status).to.be.equal(200);
                expect(body.name).to.equal('RegionArea');
            });

            it('DELETE - Delete existing region area', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status } = await agent
                    .delete('/elections/region-areas')
                    .send({
                        regionAreaId
                    });
                expect(status).to.be.equal(200);
            });
        });


        describe('/polls', () => {
            it('POST - Create new poll', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/polls')
                    .send({
                        electionId, ...poll,
                    });
                expect(status).to.be.equal(200);
                expect(body).to.include.keys('pollId');
            });

            it('GET [/template] - Get polls template upload file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/polls/template');
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="polls-template.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
            });

            it('GET - Get existing poll records file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const num = 10, rNum = regions.length;
                for (let i = 0; i < num; i++) {
                    const poll = getSamplePoll();
                    poll.regionId = regions[0].regionId;
                    await createNewPoll(poll);
                }
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/polls')
                    .query({ electionId });
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="polls.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
                expect(text.split(/[\n]/).length).to.be.equal(num + 1);
            });

            it('POST - Create new polls from file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // create file
                const lines = ['Position, Region Name'], num = 10;
                for (let i = 0; i < num; i++) {
                    const { position } = getSamplePoll();
                    const poll = [position, regions[0].name];
                    lines.push(poll.join(', '));
                }
                const data = lines.join('\n');
                const filePath = path.resolve(__dirname, '../utils/test-files/polls.csv');
                await fsPromise.writeFile(filePath, data);
                // send file
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/polls')
                    .attach('file', fs.readFileSync(filePath), 'polls.csv')
                    .field('electionId', electionId);
                expect(status).to.be.equal(200);
                expect(body.num).to.be.equal(num);
            });

            it('PUT - Edit existing poll', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                const { pollId } = await createNewPoll(poll);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .put('/elections/polls')
                    .send({
                        pollId, position: 'New position',
                    });
                expect(status).to.be.equal(200);
                expect(body.position).to.equal('New Position');
            });

            it('DELETE - Delete existing poll', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                const { pollId } = await createNewPoll(poll);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status } = await agent
                    .delete('/elections/polls')
                    .send({
                        pollId
                    });
                expect(status).to.be.equal(200);
            });
        });

        describe('/candidates', () => {
            it('POST - Create new candidate', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // poll
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                const { pollId } = await createNewPoll(poll);
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                // candidate
                const candidate = getSampleCandidate();
                candidate.regionAreaId = regionAreaId;
                candidate.pollId = pollId;
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/candidates')
                    .send({
                        electionId, ...candidate,
                    });
                expect(status).to.be.equal(200);
                expect(body).to.include.keys('candidateId');
            });

            it('GET [/template] - Get candidates template upload file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/candidates/template');
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="candidates-template.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
            });

            it('GET - Get existing candidate records file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const num = 10, rNum = regions.length;
                for (let i = 0; i < num; i++) {
                    const regionId = regions[0].regionId;
                    // poll
                    const poll = getSamplePoll();
                    poll.regionId = regionId;
                    const { pollId } = await createNewPoll(poll);
                    // region-area
                    const ra = getSampleRegionArea();
                    ra.regionId = regionId;
                    const { regionAreaId } = await createNewRegionArea(ra);
                    // candidate
                    const candidate = getSampleCandidate();
                    candidate.regionAreaId = regionAreaId;
                    candidate.pollId = pollId;
                    await createNewCandidate(candidate);
                }
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/candidates')
                    .query({ electionId });
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="candidates.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
                expect(text.split(/[\n]/).length).to.be.equal(num + 1);
            });

            it('POST - Create new candidates from file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // poll
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                await createNewPoll(poll);
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                await createNewRegionArea(ra);
                // Create the file
                const lines = ['Candidate Name, Deputy Name, Party, Profile Image Url, Position, Region Area Name']
                const num = 10;
                for (let i = 0; i < num; i++) {
                    const { name, deputyName, party } = getSampleCandidate();
                    const c = [name, deputyName, party, '', poll.position, ra.name];
                    lines.push(c.join(', '));
                }
                const data = lines.join('\n');
                const filePath = path.resolve(__dirname, '../utils/test-files/c.csv');
                await fsPromise.writeFile(filePath, data);
                // send file
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/candidates')
                    .attach('file', fs.readFileSync(filePath), 'c.csv')
                    .field('electionId', electionId);
                expect(status).to.be.equal(200);
                expect(body.num).to.be.equal(num);
            });

            it('PUT - Edit existing candidate', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // poll
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                const { pollId } = await createNewPoll(poll);
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                // candidate
                const candidate = getSampleCandidate();
                candidate.regionAreaId = regionAreaId;
                candidate.pollId = pollId;
                const { candidateId } = await createNewCandidate(candidate);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .put('/elections/candidates')
                    .send({
                        candidateId, party: 'party'
                    });
                expect(status).to.be.equal(200);
                expect(body.party).to.be.equal('Party');
            });

            it('DELETE - Delete existing candidate', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // poll
                const poll = getSamplePoll();
                poll.regionId = regions[0].regionId;
                const { pollId } = await createNewPoll(poll);
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                // candidate
                const candidate = getSampleCandidate();
                candidate.regionAreaId = regionAreaId;
                candidate.pollId = pollId;
                const { candidateId } = await createNewCandidate(candidate);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status } = await agent
                    .delete('/elections/candidates')
                    .send({
                        candidateId,
                    });
                expect(status).to.be.equal(200);
            });
        });

        describe('/voters', () => {
            it('POST - Create new voter', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                // voter
                const voter = getSampleVoter();
                voter.regionAreaId = regionAreaId;
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/voters')
                    .send({
                        electionId, ...voter,
                    });
                expect(status).to.be.equal(200);
                expect(body).to.include.keys('voterId');
            });

            it('GET [/template] - Get voters template upload file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/voters/template')
                    .query({ electionId });
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="voters-template.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
            });

            it('GET - Get existing voter records file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                const num = 10, rNum = regions.length;
                for (let i = 0; i < num; i++) {
                    // region-area
                    const ra = getSampleRegionArea();
                    ra.regionId = regions[0].regionId;
                    const { regionAreaId } = await createNewRegionArea(ra);
                    // candidate
                    const voter = getSampleVoter();
                    voter.regionAreaId = regionAreaId;
                    voter.electionId = electionId;
                    await createNewVoter(voter);
                }
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const res = await agent
                    .get('/elections/voters')
                    .query({ electionId });
                const { status, text, header } = res
                expect(status).to.be.equal(200);
                expect(header['content-disposition']).to.equal('attachment; filename="voters.csv"')
                expect(header['content-type']).to.match(/text\/csv/);
                expect(text).to.be.a('string').that.is.not.empty;
                expect(text.split(/[\n]/).length).to.be.equal(num + 1);
            });

            it('POST - Create new voters from file', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // region-area
                const pra = getSampleRegionArea();
                pra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(pra);
                const ra = getSampleRegionArea();
                ra.regionId = regions[1].regionId;
                ra.parentRegionAreaId = regionAreaId;
                await createNewRegionArea(ra);
                // Create the file
                const lines = ['Voter Name*, email*, phone*, ID Number*, Region Area Name*']
                const num = 10;
                for (let i = 0; i < num; i++) {
                    const { fullName, email, phone, idFieldValue } = getSampleVoter();
                    const voter = [
                        fullName, email, phone, idFieldValue, ra.name
                    ];
                    lines.push(voter.join(', '));
                }
                const data = lines.join('\n');
                const filePath = path.resolve(__dirname, '../utils/test-files/voters.csv');
                await fsPromise.writeFile(filePath, data);
                // send file
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .post('/elections/voters')
                    .attach('file', fs.readFileSync(filePath), 'voters.csv')
                    .field('electionId', electionId);
                expect(status).to.be.equal(200);
                expect(body.num).to.be.equal(num);
            });

            it('PUT - Edit existing voter', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                // candidate
                const voter = getSampleVoter();
                voter.regionAreaId = regionAreaId;
                voter.electionId = electionId;
                const { voterId } = await createNewVoter(voter);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .put('/elections/voters')
                    .send({
                        voterId, fullName: 'voter',
                    });
                expect(status).to.be.equal(200);
                expect(body.fullName).to.be.equal('Voter');
            });

            it('DELETE - Delete existing voter', async () => {
                const official = getSampleOfficial();
                await createNewOfficial(official);
                const { electionId } = await createNewElection({
                    ...getSampleElection(), officialEmail: official.email
                });
                const { regions } = await getElections({ electionId });
                // region-area
                const ra = getSampleRegionArea();
                ra.regionId = regions[0].regionId;
                const { regionAreaId } = await createNewRegionArea(ra);
                // candidate
                const voter = getSampleVoter();
                voter.regionAreaId = regionAreaId;
                voter.electionId = electionId;
                const { voterId } = await createNewVoter(voter);
                const admin = getSampleAdmin('admin');
                const { agent } = await login(admin);
                const { status, body } = await agent
                    .delete('/elections/voters')
                    .send({
                        voterId,
                    });
                expect(status).to.be.equal(200);
            });
        });
    });
}