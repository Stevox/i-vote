const { changeCase, BaseJoi } = require('./schema-ext');

const Joi = BaseJoi

const voterSchema = Joi.object({
    fullName: Joi.string().custom(changeCase('title')).alter({
        new: s => s.required()
    }),
    email: Joi.string().email().alter({
        new: s => s.required()
    }),
    phone: Joi.string().alter({
        new: s => s.required()
    }),
    profilePicUrl: Joi.string().allow(null),
    regionAreaId: Joi.number().alter({
        new: s => s.required()
    }),
    idFieldValue: Joi.string().alter({
        new: s => s.required()
    }),
});


/**@type {import('joi').ObjectSchema} */
module.exports.newVoterSchema = voterSchema.tailor('new').keys({
    electionId: Joi.number().required(),
});

module.exports.editVoterSchema = voterSchema.keys({
    voterId: Joi.number().required(),
});

module.exports.deleteVoterSchema = Joi.object({
    voterId: Joi.number().required(),
});





module.exports.getVotersSchema = Joi.object({
    voterId: Joi.number().required(),
});

module.exports.getVoterElectionSchema = Joi.object({
    voterId: Joi.number().required(),
});

module.exports.getVoterElectionResponseSchema = Joi.object({
    electionId: Joi.number().required(),
    regions: Joi.array().items(Joi.object({
        regionId: Joi.number().required(),
        parentRegionId: Joi.number().allow(null).required(),
        name: Joi.string().required(),
        electionId: Joi.number().required(),
        polls: Joi.array().items(Joi.object({
            pollId: Joi.number().required(),
            position: Joi.string().required(),
            regionId: Joi.number().required(),
        })).allow(null).required(),
        regionAreas: Joi.array().items(Joi.object({
            regionAreaId: Joi.number().required(),
            regionId: Joi.number().required(),
            name: Joi.string().required(),
            parentRegionAreaId: Joi.number().allow(null).required(),
            candidates: Joi.array().items(Joi.object({
                candidateId: Joi.number().required(),
                name: Joi.string(),
                deputyName: Joi.string().allow(null),
                party: Joi.string().allow(null),
                profilePicUrl: Joi.string().allow(null),
                pollId: Joi.number(),
                regionAreaId: Joi.number(),
                votes: Joi.number().allow(null),
            })).allow(null).required(),
        })).allow(null).required(),
    })).allow(null).required(),
    votesNum: Joi.number().allow(null),
    peerServerUris: Joi.array().items(Joi.string()).empty(null).default([]),
    // strip
    voters: Joi.any().strip(),
    officialId: Joi.any().strip(),
    officials: Joi.any().strip(),
});

module.exports.authenticateVoterSchema = Joi.object({
    idFieldValue: Joi.string().required(), 
    electionCode: Joi.string().required(),
});

module.exports.verifyVoterCodeSchema = Joi.object({
    voterId: Joi.number().required(),
    verificationCode: Joi.string().required(), 
});

module.exports.castVoteSchema = Joi.object({
    voterId: Joi.number().required(),
    electionId: Joi.number().required(),
    voterHash: Joi.string().required(), 
    castDate: Joi.date().required(), 
    pollVotes: Joi.array().items(Joi.object({
        pollId: Joi.number().required(),
        candidateId: Joi.number().required(),
    })).required(), 
});

module.exports.castVoteAsPeerCoordinatorSchema = Joi.object({
    electionId: Joi.number().required(),
    voterHash: Joi.string().required(), 
    castDate: Joi.date().required(), 
    pollVotes: Joi.array().items(Joi.object({
        pollId: Joi.number().required(),
        candidateId: Joi.number().required(),
    })).required(), 
});
