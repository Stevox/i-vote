const { changeCase, passwordSchema, BaseJoi } = require('./schema-ext');

const Joi = BaseJoi

const adminSchema = Joi.object({
    fullName: Joi.string().custom(changeCase('title')).alter({
        new: s => s.required()
    }),
    email: Joi.string().email().lowercase().trim().alter({
        new: s => s.required()
    }),
    phone: Joi.string().allow(null),
    password: Joi.string().trim().alter({
        new: s => s.required(),
        getRes: s => s.forbidden(),
    }),
    adminLevel: Joi.number().default(2),
    profilePicUrl: Joi.string().allow(null),
});



// Exports

module.exports.getAdminsRequestSchema = Joi.object({
    adminId: Joi.number().required(),
    adminLevel: Joi.number(),
    all: Joi.bool(),
}).required();
/**@type {import('joi').ObjectSchema} */
module.exports.getAdminsResponseSchema = adminSchema.tailor('getRes').keys({
    adminId: Joi.number().required(),
    passwordHash: Joi.string().strip(),
    passwordSalt: Joi.string().strip(),
    resetHash: Joi.string().strip().allow(null),
}).allow(null);

/**@type {import('joi').ObjectSchema} */
module.exports.newAdminSchema = adminSchema.tailor('new').required();

/**@type {import('joi').ObjectSchema} */
module.exports.editAdminSchema = adminSchema.tailor('edit').keys({
    adminId: Joi.number().required(),
    newPassword: passwordSchema,
    currentPassword: Joi.string().when('newPassword', {
        is: Joi.exist(),
        then: Joi.string().required(),
    }),
}).required();

module.exports.deleteAdminSchema = Joi.object({
    adminId: Joi.number().required(),
    undo: Joi.bool(),
}).required();


module.exports.authenticateAdminSchema = Joi.object({
    email: Joi.string().email().lowercase().trim().required(),
    password: passwordSchema.required(),
}).required();


module.exports.resetAdminPasswordSchema = Joi.object({
    email: Joi.string().email().lowercase().trim().required(),
    password: passwordSchema.required(),
    resetHash: Joi.string().required(),
}).and('resetHash', 'password').required();


module.exports.verifyAdminCodeSchema = Joi.object({
    adminId: Joi.number().required(),
    verificationCode: Joi.string().required(),
}).required();

