module.exports = {
    ...require('./admin-schema'),
    ...require('./election-schema'),
    ...require('./official-schema'),
    ...require('./voter-schema'),
}