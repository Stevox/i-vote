const { changeCase, passwordSchema, BaseJoi } = require('./schema-ext');

const Joi = BaseJoi;


const officialSchema = Joi.object({
    fullName: Joi.string().custom(changeCase('title')).alter({
        new: s => s.required()
    }),
    email: Joi.string().email().lowercase().trim().alter({
        new: s => s.required()
    }),
    phone: Joi.string().allow(null),
    password: passwordSchema.alter({
        new: s => s.required(),
        getRes: s => s.forbidden(),
    }),
    profilePicUrl: Joi.string().allow(null),
});



// Exports

/**@type {import('joi').ObjectSchema} */
module.exports.getOfficialsResponseSchema = officialSchema.tailor('getRes').keys({
    officialId: Joi.number().required(),
    passwordHash: Joi.string().strip(),
    passwordSalt: Joi.string().strip(),
    resetHash: Joi.string().strip().allow(null),
}).allow(null);


/**@type {import('joi').ObjectSchema} */
module.exports.newOfficialSchema = officialSchema.tailor('new').required();

module.exports.inviteNewOfficialSchema = Joi.object({
    email: Joi.string().email().lowercase().trim().required(),
    fullName: Joi.string().trim().required(),
    phone: Joi.string().allow(null),
    electionId: Joi.number().required(),
}).required();

module.exports.confirmOfficialAccountSchema = Joi.object({
    email: Joi.string().email().lowercase().trim().required(),
    confirmationHash: Joi.string().required(),
}).required();

/**@type {import('joi').ObjectSchema} */
module.exports.editOfficialSchema = officialSchema.tailor('edit').keys({
    officialId: Joi.number().required(),
    newPassword: passwordSchema,
    currentPassword: passwordSchema.when('newPassword', {
        is: Joi.exist(),
        then: Joi.string().required(),
    }),
}).required();

module.exports.deleteOfficialSchema = Joi.object({
    officialId: Joi.number().required(),
    undo: Joi.bool(),
}).required();


module.exports.authenticateOfficialSchema = Joi.object({
    email: Joi.string().email().lowercase().trim().required(),
    password: passwordSchema.required(),
}).required();

module.exports.verifyOfficialCodeSchema = Joi.object({
    officialId: Joi.number().required(),
    verificationCode: Joi.string().required(),
}).required();

module.exports.resetOfficialPasswordSchema = Joi.object({
    email: Joi.string().email().lowercase().trim().required(),
    password: passwordSchema.required(),
    resetHash: Joi.string().required(),
}).and('resetHash', 'password').required();