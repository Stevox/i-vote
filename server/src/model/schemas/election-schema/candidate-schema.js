const { changeCase, BaseJoi } = require('../schema-ext');

const Joi = BaseJoi;

const candidateSchema = Joi.object({
    name: Joi.string().custom(changeCase('title')).alter({
        new: s => s.required()
    }),
    deputyName: Joi.string().custom(changeCase('title')).allow(null),
    party: Joi.string().custom(changeCase('title')).allow(null),
    profilePicUrl: Joi.string().allow(null),
    pollId: Joi.number().alter({
        new: s => s.required()
    }),
    regionAreaId: Joi.number().alter({
        new: s => s.required()
    }),
});


/**@type {import('joi').ObjectSchema} */
module.exports.newCandidateSchema = candidateSchema.tailor('new');

module.exports.editCandidateSchema = candidateSchema.keys({
    candidateId: Joi.number().required(),
});

module.exports.deleteCandidateSchema = Joi.object({
    candidateId: Joi.number().required(),
});

module.exports.loadCandidateFileSchema = Joi.object({
    electionId: Joi.number().required(),
    file: Joi.object({
        path: Joi.string().required(),
    }),
});