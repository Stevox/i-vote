const { changeCase, BaseJoi } = require('../schema-ext');

const Joi = BaseJoi;

const voterSchema = Joi.object({
    fullName: Joi.string().custom(changeCase('title')).alter({
        new: s => s.required()
    }),
    email: Joi.string().email().alter({
        new: s => s.required()
    }),
    phone: Joi.string().alter({
        new: s => s.required()
    }),
    profilePicUrl: Joi.string().allow(null),
    regionAreaId: Joi.number().alter({
        new: s => s.required()
    }),
    idFieldValue: Joi.string().alter({
        new: s => s.required()
    }),
});


/**@type {import('joi').ObjectSchema} */
module.exports.newVoterSchema = voterSchema.tailor('new').keys({
    electionId: Joi.number().required(),
});

module.exports.editVoterSchema = voterSchema.keys({
    voterId: Joi.number().required(),
});

module.exports.deleteVoterSchema = Joi.object({
    voterId: Joi.number().required(),
});

module.exports.loadVoterFileSchema = Joi.object({
    electionId: Joi.number().required(),
    file: Joi.object({
        path: Joi.string().required(),
    }),
});