const { changeCase, BaseJoi } = require('../schema-ext');

const Joi = BaseJoi;

const regionAreaSchema = Joi.object({
    name: Joi.string().custom(changeCase('title')).alter({
        new: s => s.required()
    }),
    regionId: Joi.number().alter({
        new: s => s.required()
    }),
    code: Joi.string(),
    parentRegionAreaId: Joi.number().allow(null),
});


/**@type {import('joi').ObjectSchema} */
module.exports.newRegionAreaSchema = regionAreaSchema.tailor('new');

module.exports.editRegionAreaSchema = regionAreaSchema.keys({
    regionAreaId: Joi.number().required(),
});

module.exports.deleteRegionAreaSchema = Joi.object({
    regionAreaId: Joi.number().required(),
});

module.exports.loadRegionAreaFileSchema = Joi.object({
    electionId: Joi.number().required(),
    file: Joi.object({
        path: Joi.string().required(),
    }),
});