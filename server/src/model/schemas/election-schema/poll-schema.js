const { changeCase, BaseJoi } = require('../schema-ext');

const Joi = BaseJoi;

const pollSchema = Joi.object({
    position: Joi.string().custom(changeCase('title')).alter({
        new: s => s.required()
    }),
    regionId: Joi.number().alter({
        new: s => s.required()
    }),
});


/**@type {import('joi').ObjectSchema} */
module.exports.newPollSchema = pollSchema.tailor('new');

module.exports.editPollSchema = pollSchema.keys({
    pollId: Joi.number().required(),
});

module.exports.deletePollSchema = Joi.object({
    pollId: Joi.number().required(),
});

module.exports.loadPollFileSchema = Joi.object({
    electionId: Joi.number().required(),
    file: Joi.object({
        path: Joi.string().required(),
    }),
});