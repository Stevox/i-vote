const { changeCase, BaseJoi } = require('../schema-ext');
const Joi = BaseJoi;

const minDate = new Date();
minDate.setDate(minDate.getDate() + 13); // About 2 weeks



const electionSchema = Joi.object({
    title: Joi.string().alter({
        new: s => s.required()
    }).custom(changeCase('title')),
    description: Joi.string().allow(null).custom(changeCase('sentence')),
    startDate: Joi.date().alter({
        new: s => s.min(minDate).required()
    }),
    endDate: Joi.date().alter({
        new: s => s.min(Joi.ref('startDate')).required()
    }),
    peerServersNum: Joi.number().default(0).min(0).max(100).allow(null),
    idFieldName: Joi.string().allow(null).custom(changeCase('title')),
    allowNullVotes: Joi.boolean().allow(null),
    lowestRegionPollFirst: Joi.bool().allow(null),
    logoFileUrl: Joi.string().allow(null),
});


// Exports

module.exports.getElectionsRequestSchema = Joi.object({
    officialId: Joi.number(),
    adminId: Joi.number(),
    electionId: Joi.number(),
}).or('officialId', 'adminId', 'electionId');
/**@type {import('joi').ObjectSchema} */
module.exports.getElectionsResponseSchema = Joi.array().items(electionSchema.tailor('getRes').keys({
    electionId: Joi.number().required(),
    officialId: Joi.number().required(),
    votesNum: Joi.number().allow(null),
    officials: Joi.array().items(Joi.object({
        officialId: Joi.number().required(),
        email: Joi.string().email().lowercase().trim().required(),
        fullName: Joi.string().required(),
        phone: Joi.string().allow(null),
    })).allow(null).required(),
    regions: Joi.array().items(Joi.object({
        regionId: Joi.number().required(),
        parentRegionId: Joi.number().allow(null).required(),
        name: Joi.string().required(),
        electionId: Joi.number().required(),
        polls: Joi.array().items(Joi.object({
            pollId: Joi.number().required(),
            position: Joi.string().required(),
            regionId: Joi.number().required(),
        })).allow(null).required(),
        regionAreas: Joi.array().items(Joi.object({
            regionAreaId: Joi.number().required(),
            regionId: Joi.number().required(),
            name: Joi.string().required(),
            parentRegionAreaId: Joi.number().allow(null).required(),
            candidates: Joi.array().items(Joi.object({
                candidateId: Joi.number().required(),
                name: Joi.string(),
                deputyName: Joi.string().allow(null),
                party: Joi.string().allow(null),
                profilePicUrl: Joi.string().allow(null),
                pollId: Joi.number(),
                regionAreaId: Joi.number(),
                votes: Joi.number().allow(null),
            })).allow(null).required(),
        })).allow(null).required(),
    })).allow(null).required(),
    voters: Joi.array().items(Joi.object({
        voterId: Joi.number().required(),
        voteEncryptionKey: Joi.any().strip(),
        // TODO
    })).allow(null)
}));


/**@type {import('joi').ObjectSchema} */
module.exports.newElectionSchema = electionSchema.tailor('new').keys({
    officialEmail: Joi.string().email().required(),
    regions: Joi.array().items(Joi.object({
        name: Joi.string().custom(changeCase('title')).required(),
    })).min(1).required(),
}).required();
module.exports.editElectionSchema = electionSchema.keys({
    electionId: Joi.number().required(),
    officialEmail: Joi.string().email(),
    regions: Joi.array().items(Joi.object({
        regionId: Joi.number(),
        name: Joi.string().custom(changeCase('title')),
    })).min(1),
}).required();

module.exports.deleteElectionSchema = Joi.object({
    electionId: Joi.number().required(),
}).required();

module.exports.setElectionOfficialsSchema = Joi.object({
    electionId: Joi.number().required(),
    officialEmails: Joi.array().items(Joi.string().email()).required().allow(null),
}).required();

module.exports.makeElectionLiveSchema = Joi.object({
    electionId: Joi.number().required(),
}).required();