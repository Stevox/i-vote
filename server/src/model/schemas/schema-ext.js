const Joi = require("joi");


module.exports.BaseJoi = Joi.defaults(schema => {
    switch (schema.type) {
        case 'object': return schema.unknown(true);
        case 'bool': return schema.empty('');
        default: return schema.empty('');
    }
});


module.exports.changeCase = function (newCase) {
    switch (newCase) {
        case 'title': return (v) => v.replace(/((?:^|[ ])(\w))/g, m => m.toUpperCase());
        case 'sentence': return (v) => v.replace(/(\w)/, m => m.toUpperCase());

        default: return () => v;
    }
}

module.exports.passwordSchema = Joi.string().pattern(/^\S.+?\S$/, { name: '"no start or ending spaces"' });


// TODO Base Joi