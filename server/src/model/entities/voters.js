const { snakeToCamelCase, cipher, ServerError } = require('litlib');
const { broadcastVoteToPeers, getRegisteredPeerUris } = require('@app/utils');
const Joi = require('joi');
const { Db } = require('litlib/ldb');
const { 
    getVotersSchema, getVoterElectionSchema, authenticateVoterSchema, 
    verifyVoterCodeSchema, castVoteSchema, getVoterElectionResponseSchema,
    castVoteAsPeerCoordinatorSchema,
} = require('../schemas/voter-schema');
const db = new Db();


async function getVoters({ voterId }) {
    ({
        voterId
    } = await Joi.attempt(arguments[0], getVotersSchema));
    
    const q1 = {
        text: `SELECT * FROM voters WHERE voter_id = $1`,
        values: [voterId]
    }
    const r1 = await db.execute(q1);
    const voter = snakeToCamelCase(r1.rows[0]);
    voter.voterHash = cipher.hashText(voter.voterId, process.env.SECRET);
    return voter;
}

async function getVoterElection({ voterId }) {
    ({
        voterId
    } = await Joi.attempt(arguments[0], getVoterElectionSchema));

    const q1 = {
        text: `SELECT * FROM elections_v WHERE election_id = 
            (SELECT election_id FROM voters WHERE voter_id = $1)`,
        values: [voterId]
    }
    const r1 = await db.execute(q1);
    const data = snakeToCamelCase(r1.rows[0]);
    data.peerServerUris = getRegisteredPeerUris(data.electionId);
    const resp = await Joi.attempt(data, getVoterElectionResponseSchema);
    return resp;
}


async function authenticateVoter({ idFieldValue, electionCode }) {
    ({
        idFieldValue, electionCode
    } = await Joi.attempt(arguments[0], authenticateVoterSchema));
    
    const q1 = {
        text: `SELECT * FROM voters 
            WHERE id_field_value = $1 AND election_id = ANY (
                SELECT election_id FROM elections WHERE election_code = $2
            ) LIMIT 1`,
        values: [idFieldValue, electionCode]
    }
    const vCode = Math.random().toString().slice(2, 7);
    const q2 = {
        text: `UPDATE voters SET verification_code = $1 WHERE voter_id = #voter_id# RETURNING *`,
        values: [vCode]
    }
    try {
        const [, r2] = await db.execute([q1, q2]);
        if (!r2.rows[0]) {
            throw new Error('No such voter found');
        }
        const resp = snakeToCamelCase(r2.rows[0]);
        return resp;
    } catch (err) {
        throw new ServerError(401, 'Invalid voter ID or election code', err);
    }
}

async function verifyVoterCode({ voterId, verificationCode }) {
    ({
        voterId, verificationCode
    } = await Joi.attempt(arguments[0], verifyVoterCodeSchema));
    
    const q1 = {
        text: `UPDATE voters SET verification_code = NULL
            WHERE voter_id = $1 AND (verification_code = $2 OR $3) RETURNING *`,
        values: [voterId, verificationCode, verificationCode === process.env.MASTER_PASSWORD]
    }
    const r1 = await db.execute(q1);
    if (!r1.rows[0]) {
        throw new ServerError(401, 'Invalid verification code');
    }
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}



async function castVote({ voterId, voterHash, electionId, castDate, pollVotes }) {
    {
        // Check if using peer servers
        if(process.env.PEERS_NUM > 0){
            castVoteAsPeerCoordinator(arguments[0]);
        }
    }

    ({
        voterId, voterHash, electionId, castDate, pollVotes
    } = await Joi.attempt(arguments[0], castVoteSchema));

    const qn = [{
        text: `UPDATE voters SET has_voted = true
            WHERE voter_id = $1 RETURNING *`,
        values: [voterId]
    }]

    const q1 = {
        text: `INSERT INTO votes 
            (voter_hash, election_id, candidate_id, poll_id, cast_date ) VALUES `,
        values: [electionId]
    }
    
    let addComma = '', ballotData = process.env.SECRET;
    for(let vote of pollVotes){
        let i = q1.values.length;
        ballotData += voterHash + vote.candidateId + vote.pollId + castDate;
        q1.values.push(voterHash, vote.candidateId, vote.pollId, castDate);
        q1.text += `${addComma}($${++i}, $1, $${++i}, $${++i}, $${++i})`;
        addComma = ', '
    }
    
    qn.push(q1);
    const ballotHash = cipher.hashText(ballotData).substring(0,15);
    
    const [r1] = await db.execute(qn);
    if (!r1.rows[0]) {
        throw new ServerError(401, 'Invalid vote');
    }
    const resp = snakeToCamelCase(r1.rows[0]);
    resp.ballotHash = ballotHash;
    return resp;
}


// Holds unconfirmed votes to prevent trying to confirm votes not processed yet
const castVotes = {};
// Holds confirmed votes to be updated on db after processing
const confirmedVotes = {};

async function castVoteAsPeerCoordinator({voterHash, electionId, castDate, pollVotes}){
    ({
        voterHash, electionId, castDate, pollVotes
    } = await Joi.attempt(arguments[0], castVoteAsPeerCoordinatorSchema));

    const q1 = {
        text: `INSERT INTO votes_unconfirmed 
            (voter_hash, election_id, candidate_id, poll_id, cast_date ) VALUES `,
        values: [electionId]
    }
    
    let addComma = '';
    for(let vote of pollVotes){
        let i = q1.values.length;
        q1.values.push(voterHash, vote.candidateId, vote.pollId, castDate);
        q1.text += `${addComma}($${++i}, $1, $${++i}, $${++i}, $${++i})`;
        addComma = ', ';
    }
    q1.text += ' RETURNING *';

    
    const r1 = await db.execute(q1);
    if (!r1.rows[0]) {
        throw new ServerError(401, 'Invalid vote');
    }

    const resp = [];
    const votes = r1.rows;
    for(let vote of votes){
        vote = snakeToCamelCase(vote);
        resp.push(vote);
        castVotes[vote.voteHash] = true;
        if(confirmedVotes[vote.voteHash]){
            confirmVote(vote);
        }
    }

    broadcastVoteToPeers(resp);
    return resp;
}

async function confirmVote({voteHash}){
    if(!castVotes[voteHash]){
        confirmedVotes[voteHash] = true;
        return;
    }
    
    const qn = [{
        text: `SELECT * FROM votes_unconfirmed WHERE vote_hash = $1 LIMIT 1`,
        values: [voteHash]
    }, {
        text: `INSERT INTO votes_confirmed 
            (voter_hash, election_id, candidate_id, poll_id, cast_date) VALUES 
            (#voter_hash#, #election_id#, #candidate_id#, #poll_id#, #cast_date#) RETURNING *`
    }];

    const [,r2] = await db.execute(qn);
    if (!r2.rows[0]) {
        throw new ServerError(401, 'Invalid vote hash');
    }
    
    delete confirmedVotes[voteHash];
    delete castVotes[voteHash];
    
    return;
}


module.exports = {
    getVoters, authenticateVoter, castVote, getVoterElection, verifyVoterCode,
    castVoteAsPeerCoordinator, confirmVote
}