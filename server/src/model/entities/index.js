module.exports = {
    ...require('./admins'),
    ...require('./officials'),
    ...require('./elections'),
    ...require('./voters'),
}