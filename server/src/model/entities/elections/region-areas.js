const { snakeToCamelCase, } = require('litlib');
const Joi = require('joi');
const {
    newRegionAreaSchema, editRegionAreaSchema, deleteRegionAreaSchema,
    loadRegionAreaFileSchema
} = require('../../schemas/election-schema/region-areas-schema');
const fsPromise = require('fs').promises;
const path = require('path');
const { Db } = require('litlib/ldb');
const { changeCase } = require('../../schemas/schema-ext');
const db = new Db();


async function createNewRegionArea({ name, code, parentRegionAreaId, regionId, }) {
    ({
        name, code, parentRegionAreaId, regionId,
    } = await Joi.attempt(arguments[0], newRegionAreaSchema));
    const q1 = db.createInsertStatement('region_areas', { name, code, parentRegionAreaId, regionId, });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function editRegionArea({ regionAreaId, name, code, parentRegionAreaId, regionId, }) {
    ({
        regionAreaId, name, code, parentRegionAreaId, regionId,
    } = await Joi.attempt(arguments[0], editRegionAreaSchema));
    const q1 = db.createUpdateStatement('region_areas', {
        name, code, parentRegionAreaId, regionId,
    }, { regionAreaId });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function deleteRegionArea({ regionAreaId }) {
    ({ regionAreaId } = await Joi.attempt(arguments[0], deleteRegionAreaSchema));
    const q1 = {
        text: `DELETE FROM region_areas WHERE region_area_id = $1 RETURNING *`,
        values: [regionAreaId]
    };
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}


async function getRegionAreasTemplate({ }) {
    const data = 'Region Area Name, Region Area Code, Region Name, Parent Region Area Name\n';
    return data;
}

async function getRegionAreasFile({ electionId }) {
    let data = 'Region Area Name, Region Area Code, Region Name, Parent Region Area Name';
    const q1 = {
        text: `SELECT 
                ra.name, ra.code, r.name region_name, pra.name parent_region_area_name 
            FROM region_areas ra 
            LEFT JOIN regions r USING (region_id)
            LEFT JOIN region_areas pra ON (ra.parent_region_area_id = pra.region_area_id)
            WHERE election_id = $1`,
        values: [electionId]
    }
    const r1 = await db.execute(q1);
    for (let i = 0, l = r1.rows.length; i < l; i++) {
        const ra = r1.rows[i];
        data += `\n${ra.name},${ra.code},${ra.region_name},${ra.parent_region_area_name || ''}`;
    }
    return data;
}

async function loadRegionAreaFile({ electionId, file }) {
    ({ electionId, file } = await Joi.attempt(arguments[0], loadRegionAreaFileSchema));
    const qn = [{
        text: `DELETE FROM region_areas WHERE region_id = ANY(
                SELECT region_id FROM regions WHERE election_id = $1
            )`,
        values: [electionId]
    }];

    const data = await fsPromise.readFile(file.path, 'utf8');
    const lines = data.split(/[\n\r]/);
    lines.shift(); // first line is headers
    for (let line of lines) {
        if (!line) continue;
        line = line.replace(/(^"|"$)/g, '');
        const [regionAreaName, regionAreaCode, regionName, parentRegionAreaName = ''] = line.split(/"?\s*"?,"?\s*"?/);

        await Joi.assert([regionAreaName, regionAreaCode, regionName, parentRegionAreaName],
            Joi.array().items(Joi.string().allow('')).length(4));
        const toTitleCase = changeCase('title');
        const qi = {
            text: `INSERT INTO region_areas (name, code, parent_region_area_id, region_id) VALUES 
                ($2, $3, 
                (SELECT region_area_id FROM region_areas 
                    WHERE region_id = ANY (
                        SELECT region_id FROM regions WHERE election_id = $1
                    ) AND lower(name) = $4
                ),
                (SELECT region_id FROM regions WHERE election_id = $1 AND lower(name) = $5))
            `,
            values: [
                electionId, toTitleCase(regionAreaName), regionAreaCode,
                parentRegionAreaName.toLowerCase(), regionName.toLowerCase()
            ]
        }
        qn.push(qi);
    }

    const [r1] = await db.execute(qn);
    const delCount = r1.rowCount;
    const num = lines.length;
    return ({ electionId, num, delCount, postCount: num });
}


module.exports = {
    createNewRegionArea, editRegionArea, deleteRegionArea,
    getRegionAreasTemplate, getRegionAreasFile, loadRegionAreaFile,
}