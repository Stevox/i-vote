const { snakeToCamelCase, } = require('litlib');
const Joi = require('joi');
const {
    newVoterSchema, editVoterSchema, deleteVoterSchema, loadVoterFileSchema
} = require('../../schemas/election-schema/voter-schema');
const fsPromise = require('fs').promises;
const { Db } = require('litlib/ldb');
const { changeCase } = require('../../schemas/schema-ext');
const db = new Db();


async function createNewVoter({
    fullName, email, phone, profilePicUrl, regionAreaId, electionId, idFieldValue,
}) {
    ({
        fullName, email, phone, profilePicUrl, regionAreaId, electionId, idFieldValue,
    } = await Joi.attempt(arguments[0], newVoterSchema));
    const q1 = db.createInsertStatement('voters', {
        fullName, email, phone, profilePicUrl, regionAreaId, electionId,
        idFieldValue,
    });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function editVoter({
    voterId, fullName, email, phone, profilePicUrl, regionAreaId, idFieldValue,
}) {
    ({
        voterId, fullName, email, phone, profilePicUrl, regionAreaId, idFieldValue,
    } = await Joi.attempt(arguments[0], editVoterSchema));
    const q1 = db.createUpdateStatement('voters', {
        fullName, email, phone, profilePicUrl, regionAreaId, idFieldValue,
    }, { voterId });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function deleteVoter({ voterId }) {
    ({ voterId } = await Joi.attempt(arguments[0], deleteVoterSchema));
    const q1 = {
        text: `DELETE FROM voters WHERE voter_id = $1 RETURNING *`,
        values: [voterId]
    };
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function getVotersTemplate({ electionId }) {
    const q1 = {
        text: `SELECT name FROM regions WHERE region_id = get_lowest_region_id($1)`,
        values: [electionId]
    }

    const r1 = await db.execute(q1);

    const data = `Voter Name*, Email*, Phone*, ID Number*, ${r1.rows[0].name}*\n`;
    return data;
}


async function getVotersFile({ electionId }) {
    const qn = [{
        text: `SELECT name FROM regions WHERE region_id = get_lowest_region_id($1)`,
        values: [electionId]
    }]
    qn.push({
        text: `SELECT full_name, email, phone, id_field_value, ra.name region_area_name FROM voters
            LEFT JOIN region_areas ra USING (region_area_id) WHERE election_id = $1`,
        values: [electionId]
    }) 

    const [r1, r2] = await db.execute(qn);
    let data = `Voter Name*,Email*,Phone*,ID Number*,${r1.rows[0].name}*`;
    for (let i = 0, l = r2.rows.length; i < l; i++) {
        const v = r2.rows[i];
        data += `\n${v.full_name},${v.email},${v.phone},${v.id_field_value},${v.region_area_name}`;
    }
    
    return data;
}


async function loadVoterFile({ electionId, file }) {
    ({ electionId, file } = await Joi.attempt(arguments[0], loadVoterFileSchema));
    const qn = [{
        text: `DELETE FROM voters WHERE election_id = $1`,
        values: [electionId]
    }];

    const q2 = {
        text: `INSERT INTO voters (
                election_id, full_name, email, phone, id_field_value, region_area_id
            ) VALUES `,
        values: [electionId]
    };
    const data = await fsPromise.readFile(file.path, 'utf8');
    let recordAdded = false;
    const lines = data.split(/[\n\r]/);
    lines.shift(); // first line is headers
    for (let line of lines) {
        if(!line) continue;
        line = line.replace(/(^"|"$)/g, '');
        const [
            fullName, email, phone, idFieldValue, regionAreaName,
        ] = line.split(/"?\s*"?,"?\s*"?/);
        await Joi.assert([fullName, email, phone, idFieldValue, regionAreaName,],
            Joi.array().items(Joi.string()).length(5));
        const toTitleCase = changeCase('title');
        let i = q2.values.length;
        q2.values.push(
            toTitleCase(fullName), Joi.attempt(email, Joi.string().email()),
            phone, idFieldValue, regionAreaName.toLowerCase(),
        );
        if (recordAdded) {
            q2.text += ', '
        }
        q2.text += ` (
            $1, $${++i}, $${++i}, $${++i}, $${++i},
            (SELECT region_area_id FROM region_areas 
                WHERE region_id = get_lowest_region_id($1) AND lower(name) = $${++i})
        )`;
        recordAdded = true;
    }

    if (recordAdded) {
        qn.push(q2);
    }

    const [, r2] = await db.execute(qn);
    const num = r2.rowCount;
    return ({ electionId, num });
}


module.exports = {
    createNewVoter, editVoter, deleteVoter, loadVoterFile, getVotersTemplate,
    getVotersFile,
}