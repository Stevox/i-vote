const { customAlphabet } = require('nanoid/async')
const { snakeToCamelCase, cipher } = require('litlib');
const Joi = require('joi');
const {
    getElectionsRequestSchema, newElectionSchema, editElectionSchema,
    deleteElectionSchema, makeElectionLiveSchema, setElectionOfficialsSchema, getElectionsResponseSchema,
} = require('../../schemas/election-schema');
const { Db } = require('litlib/ldb');
const { getRegisteredPeersNum } = require('@app/utils');
const { ServerError } = require('litlib/utils');
const db = new Db();
const fsPromises = require('fs').promises;
const path = require('path');
const { query } = require('express');


async function getElections({ officialId, adminId, electionId }) {
    await Joi.assert(arguments[0], getElectionsRequestSchema);
    let q1;
    if (adminId) {
        q1 = `SELECT * FROM elections_v WHERE is_active`;
    } else if (officialId) {
        q1 = {
            text: `SELECT * FROM elections_v 
                WHERE is_active AND (official_id = $1 OR election_id = ANY(
                    SELECT election_id FROM election_officials WHERE official_id = $1
                ))`,
            values: [officialId]
        }
    } else if (electionId) {
        q1 = {
            text: `SELECT * FROM elections_v WHERE election_id = $1`,
            values: [electionId]
        }
    } else {
        return;
    }

    const r1 = await db.execute(q1);
    const elections = await Joi.attempt(snakeToCamelCase(r1.rows), getElectionsResponseSchema);

    return electionId ? elections[0] : elections;
}


// TODO offload generation of this file to worker thread
async function getElectionResultsFile({ electionId }) {
    await Joi.assert(arguments[0], Joi.object({ electionId: Joi.number().required() }).unknown());

    const q1 = {
        text: `SELECT * FROM elections_results_v WHERE election_id = $1`,
        values: [electionId]
    }
    const q2 = {
        text: `SELECT * FROM voters WHERE election_id = $1`,
        values: [electionId]
    }
    const [r1, r2,] = await db.execute([q1, q2,]);
    const election = r1.rows[0];
    if (new Date(election.end_date) > new Date()) {
        throw new ServerError(403, 'Results cannot be downloaded before end of the election');
    }

    const fileName = `${election.title}-${election.election_id}.csv`;
    const filePath = path.resolve(__dirname, 'tmp', fileName);
    try {
        return await fsPromises.readFile(filePath);
    } catch (err) {
        console.info('Election results file does not exist. Creating...');
        try {
            await fsPromises.mkdir(path.resolve(__dirname, 'tmp'));
        } catch (err) {
            console.info('Temp folder already exists. Continuing...');
        }
    }

    let fileData = `${election.title}\n\n\n`;
    fileData += `Summary:\n\n`;

    fileData += `Candidate,Deputy,Party,Votes,Region Name,Region Area Name,Position:`;
    let curr_ra, curr_pos;
    for (let c of election.candidates) {
        if (c.region_area_name !== curr_ra || c.position !== curr_pos) {
            curr_ra = c.region_area_name;
            curr_pos = c.position;
            fileData += '\n';
        }
        fileData += `${c.name},${c.deputy_name},${c.party},${c.votes},` +
            `${c.region_name},${c.region_area_name},${c.position}\n`;
    }

    fileData += `\n\n\n`;
    fileData += `Full Results:\n\n`;

    fileData += `Position,Candidate,Time,Voter\n`;
    for (let v of election.votes) {
        fileData += `${v.position},${v.candidate_name},${v.cast_date},${v.voter_hash}\n`;
    }

    fileData += `\n\n\n`;
    fileData += `Ballot codes:\n\n`;
    fileData += `Voter ID Number,Ballot Code\n`;
    const voters = r2.rows;
    for (let v of voters) {
        const voterHash = cipher.hashText(v.voter_id, process.env.SECRET);
        const r1 = await db.execute(`SELECT * FROM votes WHERE voter_hash = '${voterHash}'`);
        // TODO Move generation of the ballot hash to a utility function
        if(r1.rows.length === 0) continue;
        let ballotData = process.env.SECRET;
        for(let vote of r1.rows){
            ballotData += voterHash + vote.candidateId + vote.pollId + vote.castDate;
        }
        const ballotHash = cipher.hashText(ballotData).substring(0,15);
        fileData += `${v.id_field_value},${ballotHash}\n`;
    }

    await fsPromises.writeFile(filePath, fileData);
    return fileData;

}

async function createNewElection({
    logoFileUrl, title, description, startDate, endDate, idFieldName,
    allowNullVotes, lowestRegionPollFirst,
    officialEmail, regions
}) {
    ({
        logoFileUrl, title, description, startDate, endDate, idFieldName,
        allowNullVotes, lowestRegionPollFirst, officialEmail, regions
    } = await Joi.attempt(arguments[0], newElectionSchema));

    const nanoid = customAlphabet('0123456789abcdefghijklmnopqrstuvwxyz', 6);
    const electionCode = (await nanoid()).replace(/(\w{3})$/, (_, m1) => `-${m1}`);
    const q1 = {
        text: `INSERT INTO elections (
                logo_file_url, title, election_code, description, start_date, end_date, 
                id_field_name, allow_null_votes, lowest_region_poll_first, 
                official_id
            ) VALUES (
                $1, $2, $3, $4, $5, $6, $7, $8, $9,
                (SELECT official_id FROM officials WHERE email = $10)
            ) RETURNING *`,
        values: [
            logoFileUrl, title, electionCode, description, new Date(startDate),
            new Date(endDate), idFieldName, allowNullVotes, lowestRegionPollFirst,
            officialEmail,
        ]
    }

    const qn = [q1];
    for (let i = 0, l = regions.length; i < l; i++) {
        let { name } = regions[i];
        if (i === 0) {
            qn.push({
                text: `INSERT INTO regions (name, election_id) 
                    VALUES ($1, #election_id#) RETURNING *`,
                values: [name]
            });
        } else {
            qn.push({
                text: `INSERT INTO regions (name, parent_region_id, election_id) 
                    VALUES ($1, #region_id#, #election_id#) RETURNING *`,
                values: [name]
            });
        }
    }

    const [r1] = await db.execute(qn);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function editElection({
    electionId, logoFileUrl, title, description, startDate, endDate, idFieldName,
    allowNullVotes, lowestRegionPollFirst, regions,
}) {
    ({
        electionId, logoFileUrl, title, description, startDate, endDate, idFieldName,
        allowNullVotes, lowestRegionPollFirst, regions,
    } = await Joi.attempt(arguments[0], editElectionSchema));
    const q1 = db.createUpdateStatement('elections', {
        logoFileUrl, title, description, startDate, endDate, idFieldName, allowNullVotes,
        lowestRegionPollFirst,
    }, { electionId });
    q1.text += ' RETURNING *';

    const qn = [q1];
    if (regions) {
        qn.push({
            text: `UPDATE regions SET is_active = false WHERE election_id = $1`,
            values: [electionId]
        });
        for (let { name, regionId } of regions) {
            if (regionId) {
                qn.push({
                    text: `UPDATE regions SET 
                            name = $1, parent_region_id = #region_id#, is_active = true
                        WHERE region_id = $2 RETURNING *`,
                    values: [name, regionId]
                });
            } else {
                qn.push({
                    text: `INSERT INTO regions (name, parent_region_id, election_id) 
                        VALUES ($1, #region_id#, $2) RETURNING *`,
                    values: [name, electionId]
                });
            }
        }
        qn.push({
            text: `DELETE FROM regions WHERE election_id = $1 AND NOT is_active`,
            values: [electionId]
        });
    }

    const [r1] = await db.execute(qn);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function deleteElection({ electionId }) {
    ({ electionId } = await Joi.attempt(arguments[0], deleteElectionSchema));
    const q1 = {
        text: `DELETE FROM elections WHERE election_id = $1 RETURNING *`,
        values: [electionId]
    }

    const r1 = await db.execute(q1);
    return ({ electionId });
}

async function setElectionOfficials({ electionId, officialEmails }) {
    ({ electionId, officialEmails } = await Joi.attempt(arguments[0], setElectionOfficialsSchema));
    const qn = [{
        text: `DELETE FROM election_officials WHERE election_id = $1`,
        values: [electionId]
    }];

    if (officialEmails.length) {
        const q2 = {
            text: `INSERT INTO election_officials (election_id, official_id) 
                VALUES `,
            values: [electionId]
        };
        const textQuery = [];
        for (let email of officialEmails) {
            q2.values.push(email);
            textQuery.push(`($1, SELECT official_id FROM officials WHERE email = $${q2.values.length})`);
        }
        q2.text += textQuery.join(', ');
        qn.push(q2);
    }

    await db.execute(qn);
    return ({ electionId });
}

async function makeElectionLive({ electionId }) {
    {
        // Check if using peer servers
        const q0 = {
            text: `SELECT peer_servers_num FROM elections WHERE election_id = $1`,
            values: [electionId]
        }
        const r0 = await db.execute(q0);
        const num = getRegisteredPeersNum();
        if (num < r0.rows[0].peer_servers_num) {
            throw new ServerError(422, 'Number of registered peers is less than the defined number.');
        }
    }

    ({ electionId } = await Joi.attempt(arguments[0], makeElectionLiveSchema));
    const q1 = db.createUpdateStatement('elections', { isLive: true }, { electionId });
    q1.text += ` RETURNING *`;
    const qn = [q1];
    qn.push({
        text: `SELECT email, full_name FROM voters WHERE election_id = #election_id#`,
    })

    const [r1, r2] = await db.execute(qn);
    const election = snakeToCamelCase(r1.rows[0]);
    const voters = snakeToCamelCase(r2.rows);
    const resp = { election, voters };
    return resp;
}


module.exports = {
    getElections, createNewElection, editElection, deleteElection,
    setElectionOfficials, makeElectionLive, getElectionResultsFile,
    ...require('./candidates'),
    ...require('./polls'),
    ...require('./region-areas'),
    ...require('./voters'),
}