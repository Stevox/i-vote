const { snakeToCamelCase, } = require('litlib');
const Joi = require('joi');
const {
    newPollSchema, editPollSchema, deletePollSchema, loadPollFileSchema
} = require('../../schemas/election-schema/poll-schema');
const fsPromise = require('fs').promises;
const { Db } = require('litlib/ldb');
const { changeCase } = require('../../schemas/schema-ext');
const db = new Db();


async function createNewPoll({ position, regionId }) {
    ({
        position, regionId
    } = await Joi.attempt(arguments[0], newPollSchema));
    const q1 = db.createInsertStatement('polls', { position, regionId });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function editPoll({ pollId, position, regionId }) {
    ({
        pollId, position, regionId
    } = await Joi.attempt(arguments[0], editPollSchema));
    const q1 = db.createUpdateStatement('polls', { position, regionId }, { pollId });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function deletePoll({ pollId }) {
    ({ pollId } = await Joi.attempt(arguments[0], deletePollSchema));
    const q1 = {
        text: `DELETE FROM polls WHERE poll_id = $1 RETURNING *`,
        values: [pollId]
    };
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function getPollsTemplate({ }) {
    const data = 'Position*, Region Name*\n';
    return data;
}


async function getPollsFile({ electionId }) {
    let data = 'Position*, Region Name*';
    const q1 = {
        text: `SELECT position, name region_name FROM polls 
            LEFT JOIN regions USING (region_id) WHERE election_id = $1`,
        values: [electionId]
    }
    const r1 = await db.execute(q1);
    for (let i = 0, l = r1.rows.length; i < l; i++) {
        const p = r1.rows[i];
        data += `\n${p.position},${p.region_name}`;
    }
    return data;
}


async function loadPollFile({ electionId, file }) {
    ({ electionId, file } = await Joi.attempt(arguments[0], loadPollFileSchema));
    const qn = [{
        text: `DELETE FROM polls WHERE region_id = ANY(
                SELECT region_id FROM regions WHERE election_id = $1
            )`,
        values: [electionId]
    }];

    const q2 = {
        text: `INSERT INTO polls (position, region_id) VALUES `,
        values: [electionId]
    };
    const data = await fsPromise.readFile(file.path, 'utf8');
    let recordAdded = false;
    const lines = data.split(/[\n\r]/);
    lines.shift(); // first line is headers
    for (let line of lines) {
        if(!line) continue;
        line = line.replace(/(^"|"$)/g, '');
        const [position, regionName] = line.split(/"?\s*"?,"?\s*"?/);
        await Joi.assert([position, regionName],
            Joi.array().items(Joi.string()).length(2));
        const toTitleCase = changeCase('title');
        let i = q2.values.length;
        q2.values.push(toTitleCase(position), regionName.toLowerCase());
        if (recordAdded) {
            q2.text += ', '
        }
        q2.text += ` (
            $${++i}, ( SELECT region_id FROM regions 
                WHERE election_id = $1 AND lower(name) = $${++i}
            )
        )`;
        recordAdded = true;
    }

    if (recordAdded) {
        qn.push(q2);
    }

    const [, r2] = await db.execute(qn);
    const num = r2.rowCount;
    return ({ electionId, num });
}


module.exports = {
    createNewPoll, editPoll, deletePoll, loadPollFile, getPollsTemplate, getPollsFile,
}