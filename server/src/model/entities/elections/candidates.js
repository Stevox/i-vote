const { snakeToCamelCase, } = require('litlib');
const Joi = require('joi');
const {
    newCandidateSchema, editCandidateSchema, deleteCandidateSchema, loadCandidateFileSchema
} = require('../../schemas/election-schema/candidate-schema');
const fsPromise = require('fs').promises;
const { Db } = require('litlib/ldb');
const { changeCase } = require('../../schemas/schema-ext');
const db = new Db();



async function createNewCandidate({ name, deputyName, party, profilePicUrl, pollId, regionAreaId, }) {
    ({
        name, deputyName, party, profilePicUrl, pollId, regionAreaId,
    } = await Joi.attempt(arguments[0], newCandidateSchema));
    const q1 = db.createInsertStatement('candidates', {
        name, deputyName, party, profilePicUrl, pollId, regionAreaId,
    });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function editCandidate({
    candidateId, name, deputyName, party, profilePicUrl, pollId, regionAreaId,
}) {
    ({
        candidateId, name, deputyName, party, profilePicUrl, pollId, regionAreaId,
    } = await Joi.attempt(arguments[0], editCandidateSchema));
    const q1 = db.createUpdateStatement('candidates', {
        name, deputyName, party, profilePicUrl, pollId, regionAreaId,
    }, { candidateId });
    q1.text += ` RETURNING *`;
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}

async function deleteCandidate({ candidateId }) {
    ({
        candidateId
    } = await Joi.attempt(arguments[0], deleteCandidateSchema));
    const q1 = {
        text: `DELETE FROM candidates WHERE candidate_id = $1 RETURNING *`,
        values: [candidateId]
    };
    const r1 = await db.execute(q1);
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}


async function getCandidatesTemplate({ }) {
    const data = 'Candidate Name*, Deputy Name, Party, Profile Picture Url, Position*, Region Area Name*';
    return data;
}


async function getCandidatesFile({ electionId }) {
    let data = 'Candidate Name*, Deputy Name, Party, Profile Picture Url, Position*, Region Area Name*';
    const q1 = {
        text: `SELECT 
                c.name, c.deputy_name, c.party, c.profile_pic_url, p.position, ra.name region_area_name 
            FROM candidates c LEFT JOIN polls p USING (poll_id)
                LEFT JOIN region_areas ra USING (region_area_id)
            WHERE p.region_id = ANY (SELECT region_id FROM regions WHERE election_id = $1)`,
        values: [electionId]
    }
    const r1 = await db.execute(q1);
    for (let i = 0, l = r1.rows.length; i < l; i++) {
        const c = r1.rows[i];
        data += `\n${c.name},${c.deputy_name || ''},${c.party || ''},`+
            `${c.profile_pic_url || ''},${c.position},${c.region_area_name}`;
    }
    return data;
}

async function loadCandidateFile({ electionId, file }) {
    ({
        electionId, file
    } = await Joi.attempt(arguments[0], loadCandidateFileSchema));
    const qn = [{
        text: `DELETE FROM candidates WHERE poll_id = ANY(
                SELECT poll_id FROM polls WHERE region_id = ANY(
                    SELECT region_id FROM regions WHERE election_id = $1
                )
            )`,
        values: [electionId]
    }];

    const q2 = {
        text: `INSERT INTO candidates 
            (name, deputy_name, party, profile_pic_url, poll_id, region_area_id) VALUES `,
        values: [electionId]
    };
    const data = await fsPromise.readFile(file.path, 'utf8');
    let recordAdded = false;
    const lines = data.split(/[\n\r]/);
    lines.shift(); // first line is headers
    for (let line of lines) {
        if(!line) continue;
        // TODO download profile pic
        line = line.replace(/(^"|"$)/g, '');
        const [
            name, deputyName = '', party = '', profilePicUrl = '', position, regionAreaName,
        ] = line.split(/"?\s*"?,"?\s*"?/);
        await Joi.assert([name, deputyName, party, profilePicUrl, position, regionAreaName,],
            Joi.array().items(Joi.string().allow('')).length(6));
        const toTitleCase = changeCase('title');
        let i = q2.values.length;
        q2.values.push(
            toTitleCase(name), toTitleCase(deputyName), toTitleCase(party),
            profilePicUrl, position.toLowerCase(), regionAreaName.toLowerCase()
        );
        if (recordAdded) {
            q2.text += ', '
        }
        q2.text += ` (
            $${++i}, $${++i}, $${++i}, $${++i}, (SELECT poll_id FROM polls 
                WHERE region_id = ANY (
                    SELECT region_id FROM regions WHERE election_id = $1
                ) AND lower(position) = $${++i}
            ), (SELECT region_area_id FROM region_areas 
                WHERE region_id = ANY(
                    SELECT region_id FROM regions WHERE election_id = $1
                ) AND lower(name) = $${++i}
            )
        )`;
        recordAdded = true;
    }
    if (recordAdded) {
        qn.push(q2);
    }

    const [, r2] = await db.execute(qn);
    const num = r2.rowCount;
    return ({ electionId, num });
}


module.exports = {
    createNewCandidate, editCandidate, deleteCandidate, loadCandidateFile,
    getCandidatesTemplate, getCandidatesFile,
}