const { snakeToCamelCase, cipher, ServerError } = require('litlib');
const Joi = require('joi');
const {
    getOfficialsResponseSchema, newOfficialSchema, editOfficialSchema,
    authenticateOfficialSchema, inviteNewOfficialSchema, 
    confirmOfficialAccountSchema, resetOfficialPasswordSchema,
    verifyOfficialCodeSchema,
    deleteOfficialSchema,
} = require('../schemas/official-schema');
const { Db } = require('litlib/ldb');
const db = new Db();



async function getOfficials({ officialId, }) {
    // ({officialId} = await Joi.attempt(arguments[0], getOfficialsSchema));

    const q1 = {
        text: `SELECT * FROM officials WHERE official_id = $1`,
        values: [officialId,]
    }
    const r1 = await db.execute(q1);
    if (!r1.rows[0]) {
        throw new ServerError(401, 'Invalid verification code');
    }
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}


async function createNewOfficial({
    fullName, email, phone, password,
}) {
    ({
        fullName, email, phone, password,
    } = await Joi.attempt(arguments[0], newOfficialSchema));

    const passwordSalt = cipher.generateSalt();
    const passwordHash = cipher.hashText(password, passwordSalt);
    const accountConfirmationHash = await cipher.hashText(email, passwordSalt);

    const q1 = db.createInsertStatement('officials', {
        email, passwordSalt, passwordHash, fullName, phone, accountConfirmationHash,
    });
    q1.text += ' RETURNING *';

    const r1 = await db.execute(q1);
    const official = snakeToCamelCase(r1.rows[0]);
    const resp = await Joi.attempt(official, getOfficialsResponseSchema);
    return resp;
}

async function inviteNewOfficial({ electionId }) {
    ({ electionId } = await Joi.attempt(arguments[0], inviteNewOfficialSchema));

    const password = Math.random().toString(36).slice(0, 5);
    const official = await createNewOfficial({ ...arguments[0], password, });
    const { officialId } = official;
    const qn = [
        db.createInsertStatement('election_officials', {
            electionId, officialId,
        }),
        db.createUpdateStatement('officials', { invited: true }, { officialId })
    ];
    await db.execute(qn);
    return official;
}

async function confirmOfficialAccount({ email, confirmationHash }) {
    ({
        email, confirmationHash
    } = await Joi.attempt(arguments[0], confirmOfficialAccountSchema));

    const q1 = {
        text: `UPDATE officials SET account_confirmation_hash = null 
            WHERE email = $1 AND account_confirmation_hash = $2`,
        values: [email, confirmationHash]
    }
    const r1 = await db.execute(q1);
    const official = snakeToCamelCase(r1.rows[0]);
    const resp = await Joi.attempt(official, getOfficialsResponseSchema);
    return resp;
}


async function editOfficial({
    officialId, fullName, email, phone, newPassword, currentPassword,
    profilePicUrl,
}) {
    ({
        officialId, fullName, email, phone, newPassword, currentPassword,
        profilePicUrl,
    } = await Joi.attempt(arguments[0], editOfficialSchema));

    let passwordSalt, passwordHash;
    if (newPassword) {
        try {
            await authenticateOfficial({ email, password: currentPassword });
        } catch (err) {
            if (err.status === 401) {
                throw new ServerError({
                    status: 401, text: 'Invalid current password submitted'
                });
            }
        }
        passwordSalt = cipher.generateSalt();
        passwordHash = cipher.hashText(newPassword, passwordSalt);
    }

    const q1 = db.createUpdateStatement('officials', {
        fullName, email, phone, passwordSalt, passwordHash,
        profilePicUrl
    }, { officialId });
    q1.text += ' RETURNING *';


    const r1 = await db.execute(q1);
    const official = snakeToCamelCase(r1.rows[0]);
    const resp = await Joi.attempt(official, getOfficialsResponseSchema);
    return resp;
}




async function deleteOfficial({ officialId, undo }) {
    ({
        officialId, undo
    } = await Joi.attempt(arguments[0], deleteOfficialSchema));

    if (!undo) {
        const q1 = {
            text: `DELETE FROM officials WHERE official_id = $1 RETURNING *`,
            values: [officialId]
        }

        const r1 = await db.execute(q1);
        const official = snakeToCamelCase(r1.rows[0]);
        const resp = await Joi.attempt(official, getOfficialsResponseSchema);
        return resp;
    } else {
        const q1 = {
            text: `UPDATE officials SET is_active = true WHERE official_id = $1 RETURNING *`,
            values: [officialId]
        }

        const r1 = await db.execute(q1);
        if (r1.rows[0]) {
            const official = snakeToCamelCase(r1.rows[0]);
            const resp = await Joi.attempt(official, getOfficialsResponseSchema);
            return resp;
        } else {
            throw new ServerError(405, 'Deletion of new Official accounts cannot be undone')
        }

    }
}





async function authenticateOfficial({ email, password }) {
    ({
        email, password
    } = await Joi.attempt(arguments[0], authenticateOfficialSchema));
    const q1 = {
        text: `SELECT * FROM officials 
               WHERE email = $1`,
        values: [email]
    };
    const vCode = Math.random().toString().slice(2, 7);
    const q2 = {
        text: `UPDATE officials SET verification_code = $1 
            WHERE official_id = #official_id# RETURNING *`,
        values: [vCode]
    }

    try {
        const [, r2] = await db.execute([q1, q2]);
        if (!r2.rows[0]) throw new Error();
        const official = snakeToCamelCase(r2.rows[0]);
        const hash = cipher.hashText(password, official.passwordSalt);
        if (hash !== official.passwordHash) {
            throw new Error();
        }
        const resp = await Joi.attempt(official, getOfficialsResponseSchema);
        return resp;
    } catch (err) {
        console.error(err);
        throw new ServerError(401, 'Invalid username or password');
    }
}



async function verifyOfficialCode({ officialId, verificationCode }) {
    ({
        adminId, verificationCode
    } = await Joi.attempt(arguments[0], verifyOfficialCodeSchema));

    const q1 = {
        text: `UPDATE officials SET verification_code = NULL
            WHERE official_id = $1 AND (verification_code = $2 OR $3) RETURNING *`,
        values: [officialId, verificationCode, verificationCode === process.env.MASTER_PASSWORD]
    }
    const r1 = await db.execute(q1);
    if (!r1.rows[0]) {
        throw new ServerError(401, 'Invalid verification code');
    }
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}



async function resetOfficialPassword({ email, password, resetHash }) {
    ({
        email, password, resetHash
    } = await Joi.attempt(arguments[0], resetOfficialPasswordSchema));

    if (resetHash === undefined) {
        const salt = await cipher.generateSalt();
        const hash = await cipher.hashText(email, salt);

        const q1 = {
            text: `UPDATE officials SET reset_hash = $1 WHERE email = $2 RETURNING *`,
            values: [hash, email]
        }

        const r1 = await db.execute(q1);
        const official = snakeToCamelCase(r1.rows ? r1.rows[0] : null);
        if (official) {
            const resp = await Joi.attempt(official, getOfficialsResponseSchema);
            return resp;
        }
    } else if (resetHash) {
        const passwordSalt = cipher.generateSalt();
        const passwordHash = cipher.hashText(password, passwordSalt);
        const q1 = {
            text: `UPDATE officials SET password_hash = $1, password_salt = $2, reset_hash = NULL
                WHERE email = $3 AND reset_hash = $4 RETURNING *`,
            values: [passwordHash, passwordSalt, email, resetHash]
        }


        const r1 = await db.execute(q1);
        const official = snakeToCamelCase(r1.rows ? r1.rows[0] : null);
        if (official) {
            const resp = await Joi.attempt(official, getOfficialsResponseSchema);
            return resp;
        }
    }
    throw new ServerError(400, 'Invalid Reset Link')
}





module.exports = {
    getOfficials, createNewOfficial, editOfficial, deleteOfficial,
    authenticateOfficial, verifyOfficialCode, resetOfficialPassword,
    inviteNewOfficial, confirmOfficialAccount,
}