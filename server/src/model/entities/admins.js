const { snakeToCamelCase, cipher, ServerError } = require('litlib');
const Joi = require('joi');
const { getAdminsRequestSchema, getAdminsResponseSchema, newAdminSchema, editAdminSchema, authenticateAdminSchema, deleteAdminSchema, resetAdminPasswordSchema, verifyAdminCodeSchema } = require('../schemas/admin-schema');
const { Db } = require('litlib/ldb');
const db = new Db();

async function getAdmins({ adminId, adminLevel, all }) {
    ({
        adminId, adminLevel, all
    } = await Joi.attempt(arguments[0], getAdminsRequestSchema));

    let q1;
    all = !!all;
    if (all && adminLevel === 0) {
        q1 = {
            text: `SELECT * FROM admins WHERE is_active AND admin_id <> $1`,
            values: [adminId]
        }
    } else if (!all && adminId) {
        q1 = {
            text: `SELECT * FROM admins WHERE admin_id = $1`,
            values: [adminId]
        }
    } else {
        return;
    }

    const r1 = await db.execute(q1);
    const rd= r1.rows[0].reg_date;

    const admins = await Promise.all(snakeToCamelCase(r1.rows).map(async admin => {
        const adminData = await Joi.attempt(admin, getAdminsResponseSchema);
        return adminData;
    }));

    return all ? admins : admins[0];
}



async function createNewAdmin({
    fullName, email, phone, password, adminLevel,
}) {
    ({
        fullName, email, phone, password, adminLevel,
    } = await Joi.attempt(arguments[0], newAdminSchema));

    const passwordSalt = cipher.generateSalt();
    const passwordHash = cipher.hashText(password, passwordSalt);

    const q1 = db.createInsertStatement('admins', {
        email, passwordSalt, passwordHash, fullName, phone,
        adminLevel,
    });
    q1.text += ' RETURNING *';

    const r1 = await db.execute(q1);
    const admin = snakeToCamelCase(r1.rows[0]);
    const resp = await Joi.attempt(admin, getAdminsResponseSchema);
    return resp;
}


async function editAdmin({
    adminId, fullName, email, phone, newPassword, currentPassword,
    profilePicUrl,
}) {
    ({
        adminId, fullName, email, phone, newPassword, currentPassword,
        profilePicUrl,
    } = await Joi.attempt(arguments[0], editAdminSchema));

    let passwordSalt, passwordHash;
    if (newPassword) {
        try {
            await authenticateAdmin({ email, password: currentPassword });
        } catch (err) {
            if (err.status === 401) {
                throw new ServerError({
                    status: 401, text: 'Invalid current password submitted'
                });
            }
        }
        passwordSalt = cipher.generateSalt();
        passwordHash = cipher.hashText(newPassword, passwordSalt);
    }

    const q1 = db.createUpdateStatement('admins', {
        fullName, email, phone, passwordSalt, passwordHash,
        profilePicUrl
    }, { adminId });
    q1.text += ' RETURNING *';


    const r1 = await db.execute(q1);
    const admin = snakeToCamelCase(r1.rows[0]);
    const resp = await Joi.attempt(admin, getAdminsResponseSchema);
    return resp;
}




async function deleteAdmin({ adminId, undo }) {
    ({
        adminId, undo
    } = await Joi.attempt(arguments[0], deleteAdminSchema));

    if (!undo) {
        const q1 = {
            text: `DELETE FROM admins WHERE admin_id = $1 RETURNING *`,
            values: [adminId]
        }

        const r1 = await db.execute(q1);
        const admin = snakeToCamelCase(r1.rows[0]);
        const resp = await Joi.attempt(admin, getAdminsResponseSchema);
        return resp;
    } else {
        const q1 = {
            text: `UPDATE admins SET is_active = true WHERE admin_id = $1 RETURNING *`,
            values: [adminId]
        }

        const r1 = await db.execute(q1);
        if (r1.rows[0]) {
            const admin = snakeToCamelCase(r1.rows[0]);
            const resp = await Joi.attempt(admin, getAdminsResponseSchema);
            return resp;
        } else {
            throw new ServerError(405, 'Deletion of new Admin accounts cannot be undone')
        }

    }
}





async function authenticateAdmin({ email, password }) {
    ({
        email, password
    } = await Joi.attempt(arguments[0], authenticateAdminSchema));
    const username = email.match(/^(.+?)@ivote.com/)[1];
    const q1 = {
        text: `SELECT * FROM admins 
               WHERE email LIKE $1`,
        values: [`${username}@%`]
    };
    const vCode = Math.random().toString().slice(2, 7);
    const q2 = {
        text: `UPDATE admins SET verification_code = $1 
            WHERE admin_id = #admin_id# RETURNING *`,
        values: [vCode]
    }

    const [, r2] = await db.execute([q1, q2]);

    for (let admin of (r2.rows || [])) {
        admin = snakeToCamelCase(admin);
        const hash = cipher.hashText(password, admin.passwordSalt);
        if (hash === admin.passwordHash) {
            const resp = await Joi.attempt(admin, getAdminsResponseSchema);
            return resp;
        }
    }
    throw new ServerError(401, 'Invalid username or password');;
}



async function verifyAdminCode({ adminId, verificationCode }) {
    ({
        adminId, verificationCode
    } = await Joi.attempt(arguments[0], verifyAdminCodeSchema));

    const q1 = {
        text: `UPDATE admins SET verification_code = NULL
            WHERE admin_id = $1 AND (verification_code = $2 OR $3) RETURNING *`,
        values: [adminId, verificationCode, verificationCode === process.env.MASTER_PASSWORD]
    }
    const r1 = await db.execute(q1);
    if (!r1.rows[0]) {
        throw new ServerError(401, 'Invalid verification code');
    }
    const resp = snakeToCamelCase(r1.rows[0]);
    return resp;
}


async function resetAdminPassword({ email, password, resetHash }) {
    ({
        email, password, resetHash
    } = await Joi.attempt(arguments[0], resetAdminPasswordSchema));

    if (resetHash === undefined) {
        const salt = await cipher.generateSalt();
        const hash = await cipher.hashText(email, salt);

        const q1 = {
            text: `UPDATE admins SET reset_hash = $1 WHERE email = $2 RETURNING *`,
            values: [hash, email]
        }

        const r1 = await db.execute(q1);
        const admin = snakeToCamelCase(r1.rows ? r1.rows[0] : null);
        if (admin) {
            const resp = await Joi.attempt(admin, getAdminsResponseSchema);
            return resp;
        }
    } else if (resetHash) {
        const passwordSalt = cipher.generateSalt();
        const passwordHash = cipher.hashText(password, passwordSalt);
        const q1 = {
            text: `UPDATE admins SET password_hash = $1, password_salt = $2, reset_hash = NULL
                WHERE email = $3 AND reset_hash = $4 RETURNING *`,
            values: [passwordHash, passwordSalt, email, resetHash]
        }


        const r1 = await db.execute(q1);
        const admin = snakeToCamelCase(r1.rows ? r1.rows[0] : null);
        if (admin) {
            const resp = await Joi.attempt(admin, getAdminsResponseSchema);
            return resp;
        }
    }
    throw new ServerError(400, 'Invalid Reset Link')
}





module.exports = {
    getAdmins, createNewAdmin, editAdmin, deleteAdmin, authenticateAdmin,
    verifyAdminCode, resetAdminPassword,
}