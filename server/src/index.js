if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}
process.env.MASTER_PASSWORD = process.env.MASTER_PASSWORD || Math.random().toString().slice(2, 9);
process.env.SECRET = process.env.SECRET || Math.random().toString().slice(2, 9);
// TODO log these env values?
const path = require('path');
const express = require('express');
const compression = require('compression');
const cookieParser = require('cookie-parser')
const formidable = require('formidable');
const helmet = require('helmet');

const { ServerError, manageLogs } = require('litlib');
const { 
    appLog, startLogging, stopLogging, connectSocket, cookieManager,
} = require('@app/utils');

manageLogs();
global.ServerError = ServerError;

const mountRoutes = require('./routes');
const app = express();
/** @type {import('http').Server} */
const server = require('http').Server(app);

connectSocket(server);

server.on('listening', async () => {
    try {
        await startLogging();
    } catch (err) {
        console.error('Could not start logging.');
        console.error(err);
    }
    console.sysLog = appLog;
});
server.on('close', async () => {
    await stopLogging();
});


/**
 * Force redirect to https in production
 */
if (process.env.NODE_ENV === 'production') {
    app.use(function (req, res, next) {
        if (req.headers['x-forwarded-proto'] !== 'https') {
            return res.redirect(['https://', req.get('Host'), req.url].join(''));
        }
        return next();
    });
}



app.use(cookieParser(process.env.MASTER_PASSWORD));
app.use(helmet());
app.use(compression());
app.use((req, res, next) => {
    let form = new formidable.IncomingForm({
        encoding: 'utf-8',
        multiples: true,
        keepExtension: true
    });
    form.once('error', err => {
        console.error('Formidable error: ', err);
        res.sendStatus(500);
    });

    const parsedJSONFields = {};
    const deletedKeys = [];
    form.on('field', (key, val) => {
        if (/{}$/.test(key)) {
            parsedJSONFields[key.replace(/{}$/, '')] = JSON.parse(val);
            deletedKeys.push(key);
        }
    });

    form.parse(req, (err, fields, files) => {
        for (key of deletedKeys) {
            delete fields[key];
        }
        fields = { ...fields, ...parsedJSONFields };

        Object.assign(req, { fields, files });
        next();
    });
});


/**
 * Static files
 */
const BUILD_PATH = path.resolve('../react-ui/build');
app.use(express.static(BUILD_PATH));


/**
 * Cookie Authentication
 */
app.use(async (req, _res, next) => {
    const { user, verified } = await cookieManager.getCookieData(req);
    if (user && (user.verified || verified)) {
        req.auth = user;
    }

    next();
});



/**
 * App Routes
 */
mountRoutes(app);


/**
 * Default Handler
 */
app.get('*', (_req, res) => {
    res.sendFile(`${BUILD_PATH}/index.html`, err => {
        if (err) {
            console.error("Error loading index html file: ", err);
            res.sendStatus(500);
        }
    });
});

/**
 * Error Handler
 */
app.use((err, req, res, next) => {
    if (/^AE/.test(err.code)) {
        let msg = err.message;
        if(err.detail){
            msg += `; DETAIL: ${err.detail}`;
        }
        err = new ServerError(422, msg, err);
    }
    else if (err.name === 'ValidationError') {
        const text = err.details.map(e => e.message).join(', ');
        err = new ServerError(422, text, err);
    }
    else if (err.code === '23505') {
        const text = `Duplicate entry. ${err.detail}`;
        err = new ServerError(422, text, err);
    }
    else if (err.code === '23502') {
        const text = `${err.message}`;
        err = new ServerError(422, text, err);
    }

    if (res.headersSent) {
        return next(err);
    }

    console.error(`Error in [${req.method}] ${req.path}`);
    if (err.name !== 'ServerError') err = new ServerError(err);

    if(err.status === 401 || err.status === 501){
        console.error(err.message);
    } else {
        console.error({ ...err });
    }
    console.error();

    err.status = err.status || 500;
    if (err.text) {
        res.status(err.status).send(err.text);
    } else {
        res.sendStatus(err.status);
    }
});


module.exports = server;