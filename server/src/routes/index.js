/**
 * @param {import('express').Express} app
 */
module.exports = app => {
    app.use((req, res, next) => {
        if (req.method === 'GET' && (!req.xhr && /html/.test(req.headers.accept))) {
            // Prevent clashes with front-end routing 
            const paths = 'login|officials|admins|elections|users';
            const regex = new RegExp(`^\\/(?:${paths})`);
            if (regex.test(req.path)){
                return res.sendStatus(204);
            }
        }
        next();
    });
    require('./login')(app);
    require('./officials')(app);
    require('./admins')(app);
    require('./elections')(app);
    require('./voters')(app);
    require('./users')(app);
    require('./sign-up')(app);
}