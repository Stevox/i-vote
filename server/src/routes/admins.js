const express = require('express');
const { wrapAsync, } = require('litlib');
const { editAdmin, createNewAdmin, deleteAdmin } = require('../model/entities/admins');
const { appLog } = require('@app/utils');

const router = express.Router();

module.exports = app => {
    app.use('/admins', router);
}

router.use((req, _, next) => {
    if (!req.auth || !req.auth.adminId) {
        throw new ServerError(401);
    }
    next();
});

router.put('/', wrapAsync(async (req, res) => {
    const { adminId } = req.auth;
    const resp = await editAdmin({ ...req.fields, adminId });
    res.send(resp);
}));

router.use((req, _, next) => {
    if (req.auth.adminLevel > 1) {
        throw new ServerError(401);
    }
    next();
});

router.post('/', wrapAsync(async (req, res) => {
    const resp = await createNewAdmin({ ...req.fields });
    res.send(resp);

    appLog('Admin', 'New admin created', req.auth, {...resp});
}));

router.delete('/', wrapAsync(async (req, res) => {
    const resp = await deleteAdmin({ ...req.fields });
    res.send(resp);

    appLog('Admin', 'Admin deleted', req.auth, {...resp});
}));