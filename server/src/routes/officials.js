const express = require('express');
const { wrapAsync, } = require('litlib');
const { 
    editOfficial, createNewOfficial, deleteOfficial 
} = require('../model/entities/officials');
const { Email } = require('litlib/mailer');

const router = express.Router();

module.exports = app => {
    app.use('/officials', router);
}

router.use((req, _, next) => {
    if (!req.auth) {
        throw new ServerError(401);
    }
    next();
});

router.post('/contact-admin', wrapAsync(async (req, res) => {
    if (!req.auth || !req.auth.officialId) {
        throw new ServerError(401);
    }
    const {subject, message} = req.fields;
    const mailer = new Email();
    await mailer.send({
        to: process.env.EMAIL_ADDRESS,
        subject: `Message from election official: ${req.auth.fullName}`,
        html: `
            <div>
                <b>From: ${req.auth.fullName} "${req.auth.email}" - ${req.auth.phone}</b> <br/>
                <b>SUBJECT: ${subject}</b> <br/><br/>

                ${message}
            </div>
        `,
    });

    res.sendStatus(204);
}))

router.post('/', wrapAsync(async (req, res) => {
    if (!req.auth || !req.auth.adminId) {
        throw new ServerError(401);
    }
    const resp = await createNewOfficial({ ...req.fields });
    res.send(resp);
}));

router.put('/', wrapAsync(async (req, res) => {
    if (!req.auth || !req.auth.officialId) {
        throw new ServerError(401);
    }
    const {officialId} = req.auth;
    const resp = await editOfficial({ ...req.fields, officialId });
    res.send(resp);
}));


router.delete('/', wrapAsync(async (req, res) => {
    if (!req.auth || !req.auth.adminId) {
        throw new ServerError(401);
    }
    const resp = await deleteOfficial({ ...req.fields });
    res.send(resp);
}));
