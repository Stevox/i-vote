const express = require('express');
const { createNewOfficial, confirmOfficialAccount } = require('../model/entities/officials');
const { wrapAsync, cipher, ServerError } = require('litlib');
const { Email } = require('litlib/mailer');
const { appLog } = require('@app/utils');

const router = express.Router();

module.exports = app => {
    app.use('/sign-up', router);
};


router.get('/confirmation', wrapAsync(async (req, res) => {
    const resp = await confirmOfficialAccount({ ...req.query });
    res.send('Account successfully confirmed');
    appLog('Signup', `New signup by ${resp.email}`, { resp });

}));


router.post('/', wrapAsync(async (req, res) => {
    const resp = await createNewOfficial({ ...req.fields });
    res.send(resp);
    appLog('Signup', `New signup by ${resp.email}`, { resp });
    // TODO
    const link = `${req.protocol}://${req.get('host')}/confirmation?user=${resp.email}&confirmHash=${resp.accountConfirmationHash}`;
    const mailer = new Email();
    await mailer.send({
        to: `${resp.email} "${resp.fullName}"`,
        subject: 'I-Vote Email verification',
        html: `
            <div>
                <h2>Welcome!</h2><br/><br/>
                <p>
                    Hi there, <br/>
                    We're excited to have you get started. First, you need to 
                    confirm your email. Just press the button below. <br/><br/>
                    <a href="${link}">
                        <button>Confirm Account</button>
                    </a><br/><br/><br/>

                    If that doesn't work, copy and paste the following link in 
                    your browser: <a href="${link}">${link}</a><br/><br/><br/>

                    If you have any questions, just reply to this email -- 
                    we're always happy to help out.<br/><br/>

                    Cheers,<br/>
                    The I-Vote  Team.
                </p>
            <div>
        `
    });
}));
