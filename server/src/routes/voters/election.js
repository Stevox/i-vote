const express = require('express');
const { wrapAsync, } = require('litlib');
const { appLog } = require('@app/utils');
const { getVoterElection, castVote, getElectionResultsFile } = require('../../model/entities');
const { Email } = require('litlib/mailer');

const router = express.Router();

module.exports = app => {
    app.use('/election', router);
}

router.use((req, _, next) => {
    if (!req.auth || !req.auth.voterId) {
        throw new ServerError(401);
    }
    next();
});

router.get('/', wrapAsync(async (req, res) => {
    const { voterId } = req.auth;
    const resp = await getVoterElection({ ...req.fields, voterId });
    res.send(resp);
}));

router.get('/results', wrapAsync(async (req, res) => {
    const resp = await getElectionResultsFile({ ...req.query });
    res.attachment('election-results.csv');
    res.send(resp);
}));

router.post('/vote', wrapAsync(async (req, res) => {
    const { voterId } = req.auth;
    const resp = await castVote({ ...req.fields, voterId });
    const mailer = new Email();
    mailer.send({
        to: `${resp.email}`,
        subject: 'Vote Cast',
        html: `
            <div>
                <h3>Vote Successfully Recorded</h3>
                <p>
                    Your vote, cast on ${new Date().toUTCString()} (Greenwich-Meridian Time),
                    has been successfully recorded. Your ballot code is: <br/>
                    <center><b>${resp.ballotHash}</b></center>
                    <br/><br/>

                    This email is a reciept of your vote. You can use your 
                    ballot code to confirm that your vote was correctly recorded.<br/>
                    Thank you for using I-Vote. <br/><br/>

                    Cheers,<br/>
                    The I-Vote Team.
                </p>
            </div>
        `,
    })
    res.sendStatus(204);
}));