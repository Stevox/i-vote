const express = require('express');

const router = express.Router();

module.exports = app => {
    app.use('/voters', router);
}

require('./election')(router);