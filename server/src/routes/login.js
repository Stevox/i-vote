const express = require('express');
const {
    authenticateOfficial, resetOfficialPassword, createNewOfficial, verifyOfficialCode, getOfficials,
} = require('../model/entities/officials');
const { authenticateAdmin, resetAdminPassword, verifyAdminCode, getAdmins } = require('../model/entities/admins');
const { wrapAsync, cipher, ServerError } = require('litlib');
const { email } = require('litlib/mailer');
const { appLog, cookieManager, sms } = require('@app/utils');
const { authenticateVoter, verifyVoterCode, getVoters } = require('../model/entities');

const router = express.Router();

module.exports = app => {
    app.use('/login', router);
};


router.post('/forgot', wrapAsync(async (req, res) => {
    const resp = await isAdmin(req.fields) ?
        resetAdminPassword({ ...req.fields }) : resetOfficialPassword({ ...req.fields });
    if (resp) {
        const link = `${req.protocol}://${req.get('host')}/reset?user=${resp.email}&resetHash=${resp.resetHash}`;
        const mailer = new Email();
        await mailer.send({
            to: resp.email,
            subject: 'Account Password Reset',
            message: `Use the link below to reset your account. 
                The link will expire in 15 minutes.<br/><br/>
                <a href=${link}>Reset account password</a>
                <br/><br/><br/><br/>
                <i>If you did not request to reset your account, please ignore this email.</i>
            `
        });
    }
    res.sendStatus(204);
}));


router.post('/reset', wrapAsync(async (req, res) => {
    const resp = await isAdmin(req.fields) ?
        resetAdminPassword({ ...req.fields }) : resetOfficialPassword({ ...req.fields });
    res.sendStatus(204);
    appLog('Password Reset', 'User reset their account password', {
        resp
    });
}));

router.post('/verify-code', wrapAsync(async (req, res) => {
    let { user, remember } = await cookieManager.getCookieData(req);
    const { adminId, officialId, voterId } = user;
    if (adminId) {
        user = await verifyAdminCode({ ...req.fields, adminId });
    } else if (officialId) {
        user = await verifyOfficialCode({ ...req.fields, officialId });
    } else if (voterId) {
        user = await verifyVoterCode({ ...req.fields, voterId });
    } else {
        throw new ServerError(401);
    }
    user.verified = true;
    await cookieManager.setCookies(res, { user, remember });
    res.sendStatus(204);
}));

router.get('/resend-code', wrapAsync(async (req, res) => {
    if (!req.auth) throw new ServerError(401);
    const { adminId, officialId, voterId } = req.auth;
    let user;
    if (adminId) {
        user = await getAdmins({ adminId });
    } else if (officialId) {
        user = await getOfficials({ officialId });
    } else if (voterId) {
        user = await getVoters({ voterId });
    } else {
        throw new ServerError(401);
    }
    console.debug(`Verification code: ${user.verificationCode}`);
    res.sendStatus(204);
}));



router.post('/', wrapAsync(async (req, res) => {
    let { email, idFieldValue, remember } = req.fields;
    const isAdmin = getIsAdmin(req.fields);
    let user;
    if (idFieldValue) {
        user = await authenticateVoter(req.fields);
    } else if (email) {
        if (isAdmin) {
            user = await authenticateAdmin(req.fields);
        } else {
            user = await authenticateOfficial(req.fields);
        }
    }

    sms.send({
        message: `Your I-VOTE verification code is: ${user.verificationCode}`,
        to: user.phone,
    });
    console.debug(`Verification code: ${user.verificationCode}`);
    await cookieManager.setCookies(res, { user, remember });
    res.sendStatus(204);
    appLog('Login', `New login by ${isAdmin ? 'Admin' : 'Official'}`, req.fields);
}));


router.delete('/', (req, res) => {
    for (let cookie in req.cookies) {
        res.clearCookie(cookie);
    }
    for (let cookie in req.signedCookies) {
        res.clearCookie(cookie);
    }
    res.sendStatus(204);
});



function getIsAdmin({ email }) {
    return /@ivote.com$/.test(email);
}

// async function setCookies(res, { user, remember }) {
//     const oneHour = 1000 * 60 * 60 * 1;
//     let maxAge = oneHour;
//     if (user.adminId) {
//         maxAge = oneHour * 24; // one day
//     } else if (user.voterId) {
//         maxAge = oneHour / 3; // 20 min
//     }
//     const key = await cipher.generateKey(process.env.MASTER_PASSWORD);
//     const data = JSON.stringify(user);
//     const encryptedSub = await cipher.encrypt(data, key);
//     res.cookie(
//         'sub',
//         encryptedSub,
//         { maxAge, httpOnly: true, signed: true }
//     );
//     res.cookie('exp', Date.now() + maxAge, { maxAge, httpOnly: true, signed: true });
//     res.cookie('isLoggedIn', true, { maxAge });
//     res.cookie('remember', remember, { maxAge, httpOnly: true });
// }
