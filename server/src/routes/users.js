const express = require('express');
const { wrapAsync, } = require('litlib');
const { getAdmins } = require('../model/entities/admins');
const { getOfficials, getVoters } = require('../model/entities');
const { ServerError } = require('litlib/utils');

const router = express.Router();

module.exports = app => {
    app.use('/users', router);
}

router.use((req, _, next) => {
    if (!req.auth) {
        throw new ServerError(401);
    }
    next();
});

router.get('/', wrapAsync(async (req, res) => {
    const { adminId, officialId, voterId } = req.auth;
    let resp;
    if(adminId){
        resp = await getAdmins({adminId});
    } else if (officialId){
        resp = await getOfficials({officialId});
    } else if (voterId){
        resp = await getVoters({voterId});
    } else {
        throw new ServerError(403);
    }
    res.send(resp);
}));