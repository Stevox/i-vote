const express = require('express');
const { wrapAsync, } = require('litlib');
const { appLog } = require('@app/utils');
const { 
    loadPollFile, createNewPoll, editPoll, deletePoll, getPollsTemplate,
    getPollsFile,
} = require('../../model/entities/elections/polls');

const router = express.Router();

module.exports = app => {
    app.use('/polls', router);
}

router.use((req, _, next) => {
    if (!req.auth || !req.auth.adminId) {
        throw new ServerError(401);
    }
    next();
});

router.get('/template', wrapAsync(async (req, res) => {
    const data = await getPollsTemplate({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('polls-template.csv');
    res.send(data);
}));

router.get('/', wrapAsync(async (req, res) => {
    const data = await getPollsFile({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('polls.csv');
    res.send(data);
}));


router.post('/', wrapAsync(async (req, res) => {
    const { file } = req.files;
    let resp;
    if(file){
        resp = await loadPollFile({...req.fields, file});
        appLog('Poll', 'Poll file loaded', req.auth, { ...resp });
    } else {
        resp = await createNewPoll({ ...req.fields });
        appLog('Poll', 'New poll created', req.auth, { ...resp });
    }
    res.send(resp);

}));

router.put('/', wrapAsync(async (req, res) => {
    const resp = await editPoll({...req.fields});
    res.send(resp);
}));

router.delete('/', wrapAsync(async (req, res) => {
    const resp = await deletePoll({...req.fields});
    res.send(resp);
    appLog('Poll', 'Poll deleted', req.auth, { ...resp });
}));