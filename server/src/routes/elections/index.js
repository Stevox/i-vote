const express = require('express');
const { wrapAsync, Email } = require('litlib');
const { getElections, createNewElection, editElection, deleteElection, makeElectionLive, getElectionResultsFile } = require('../../model/entities/elections');
const { appLog } = require('@app/utils');
const { snakeToCamelCase } = require('litlib/utils');

const router = express.Router();

module.exports = app => {
    app.use('/elections', router);
}

require('./candidates')(router);
require('./polls')(router);
require('./region-areas')(router);
require('./voters')(router);

router.use((req, _, next) => {
    if (!req.auth) {
        throw new ServerError(401);
    }
    next();
});


router.get('/', wrapAsync(async (req, res) => {
    const { officialId, adminId } = req.auth;
    const resp = await getElections({ ...req.fields, officialId, adminId });
    res.send(resp);
}));

router.get('/results', wrapAsync(async (req, res) => {
    const resp = await getElectionResultsFile({ ...req.query });
    res.attachment('election-results.csv');
    res.send(resp);
}));


router.use((req, _, next) => {
    if (!req.auth.adminId) {
        throw new ServerError(401);
    }
    next();
});

router.post('/', wrapAsync(async (req, res) => {
    const resp = await createNewElection({ ...req.fields, });
    res.send(resp);

    const { electionId, title } = resp;
    appLog('Election', "New election created", req.auth, { electionId, title });
}));


router.post('/go-live', wrapAsync(async (req, res) => {
    const resp = await makeElectionLive({ ...req.fields, });
    const {election, voters} = resp
    res.send(election);
    appLog('Election', 'Gone live', req.auth, {election: election.title});
    
    const { title, startDate, electionCode } = election;
    for (let voter of (voters || [])) {
        const { email, fullName } = snakeToCamelCase(voter);
        const url = `${req.protocol}://${req.get('host')}/voter/login`;

        const mailer = new Email();
        mailer.send({
            to: email,
            subject: 'Election Announcement',
            html: `
                <p>
                    Hey ${fullName.match(/(\w+)/)[0]},<br/><br/>

                    You have been registered to the election ${title} 
                    which begins on ${new Date(startDate).toDateString()}.<br/>
                    Click on the button below to go to the Election Page and 
                    login using your ID Number and the election code: ${electionCode}
                    <br/><br>

                    <a href="${url}" >
                        <button>
                            LOGIN
                        </button>
                    </a><br/><br/><br/>
                
                    
                    If that doesn't work, copy and paste the following link in 
                    your browser: <a href="${url}">${url}</a><br/><br/>

                    If you have any questions, just reply to this email -- 
                    we're always happy to help out.<br/><br/>

                    Cheers,<br/>
                    The I-Vote  Team.
                </p><br/><br/><br/>
            `,
        });
    }
}));


router.put('/', wrapAsync(async (req, res) => {
    const resp = await editElection({ ...req.fields, });
    res.send(resp);
}));

router.delete('/', wrapAsync(async (req, res) => {
    const resp = await deleteElection({ ...req.fields, });
    res.send(resp);

    const { electionId, title } = resp;
    appLog('Election', "Election deleted", req.auth, { electionId, title });
}));