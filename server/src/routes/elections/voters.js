const express = require('express');
const { wrapAsync, } = require('litlib');
const { appLog } = require('@app/utils');
const { 
    loadVoterFile, createNewVoter, editVoter, deleteVoter, getVotersTemplate,
    getVotersFile,
} = require('../../model/entities/elections/voters');

const router = express.Router();

module.exports = app => {
    app.use('/voters', router);
}

router.use((req, _, next) => {
    if (!req.auth || !req.auth.adminId) {
        throw new ServerError(401);
    }
    next();
});


router.get('/template', wrapAsync(async (req, res) => {
    const data = await getVotersTemplate({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('voters-template.csv');
    res.send(data);
}));

router.get('/', wrapAsync(async (req, res) => {
    const data = await getVotersFile({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('voters.csv');
    res.send(data);
}));

router.post('/', wrapAsync(async (req, res) => {
    const { file } = req.files;
    let resp;
    if(file){
        resp = await loadVoterFile({...req.fields, file});
        appLog('Voter', 'Voter file loaded', req.auth, { ...resp });
    } else {
        resp = await createNewVoter({ ...req.fields });
        appLog('Voter', 'New voter created', req.auth, { ...resp });
    }
    res.send(resp);
}));

router.put('/', wrapAsync(async (req, res) => {
    const resp = await editVoter({...req.fields});
    res.send(resp);
}));

router.delete('/', wrapAsync(async (req, res) => {
    const resp = await deleteVoter({...req.fields});
    res.send(resp);
    appLog('Voter', 'Voter deleted', req.auth, { ...resp });
}));