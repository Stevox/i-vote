const express = require('express');
const { wrapAsync, } = require('litlib/utils');
const { appLog } = require('@app/utils');
const { 
    loadRegionAreaFile, createNewRegionArea, editRegionArea, deleteRegionArea,
    getRegionAreasTemplate, getRegionAreasFile,
} = require('../../model/entities/elections/region-areas');

const router = express.Router();

module.exports = app => {
    app.use('/region-areas', router);
}

router.use((req, _, next) => {
    if (!req.auth || !req.auth.adminId) {
        throw new ServerError(401);
    }
    next();
});


router.get('/template', wrapAsync(async (req, res) => {
    const data = await getRegionAreasTemplate({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('region-areas-template.csv');
    res.send(data);
}));

router.get('/', wrapAsync(async (req, res) => {
    const data = await getRegionAreasFile({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('region-areas.csv');
    res.send(data);
}));


router.post('/', wrapAsync(async (req, res) => {
    const { file } = req.files;
    let resp;
    if(file){
        resp = await loadRegionAreaFile({...req.fields, file});
        appLog('Region Area', 'Region area file loaded', req.auth, { ...resp });
    } else {
        resp = await createNewRegionArea({ ...req.fields });
        appLog('Region Area', 'New region area created', req.auth, { ...resp });
    }
    res.send(resp);
}));

router.put('/', wrapAsync(async (req, res) => {
    const resp = await editRegionArea({...req.fields});
    res.send(resp);
}));

router.delete('/', wrapAsync(async (req, res) => {
    const resp = await deleteRegionArea({...req.fields});
    res.send(resp);
    appLog('Region Area', 'Region area deleted', req.auth, { ...resp });
}));