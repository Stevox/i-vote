const express = require('express');
const { wrapAsync, } = require('litlib');
const { appLog } = require('@app/utils');
const { 
    loadCandidateFile, createNewCandidate, editCandidate, deleteCandidate,
    getCandidatesTemplate, getCandidatesFile,
} = require('../../model/entities/elections/candidates');

const router = express.Router();

module.exports = app => {
    app.use('/candidates', router);
}

router.use((req, _, next) => {
    if (!req.auth || !req.auth.adminId) {
        throw new ServerError(401);
    }
    next();
});


router.get('/template', wrapAsync(async (req, res) => {
    const data = await getCandidatesTemplate({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('candidates-template.csv');
    res.send(data);
}));

router.get('/', wrapAsync(async (req, res) => {
    const data = await getCandidatesFile({...req.query});
    res.set("Content-Type", "text/csv");
    res.attachment('candidates.csv');
    res.send(data);
}));

router.post('/', wrapAsync(async (req, res) => {
    const { file } = req.files;
    let resp;
    if(file){
        resp = await loadCandidateFile({...req.fields, file});
        appLog('Candidate', 'Candidate file loaded', req.auth, { ...resp });
    } else {
        resp = await createNewCandidate({ ...req.fields });
        appLog('Candidate', 'New candidate created', req.auth, { ...resp });
    }
    res.send(resp);

}));

router.put('/', wrapAsync(async (req, res) => {
    const resp = await editCandidate({...req.fields});
    res.send(resp);
}));

router.delete('/', wrapAsync(async (req, res) => {
    const resp = await deleteCandidate({...req.fields});
    res.send(resp);
    appLog('Candidate', 'Candidate deleted', req.auth, { ...resp });
}));