const path = require('path');
const fs = require('fs');
const EOL = require('os').EOL;
const fsPromises = fs.promises;
const { S3 } = require('litlib/lfs');
const s3 = new S3();



/**@type {string} - Path to log file */
let filePath;
/**@type {fs.WriteStream} - WriteStream to log file */
let inStream;
const dirPath = path.resolve(__dirname, 'app-logs');

async function startLogging() {
    try {
        await fsPromises.mkdir(dirPath);
    } catch (err) {
        if (err.code !== 'EEXIST') {
            console.warn('Logs folder not created.', err);
        }
    }
    if (process.env.NODE_ENV === 'production') {
        generateFilePath();
        try {
            const data = await s3.getFile(filePath);
            if (data) {
                fsPromises.writeFile(filePath, data);
            }
        } catch (err) {
            console.warn(new Error('No log file found in s3'));
        }
    }
}

async function generateFilePath() {
    if (filePath) return;
    const dateStr = new Date().toISOString();
    let filename = `${dateStr.slice(0, dateStr.indexOf('T'))}.log`;
    filePath = path.resolve(dirPath, filename);
}

async function createInstream(data) {
    if (inStream) return;
    generateFilePath();
    let filename = filePath.slice(filePath.lastIndexOf('/') + 1);
    try {
        const files = await fsPromises.readdir(dirPath);
        filename = files.find(file => file === filename) || filename;
    } catch (err) {
        throw new Error(err);
    }
    inStream = fs.createWriteStream(filePath, { flags: 'a' });
}

async function stopLogging() {
    inStream.end();
    if (process.env.NODE_ENV === 'production') {
        await s3.saveFile(filePath);
    }
}

/**
 * @param {string} title 
 * @param {string} [msg] 
 * @param {{
    *  name?: string, firstName?: string, lastName?: string, fullName?:string,
    *  email: string,
    * }} [user]
    * @param {object} [args] 
    */
const appLog = async (title, message, user, args) => {
    if (!title) {
        throw new Error('Log title required');
    }

    let logData = `${new Date().toISOString()} |`;

    let name, email;
    if (user) {
        name = user.name || user.fullName || user.lastName || user.otherNames || user.firstName;
        email = user.email;
    }

    let info = '';
    for (let [key, val] of Object.entries(args || [])) {
        info += `${key}="${val}"; `
    }

    logData += `[${email || 'SYSTEM'}] |[${name || '--'}] | ${title} | ${message} | ${info || ''}` + EOL;

    await createInstream();
    if (inStream) {
        inStream.write(logData);
    } else {
        console.info(logData);
    }
}

module.exports = { appLog, startLogging, stopLogging };