process.env.PEERS_NUM = 0;

const { getSocketIo, connectSocketClient } = require('./socket');
const Joi = require('joi');
const { appLog } = require('./logger');
const { Db } = require('litlib/ldb');
const db = new Db();

const peers = {};

function coordinatorSetup() {
    const io = getSocketIo();
    io.origins = '*';


    io.on('connection', s => {
        /** @type {import('socket.io').Socket} */
        let socket = s;
        socket.on('peer-registration', async (data, cb) => {
            // Validate
            try {
                Joi.assert(data, Joi.object({
                    uri: Joi.string(),
                    electionId: Joi.number(),
                }).unknown());
            } catch (err) {
                return cb && cb({ error: new Error('Invalid data format').toString() });
            }
            const r1 = await db.execute({
                text: `SELECT peer_servers_num FROM elections WHERE election_id = $1`,
                values: [data.electionId]
            });
            if (Object.keys(peers).length == (r1.rows[0].peer_servers_num || 0)) {
                return cb && cb({ error: new Error('Peer server number limit reached').toString() });
            }


            process.env.PEERS_NUM += 1;
            peers[socket.id] = data;
            socket.join('peers');
            socket.to('peers').emit('new-peer', data);
            await connectToPeerServer(data);
            const peerVals = Object.values(peers);
            for (let val of peerVals) {
                if (val.uri === data.uri) continue;
                // Inform new sockets of existing peers
                socket.emit('new-peer', val);
            }
            console.info({
                info: 'New peer registered',
                uri: data.uri,
                totalPeers: peerVals.length
            });
            appLog('PEER', 'Peer registered', null, data);
            cb({ secret: process.env.SECRET });
        });
        socket.on('disconnect', () => {
            process.env.PEERS_NUM -= 1;
            const peer = peers[socket.id];
            delete peers[socket.id];
            if (!peer) return;
            console.error({
                error: 'Peer disconnected', 
                uri: peer.uri,
                totalPeers: Object.keys(peers).length,
            });
            appLog('PEER', 'Peer disconnected', null, { peer });
        })
    });
}


const MIN_PC = .6;
async function connectToPeerServer(peer) {
    const { confirmVote } = require('../model/entities/voters');
    const client = await connectSocketClient(peer.uri);
    console.info({ info: 'Connected to new peer server', uri: peer.uri });
    const confirmationCount = {};
    client.on('test', console.log)
    client.on('vote-confirmation', (votes) => {
        Joi.assert(votes, Joi.array().items(Joi.object({ voteHash: Joi.string() }).unknown()));
        const MIN_CONFIRMATIONS = Math.ceil(Object.keys(peers).length * MIN_PC);
        for (let vote of votes) {
            confirmationCount[vote.voteHash] = confirmationCount[vote.voteHash] || 0;
            const count = ++confirmationCount[vote.voteHash];
            if (count >= MIN_CONFIRMATIONS) {
                confirmVote(vote);
                delete confirmationCount[vote.voteHash];
                appLog('PEER', `Vote confirmed by at least ${MIN_PC * 100}% of the peers`);
            }
        }
    });
}


function broadcastVoteToPeers(data) {
    const io = getSocketIo();
    io.to('peers').emit('vote-confirmation', data);
}

function getRegisteredPeersNum() {
    return Object.keys(peers).length;
}

function getRegisteredPeerUris(electionId) {
    if (!electionId) {
        throw new Error('ElectionId is required');
    }
    const electionPeers = [];
    for (let val of Object.values(peers)) {
        if (val.electionId == electionId) {
            electionPeers.push(val.uri);
        }
    }
    return electionPeers;
}


module.exports = {
    broadcastVoteToPeers, coordinatorSetup, getRegisteredPeersNum,
    getRegisteredPeerUris,
}