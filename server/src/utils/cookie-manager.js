const { cipher } = require("litlib");

module.exports.cookieManager = {
    setCookies: async function (res, { user, remember }) {
        const oneHour = 1000 * 60 * 60 * 1;
        let maxAge = oneHour;
        if (user.adminId) {
            maxAge = oneHour * 24; // one day
        } else if (user.voterId) {
            maxAge = oneHour / 3; // 20 min
        }
        const key = await cipher.generateKey(process.env.MASTER_PASSWORD);
        const data = JSON.stringify(user);
        const encryptedSub = await cipher.encrypt(data, key);
        res.cookie(
            'sub',
            encryptedSub,
            { maxAge, httpOnly: true, signed: true }
        );
        if(user && user.verified){
            res.cookie('verified', true, { maxAge, httpOnly: true, signed: true });
        }
        res.cookie('exp', Date.now() + maxAge, { maxAge, httpOnly: true, signed: true });
        res.cookie('isLoggedIn', true, { maxAge });
        res.cookie('remember', remember, { maxAge, httpOnly: true });
    },

    getCookieData: async function (req) {
        const userCookie = req.signedCookies.sub;
        if (userCookie && !req.cookies.isLoggedIn) {
            console.log('cleared cookies');
            for (let cookie in { ...req.cookies, ...req.signedCookies }) {
                res.clearCookie(cookie);
            }
        }

        const key = await cipher.generateKey(process.env.MASTER_PASSWORD);
        const data = !!userCookie && await cipher.decrypt(userCookie, key);

        // TODO give the cookie more time if about to expire
        const user = data && JSON.parse(data) || {};
        const remember = req.cookies.remember;
        const verified = req.signedCookies.verified;
        return { user, remember, verified };
    }
}