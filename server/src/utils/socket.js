const socketio = require('socket.io');
const ioClient = require('socket.io-client');

/** @type {SocketIO.Server} */
let io;
/** @param {import('http').Server} server */
function connectSocket(server) {
    io = socketio(server);
    require('./coordinator-actions').coordinatorSetup();
    return io;
}


function getSocketIo(){
    if (!io){
        throw new Error('Socket.io is not initialized!');
    }
    return io;
}

const clients = {};
/** @returns {SocketIOClient.Socket} */
function connectSocketClient(uri){
    const client = clients[uri];
    if(!client){
        const client = ioClient(uri);
        return new Promise((resolve) => {
            client.on('connect', () => {
                clients[uri] = client;
                resolve(client);
            });
        })
    }
    return client;
}

function getSocketClients(){
    return clients;
}


module.exports = {
    connectSocket, getSocketIo, connectSocketClient, getSocketClients,
};