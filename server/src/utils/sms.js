if (!process.env.SMS_API_KEY) {
    require('dotenv').config();
}
const FormData = require('form-data');
const Joi = require('joi');
var XMLparser = require('xml2json');
const axios = require('axios').default;
const url = process.env.SMS_USERNAME === 'sandbox' ?
    'https://api.sandbox.africastalking.com/version1/messaging' :
    'https://api.africastalking.com/version1/messaging';

async function send({ message, to, }) {
    try {
        Joi.assert(message, Joi.string());
        Joi.assert(to, Joi.string());
    } catch (err) {
        console.error(err);
        return;
    }
    to = to.toString();
    const form = new FormData();
    form.append('to', to);
    form.append('message', message);
    form.append('username', process.env.SMS_USERNAME);


    const resp = await axios.post(url, form, {
        headers: {
            ...form.getHeaders(),
            'apiKey': process.env.SMS_API_KEY,
        },
    });
    const { status, statusText} = resp;
    const data = JSON.parse(XMLparser.toJson(resp.data)).AfricasTalkingResponse.SMSMessageData;
    return { status, statusText, data };
}


module.exports.sms = {
    send
}