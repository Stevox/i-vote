const runDbDef = require('../database/scripts/run-db-def');
const createAdmin = require('./create-admin');
const createOfficial = require('./create-official');

async function main(){
    await runDbDef();
    await createAdmin({
        fullName: 'Ben Steve',
        email: 'benzstevox@mailinator.com',
        adminLevel: 1,
    });
    await createOfficial({
        fullName: 'Bonza',
        email: 'bonza@mailinator.com',
    })
    process.exit();
}


main().catch(err => {
    console.error(err);
    process.exit(1);
});