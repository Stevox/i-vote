const { cipher } = require('litlib');
const { Db } = require('litlib/ldb');
const db = new Db();
const { castVote } = require('../src/model/entities/voters');
const Chance = require('chance');

async function main() {
    const q1 = {
        text: `SELECT * FROM voters WHERE election_id = $1 AND email NOT LIKE 'benstevethuku%'`,
        values: [1]
    }
    const r1 = await db.execute(q1);
    for (let voter of r1.rows) {
        const chance = Chance();
        const { voter_id: voterId } = voter;
        const castDate = chance.date({ year: 2020, month: 11, day: new Date().getDate() });
        const voterHash = cipher.hashText(voter.voter_id, process.env.SECRET);
        const pollVotes = [
            {
                candidateId: chance.integer({ min: 1, max: 5 }),
                pollId: 1,
            },
            {
                candidateId: chance.integer({ min: 6, max: 19 }),
                pollId: 2,
            },
            {
                candidateId: chance.integer({ min: 20, max: 33 }),
                pollId: 3,
            },
            {
                candidateId: chance.integer({ min: 34, max: 148 }),
                pollId: 4,
            },
        ]

        await castVote({ voterId, voterHash, castDate, electionId: 1, pollVotes })
    }
    process.exit(0);
}

try {
    main();
} catch (error) {
    console.error(err);
    process.exit(1);
} 
// finally {
// }