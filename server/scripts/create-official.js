const Joi = require('joi');
const readline = require('readline');
const { createNewOfficial } = require('../src/model/entities');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    crlfDelay: Infinity,
});

async function createOfficial(data) {
    if (!data) {
        let invalid = false;
        do {
            const fullName = await ask('Enter full name: ');
            const email = await ask('Enter email: ');
            const password = await ask('Enter password: [123] ') || undefined;
            data = { fullName, email, password };
            try {
                data = validate(data);
            } catch (err) {
                console.error(`${err.toString()}\n\n`);
                invalid = true;
            }
        } while (invalid)

        rl.close();
    } else {
        data = validate(data);
    }
    await createNewOfficial(data);
    console.info(`Official ${data.email} successfully created.`);
}

function ask(qn) {
    return new Promise((resolve) => {
        rl.question(qn, ans => {
            resolve(ans);
        });
    });
}


function validate(data) {
    try {
        return Joi.attempt(data, Joi.object({
            fullName: Joi.string().required(),
            email: Joi.string().email().required(),
            password: Joi.string().default('123'),
        }).unknown());
    } catch (err) {
        throw new Error(`${err.details[0].message}`);
    }
}

if (require.main === module) {
    createOfficial()
        .catch(e => console.error(err))
        .finally(_ => process.exit)
}
module.exports = createOfficial;