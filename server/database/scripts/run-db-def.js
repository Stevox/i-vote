/** @license
 * 
 * The MIT License (X11)
 * Copyright (c) 2020 Ben Steve <benzstevox@gmail.com>
 * 
 * Permission is hereby granted, free of charge, 
 * to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, 
 * including without limitation the rights to 
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of 
 * the above copyright holders shall not be used in advertising 
 * or otherwise to promote the sale, use or other dealings in this Software 
 * without prior written authorization. 
 * 
 */

const fsPromises = require('fs').promises;
const path = require('path');
const { loadFile } = require('./load-sql-file');
require('dotenv').config();

const SQL_DEF_PATH = path.resolve(__dirname, '../def');

module.exports = main;

async function main() {
    // TODO recursive read dir
    const tableFilenames = await getTableFilenames();
    const dropCmd = `DROP SCHEMA public CASCADE; CREATE SCHEMA public;`;
    const beginCmd = await getCreateSQLObjectsCmd({ only: ['@begin'] });
    const createTblCmd = await getCreateTablesCmd(tableFilenames);
    const createObjCmd = await getCreateSQLObjectsCmd({ exclude: ['@begin', '@end', '@ignore', 'tables'] });
    const endCmd = await getCreateSQLObjectsCmd({ only: ['@end'] });

    const dumpPath = path.resolve(__dirname, 'dump.sql');
    await fsPromises.writeFile(dumpPath, (
        dropCmd + beginCmd + createTblCmd + createObjCmd + endCmd
    ));

    await loadFile(dumpPath);

    await fsPromises.unlink(dumpPath);
}


async function getTableFilenames() {
    console.info('Resolving table dependencies...');
    const fDeps = {}; // forward dependencies - table A depends on tables [B, C, ...]
    const bDeps = {}; // backward dependencies - table B is depended on by tables [A, D, ...]
    const dirPath = path.join(SQL_DEF_PATH, 'tables');
    const filepaths = await recursiveDirRead(dirPath);
    const tableFilenames = {};
    for (let filepath of filepaths) {
        const filename = /^.+\/(.+)/.exec(filepath)[1];
        const data = await fsPromises.readFile(filepath, 'utf8');
        const table = data.match(/CREATE TABLE (\w+)/)[1].toLowerCase();
        tableFilenames[table] = filename;
        const matches = data.matchAll(/REFERENCES (\w+)/g);
        // const matches2 = data.matchAll(/SELECT .+? FROM (\w+)/g);
        // for (let match of [...matches, ...matches2]) {
        for (let match of matches) {
            const ref = match[1].toLowerCase();
            if (ref === table) continue; // Self referencing table
            if (fDeps[ref] && fDeps[ref].includes(table)) {
                throw new Error(`Circular table dependency encountered on tables ${table} and ${ref}`);
            }
            fDeps[table] ? fDeps[table].push(ref) : fDeps[table] = [ref];
            bDeps[ref] ? bDeps[ref].push(table) : bDeps[ref] = [table];
        }
    }



    const tables = {};
    let nextTable = Object.keys(tableFilenames)[0];
    while (nextTable) {
        nextTable = removeDeps(nextTable);
    }

    for (let [tbl, fp] of Object.entries(tables)) {
        if (!fp) {
            throw new Error(`Missing file path for the table: ${tbl}`);
        }
    }

    return tables;

    // TODO throw error on circular dependency
    function removeDeps(table) {
        if (!fDeps[table]) {
            tables[table] = tableFilenames[table];
            delete tableFilenames[table];
            if (bDeps[table]) {
                for (dep of bDeps[table]) {
                    fDeps[dep].splice(fDeps[dep].indexOf(table), 1);
                    if (fDeps[dep].length === 0) {
                        delete fDeps[dep];
                    }
                }
            }
            return Object.keys(tableFilenames)[0];
        }
        return fDeps[table][0];
    }
}




/**
 * Get SQL commands to drop tables
 * @param {string[]} tables
 * @returns {string} The drop table commands 
 */
async function getDropTablesCmd(tables) {
    const dropQueries = [];
    for (let i = tables.length - 1; i > -1; i--) {
        dropQueries.push(`DROP TABLE IF EXISTS ${tables[i]} CASCADE;`);
    }
    return dropQueries.join('\n') + '\n\n';
}




/**
 * Get SQL commands to create tables
 * @param {tableFilename} tableFilenames 
 * @param {string} dirPath 
 * @returns {string} The create tables commands
 */
async function getCreateTablesCmd(tableFilenames) {
    const cmds = [];
    console.info(tableFilenames)
    for (let filename of Object.values(tableFilenames)) {
        const filepath = path.resolve(SQL_DEF_PATH, 'tables', filename);
        cmds.push(`\\i '${filepath}';`);
    }
    return cmds.join('\n') + '\n\n';
}
/**
 * An object with key as the table name and value as the path.
 *   Created by the getTableFilenames function.
 * @typedef {object} tableFilename
 */



/**
* Get SQL commands to create SQL objects in given path.
*   These objects are such as views, triggers, functions etc.
*   Note, no dependency resolution is done for these objects.
* @param {{
*  exclude: string[],
*  only: string[],
* }} opts 
* @returns {string} The create commands
*/
async function getCreateSQLObjectsCmd(opts = { exclude: [] }) {
    const dirs = await fsPromises.readdir(SQL_DEF_PATH, { withFileTypes: true });
    const cmds = [];

    for (let dir of dirs) {
        if (!dir.isDirectory()) continue;
        if (opts.exclude && opts.exclude.includes(dir.name)) continue;
        if (opts.only && !opts.only.includes(dir.name)) continue;

        const filepaths = await recursiveDirRead(path.resolve(SQL_DEF_PATH, dir.name));
        for (let filepath of filepaths) {
            cmds.push(`\\i '${filepath}';`);
        }
    }
    return cmds.join('\n') + '\n\n';


}

async function recursiveDirRead(dirPath) {
    const dirs = await fsPromises.readdir(dirPath, { withFileTypes: true });
    const paths = [];
    for (let dir of dirs) {
        const dirp = path.resolve(dirPath, dir.name);
        if (dir.isDirectory()) {
            const tmp = await recursiveDirRead(dirp);
            paths.push(...tmp);
        } else {
            paths.push(dirp);
        }
    }
    return paths;
}