/** @license
 * 
 * The MIT License (X11)
 * Copyright (c) 2020 Ben Steve <benzstevox@gmail.com>
 * 
 * Permission is hereby granted, free of charge, 
 * to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, 
 * including without limitation the rights to 
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name(s) of 
 * the above copyright holders shall not be used in advertising 
 * or otherwise to promote the sale, use or other dealings in this Software 
 * without prior written authorization. 
 * 
 */

const url = require('url');
const { exec } = require('child_process');
require('dotenv').config();

async function loadFile(filename) {
    const { user, password, host, port, database } = getConfig();
    console.info(`\nConnecting to database: ${database}`);

    const dbCmd = `PGPASSWORD=${password} PGOPTIONS='--client-min-messages=warning' ` +
        `psql -h ${host} -p ${port} -U ${user} -d ${database} -f ${filename} ` +
        `--quiet -x --pset pager=off -v ON_ERROR_STOP=1`;

    await runCommand(dbCmd);
}


async function runCommand(cmd) {
    return new Promise((resolve, reject) => {
        console.info('Executing command...');
        exec(cmd, (err, stdout, stderr) => {
            if (err) {
                console.error('Error running terminal:\n');
                return reject(err);
            }
            if (stderr) {
                console.error('Error running command:\n', stderr);
            }
            console.info(stdout);
            return resolve();
        });
    })
}



function getConfig() {
    let config = {}
    if (process.env.DATABASE_URL) {
        //Heroku
        const params = url.parse(process.env.DATABASE_URL);
        const auth = params.auth.split(':');

        config = {
            user: auth[0],
            password: auth[1],
            host: params.hostname,
            port: params.port,
            database: params.pathname.split('/')[1],
            ssl: true
        };
    } else {
        //Development
        config = {
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            max: process.env.MAX_POOL || 20,
            idleTimeoutMillis: process.env.IDLE_TIMEOUT_MS || 60000,
            port: process.env.DB_PORT || 5432,
            host: process.env.DB_HOST || 'localhost',
            database: process.env.DB_DATABASE
        }

        if (process.env.NODE_ENV === 'test' && process.env.DB_DATABASE_TEST) {
            config.database = process.env.DB_DATABASE_TEST;
        }

        if (!(config.user && config.password && config.port && config.database)) {
            let msg = `Please set up the .env file with the keys: 
			\tprocess.env.DB_USER={user} 
			\tprocess.env.DB_PASSWORD={password} 
			\tprocess.env.DB_PORT={port} 
			\tprocess.env.DB_DATABASE={database_name}`
            console.error(msg);
        }
    }
    return config;
}

module.exports = { loadFile };