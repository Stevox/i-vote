CREATE OR REPLACE FUNCTION prevent_forbidden_action() RETURNS trigger AS $$
  BEGIN
    RAISE restrict_violation USING MESSAGE = 'The requested action is forbidden.';
    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;