CREATE OR REPLACE VIEW candidates_v AS (
  SELECT c.*, COUNT(vote_id) votes, region_id
    FROM candidates c LEFT JOIN votes USING (candidate_id) 
      LEFT JOIN region_areas USING (region_area_id)
    GROUP BY (candidate_id, region_id)
    ORDER BY candidate_id
);

CREATE OR REPLACE VIEW region_areas_v AS (
  SELECT 
    ra.*,
    candidates.candidates
  FROM region_areas ra
    LEFT JOIN (
      SELECT region_area_id, json_agg(c) candidates FROM candidates_v c
      GROUP BY region_area_id
    ) candidates USING (region_area_id)
  ORDER BY (region_id, ra.name) ASC
);


CREATE OR REPLACE VIEW regions_v AS (
  SELECT 
    r.*,
    polls.polls,
    region_areas.region_areas
  FROM regions r
    LEFT JOIN (
      SELECT region_id, json_agg(p) polls FROM polls p
      GROUP BY region_id
      ORDER BY region_id
    ) polls USING (region_id)
    LEFT JOIN (
      SELECT region_id, json_agg(ra) region_areas FROM region_areas_v ra
      GROUP BY region_id
      ORDER BY region_id
    ) region_areas USING (region_id)
  WHERE r.is_active
  ORDER BY region_id ASC
);


CREATE OR REPLACE VIEW voters_v AS (
  SELECT
    v.*,
    ra.name region_area_name
  FROM voters v LEFT JOIN region_areas ra USING (region_area_id)
  ORDER BY v.full_name
);

CREATE OR REPLACE FUNCTION get_lowest_region_id(r_election_id BIGINT) RETURNS BIGINT AS $$
  BEGIN
    RETURN (SELECT region_id FROM regions 
      WHERE regions.election_id = r_election_id AND region_id <> ALL(
          SELECT COALESCE(parent_region_id, 0) FROM regions 
            WHERE regions.election_id = r_election_id
      )
    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW elections_v AS (
  SELECT 
    election.*,
    (SELECT email FROM officials WHERE official_id = election.official_id) official_email,
    get_lowest_region_id(election.election_id) lowest_region_id,
    (SELECT count(DISTINCT voter_hash) FROM votes WHERE election_id = election.election_id) votes_num,
    eo.officials,
    regions.regions,
    voters.voters
  FROM elections election
    LEFT JOIN (
      SELECT election_id, json_agg(eo) officials 
        FROM (
          SELECT election_id, official_id, email, phone, full_name
            FROM election_officials LEFT JOIN officials USING (official_id)
            WHERE is_active
          UNION 
            SELECT election_id, official_id, email, phone, full_name
              FROM officials LEFT JOIN elections USING (official_id)
        ) eo
        GROUP BY election_id
    ) eo USING (election_id)
    LEFT JOIN (
      SELECT election_id, json_agg(r) regions FROM regions_v r
      GROUP BY election_id
    ) regions USING (election_id)
    LEFT JOIN (
      SELECT election_id, json_agg(v) voters FROM voters_v v
      GROUP BY election_id
    ) voters USING (election_id)
  WHERE election.is_active
  ORDER BY election.start_date
);
