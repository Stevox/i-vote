CREATE OR REPLACE VIEW candidates_results_v AS (
  SELECT
      r.election_id,
      c.name,
      c.deputy_name,
      c.party,
      COUNT(vote_id) votes,
      r.name region_name,
      ra.name region_area_name,
      p.position
    FROM candidates c LEFT JOIN votes USING (candidate_id)
      LEFT JOIN region_areas ra  USING (region_area_id)
      LEFT JOIN regions r  USING (region_id)
      LEFT JOIN polls p ON c.poll_id = p.poll_id
    GROUP BY (r.election_id, candidate_id, ra.name, r.name, p.position, p.poll_id)
    ORDER BY region_area_id ASC, p.poll_id ASC, COUNT(vote_id) DESC
);


CREATE OR REPLACE VIEW votes_results_v AS (
  SELECT
    v.election_id,
    v.voter_hash,
    v.cast_date,
    c.name candidate_name,
    p.position
  FROM votes v LEFT JOIN candidates c USING (candidate_id)
    LEFT JOIN polls p ON v.poll_id = p.poll_id
  ORDER BY p.region_id ASC, p.poll_id ASC
);


CREATE OR REPLACE VIEW elections_results_v AS (
  SELECT
    e.election_id,
    e.title,
    e.election_code,
    e.description,
    e.start_date,
    e.end_date,
    c.candidates,
    v.votes
  FROM elections e
    LEFT JOIN (
      SELECT election_id, json_agg(c) candidates FROM candidates_results_v c
      GROUP BY election_id
    ) c USING (election_id)
    LEFT JOIN (
      SELECT election_id, json_agg(v) votes FROM votes_results_v v
      GROUP BY election_id
    ) v USING (election_id)
  WHERE e.is_active
);
