CREATE TABLE admins (
  admin_id BIGSERIAL PRIMARY KEY,
  full_name TEXT NOT NULL,
  email TEXT NOT NULL UNIQUE,
  admin_level SMALLINT NOT NULL DEFAULT 2, -- 1 is highest
  password_hash TEXT NOT NULL,
  password_salt TEXT NOT NULL,
  phone TEXT,
  profile_pic_url TEXT,
  verification_code TEXT,
  verification_expiry TEXT,
  reset_hash TEXT,
  reset_request_expiry TIMESTAMP,
  reg_date TIMESTAMP WITH time zone NOT NULL DEFAULT now(),
  is_active BOOLEAN DEFAULT true
);


CREATE TRIGGER disable_admin_trigg BEFORE DELETE ON admins 
  FOR EACH ROW EXECUTE PROCEDURE disable_row_func();


CREATE TRIGGER set_admin_reset_request_expiry_trigg BEFORE UPDATE ON admins 
  FOR EACH ROW WHEN (NEW.reset_hash IS NOT NULL) EXECUTE PROCEDURE set_reset_request_expiry();


CREATE TRIGGER validate_admin_account_password_trigg_reset BEFORE UPDATE ON admins 
  FOR EACH ROW WHEN (OLD.reset_hash IS NOT NULL AND NEW.reset_hash IS NULL)
  EXECUTE PROCEDURE validate_account_password_reset();




CREATE TRIGGER set_admin_login_verification_request_expiry_trigg BEFORE UPDATE ON admins 
  FOR EACH ROW WHEN (NEW.verification_code IS NOT NULL) EXECUTE PROCEDURE set_login_verification_request_expiry();



CREATE TRIGGER validate_admin_login_verification_trigg BEFORE UPDATE ON admins 
  FOR EACH ROW WHEN (OLD.verification_code IS NOT NULL AND NEW.verification_code IS NULL)
  EXECUTE PROCEDURE validate_login_verification();