CREATE TABLE polls (
    poll_id BIGSERIAL PRIMARY KEY,
    position TEXT NOT NULL,
    description TEXT,
    region_id BIGINT NOT NULL REFERENCES regions ON DELETE CASCADE
);