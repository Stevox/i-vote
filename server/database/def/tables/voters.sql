CREATE TABLE voters (
    voter_id BIGSERIAL PRIMARY KEY,
    full_name TEXT NOT NULL,
    email TEXT NOT NULL,
    phone TEXT NOT NULL,
    profile_pic_url TEXT,
    region_area_id BIGINT NOT NULL REFERENCES region_areas ON DELETE RESTRICT,
    election_id BIGINT NOT NULL REFERENCES elections ON DELETE CASCADE,
    id_field_value TEXT,
    verification_code TEXT,
    verification_expiry TEXT,
    has_voted BOOLEAN NOT NULL DEFAULT false,
    reg_date TIMESTAMP WITH time zone NOT NULL DEFAULT now(),

    -- WILL 3 UNIQUE CONSTRAINTS CAUSE MEMORY ISSUE?
    UNIQUE(election_id, phone),
    UNIQUE(election_id, email),
    UNIQUE(election_id, id_field_value)
);




CREATE TRIGGER set_voter_login_verification_request_expiry_trigg BEFORE UPDATE ON voters 
  FOR EACH ROW WHEN (NEW.verification_code IS NOT NULL) EXECUTE PROCEDURE set_login_verification_request_expiry();



CREATE TRIGGER validate_voter_login_verification_trigg BEFORE UPDATE ON voters 
  FOR EACH ROW WHEN (OLD.verification_code IS NOT NULL AND NEW.verification_code IS NULL)
  EXECUTE PROCEDURE validate_login_verification();