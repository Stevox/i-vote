CREATE TABLE regions (
    region_id BIGSERIAL PRIMARY KEY,
    parent_region_id BIGINT REFERENCES regions ON DELETE SET NULL,
    name TEXT NOT NULL, -- name of the region type eg Country, Ward etc
    election_id BIGINT NOT NULL REFERENCES elections ON DELETE CASCADE,
    is_active BOOLEAN DEFAULT true,

    UNIQUE(election_id, name)
);