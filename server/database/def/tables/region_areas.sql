CREATE TABLE region_areas (
    region_area_id BIGSERIAL PRIMARY KEY,
    region_id BIGINT NOT NULL REFERENCES regions ON DELETE CASCADE,
    name TEXT NOT NULL,
    code TEXT,
    parent_region_area_id BIGINT REFERENCES region_areas ON DELETE CASCADE,
    UNIQUE (region_id, name)
);

CREATE OR REPLACE FUNCTION require_parent_region_area() RETURNS trigger AS $$
  BEGIN
    IF (
      SELECT parent_region_id IS NOT NULL FROM regions WHERE region_id = NEW.region_id
    ) AND NEW.parent_region_area_id IS NULL THEN
      RAISE SQLSTATE 'AE001' 
        USING MESSAGE = 'Provided region area requires a parent region',
        DETAIL = 'Region name: ' || NEW.name;
    END IF;
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER require_parent_region_area_trigg AFTER INSERT OR UPDATE ON region_areas
  FOR EACH ROW EXECUTE PROCEDURE require_parent_region_area();
