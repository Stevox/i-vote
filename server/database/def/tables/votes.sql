CREATE TABLE votes (
  vote_id BIGSERIAL PRIMARY KEY,
  voter_hash TEXT NOT NULL,
  election_id BIGINT NOT NULL REFERENCES elections,
  candidate_id BIGINT NOT NULL REFERENCES candidates,
  poll_id BIGINT NOT NULL REFERENCES polls,
  cast_date TIMESTAMP NOT NULL DEFAULT now(),
  block_hash TEXT NOT NULL,
  UNIQUE (voter_hash, election_id, poll_id) -- Single vote per poll
);


CREATE OR REPLACE FUNCTION hash_new_vote() RETURNS trigger AS $$
  DECLARE
    prev_hash TEXT;
  BEGIN
    IF NOT (SELECT is_live FROM elections WHERE election_id = NEW.election_id) THEN
      RAISE SQLSTATE 'AE001' 
        USING MESSAGE = 'Election must be live in order to vote';
    END IF;
    IF NEW.block_hash IS NOT null THEN
      PERFORM prevent_forbidden_action();
    END IF;
    NEW.block_hash = (SELECT substring(digest(
        NEW::TEXT || (SELECT COALESCE((
          SELECT block_hash FROM votes WHERE vote_id = NEW.vote_id - 1
        ),'')),
    'sha512') from 3 ));

    
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER hash_new_vote_trigg BEFORE INSERT ON votes 
  FOR EACH ROW EXECUTE PROCEDURE hash_new_vote();



CREATE TRIGGER prevent_vote_delete_trigg BEFORE DELETE ON votes 
  FOR EACH ROW EXECUTE PROCEDURE prevent_forbidden_action();