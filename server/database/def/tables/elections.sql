CREATE TABLE elections (
    election_id BIGSERIAL PRIMARY KEY,
    election_code TEXT NOT NULL UNIQUE,
    logo_file_url TEXT,
    title TEXT NOT NULL,
    description TEXT,
    start_date TIMESTAMP NOT NULL,
    peer_servers_num SMALLINT,
    end_date TIMESTAMP NOT NULL,
    id_field_name TEXT DEFAULT 'ID Number', -- Voter Identification Number
    allow_null_votes BOOLEAN DEFAULT false,
    lowest_region_poll_first BOOLEAN DEFAULT true,
    is_live BOOLEAN DEFAULT false,
    creation_date TIMESTAMP NOT NULL DEFAULT now(),
    official_id BIGINT NOT NULL REFERENCES officials ON DELETE RESTRICT, -- Organizer
    is_active BOOLEAN DEFAULT true,

    UNIQUE(official_id, title, start_date)
);


CREATE FUNCTION go_live() RETURNS trigger AS $$
  BEGIN
    IF(SELECT count(*) = 0 FROM voters WHERE election_id = NEW.election_id) THEN
      RAISE SQLSTATE 'AE001' 
        USING MESSAGE = 'Election must have voters in order to go live';
    END IF;
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER go_live_trigger BEFORE UPDATE ON elections
  FOR EACH ROW WHEN (NEW.is_live) EXECUTE PROCEDURE go_live();



CREATE TRIGGER disable_election_trigg BEFORE DELETE ON elections 
  FOR EACH ROW EXECUTE PROCEDURE disable_row_func();
  