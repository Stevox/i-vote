CREATE TABLE election_officials (
    official_id BIGINT NOT NULL REFERENCES officials ON DELETE CASCADE,
    election_id BIGINT NOT NULL REFERENCES elections ON DELETE CASCADE,

    PRIMARY KEY(official_id, election_id)
);