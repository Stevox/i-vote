CREATE TABLE officials (
  official_id BIGSERIAL PRIMARY KEY, 
  full_name TEXT,
  email TEXT NOT NULL UNIQUE,
  password_hash TEXT NOT NULL,
  password_salt TEXT NOT NULL,
  phone TEXT,
  profile_pic_url TEXT,
  account_confirmation_hash TEXT,
  verification_code TEXT,
  verification_expiry TEXT,
  reset_hash TEXT,
  reset_request_expiry TIMESTAMP,
  invited BOOLEAN DEFAULT false,
  reg_date TIMESTAMP WITH time zone NOT NULL DEFAULT now(),
  is_active BOOLEAN DEFAULT true
);


CREATE TRIGGER set_official_reset_request_expiry_trigg BEFORE UPDATE ON officials 
  FOR EACH ROW WHEN (NEW.reset_hash IS NOT NULL) EXECUTE PROCEDURE set_reset_request_expiry();


CREATE TRIGGER validate_official_account_password_reset_trigg BEFORE UPDATE ON officials 
  FOR EACH ROW WHEN (OLD.reset_hash IS NOT NULL AND NEW.reset_hash IS NULL)
  EXECUTE PROCEDURE validate_account_password_reset();



CREATE TRIGGER set_official_login_verification_request_expiry_trigg BEFORE UPDATE ON officials 
  FOR EACH ROW WHEN (NEW.verification_code IS NOT NULL) EXECUTE PROCEDURE set_login_verification_request_expiry();



CREATE TRIGGER validate_official_login_verification_trigg BEFORE UPDATE ON officials 
  FOR EACH ROW WHEN (OLD.verification_code IS NOT NULL AND NEW.verification_code IS NULL)
  EXECUTE PROCEDURE validate_login_verification();



CREATE FUNCTION handle_official_deletion() RETURNS trigger AS $$
  BEGIN 
    IF (SELECT count(*) FROM elections WHERE official_id = NEW.official_id AND is_active) THEN
      RAISE SQLSTATE 'AE001' 
        USING MESSAGE = 'The election organized by the official must be deleted first.';
    END IF;
    RETURN OLD;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER handle_official_deletion_trigg BEFORE DELETE ON officials 
  FOR EACH ROW EXECUTE PROCEDURE handle_official_deletion();

CREATE TRIGGER disable_official_trigg BEFORE DELETE ON officials 
  FOR EACH ROW EXECUTE PROCEDURE disable_row_func();
  