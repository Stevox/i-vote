CREATE TABLE candidates (
    candidate_id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    deputy_name TEXT,
    profile_pic_url TEXT,
    party TEXT,
    poll_id BIGINT NOT NULL REFERENCES polls ON DELETE CASCADE,
    region_area_id BIGINT NOT NULL REFERENCES region_areas ON DELETE CASCADE,

    UNIQUE(name, poll_id, region_area_id)
);